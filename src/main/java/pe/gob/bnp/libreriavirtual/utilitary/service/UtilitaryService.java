package pe.gob.bnp.libreriavirtual.utilitary.service;

import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.model.EmailTemplate;
import pe.gob.bnp.libreriavirtual.utilitary.repository.UtilitaryRepository;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Service
public class UtilitaryService {
	
	private static final Logger logger = LoggerFactory.getLogger(UtilitaryService.class);
	
	@Autowired
	UtilitaryRepository utilitaryRepository;	
	
	private EmailTemplate emailData;
	
	private ResponseTransaction responseEmail;
	
	public ResponseTransaction searchMaster(String type) {

		return utilitaryRepository.listMaster(type);
	}

	public ResponseTransaction searchDepartment() {

		return utilitaryRepository.listDepartment();
	}

	public ResponseTransaction searchProvince(String codeDpt) {

		return utilitaryRepository.listProvince(codeDpt);
	}

	public ResponseTransaction searchDistrict(String codeDpt,String codeProv) {

		return utilitaryRepository.listDistrict(codeDpt,codeProv);
	}
	
	public ResponseTransaction searchCategories() {

		return utilitaryRepository.listCategories();
	}
	
	public ResponseTransaction searchAuthors() {

		return utilitaryRepository.listAuthors();
	}
	
	public ResponseTransaction searchYears() {

		return utilitaryRepository.listYears();
	}	
	
	public void sendEmailLV(String templateName,String operationNumber) {
		
		inicializarVariables();
		
		getEmail(templateName,operationNumber);
		
		if(responseEmail.getCodeResponse().equalsIgnoreCase("0000") && emailData.getActive().equalsIgnoreCase("1")) sendEmail();
		
	}
	
	private void inicializarVariables() {

		responseEmail = new ResponseTransaction();
		
	}

	private void getEmail(String templateName,String operationNumber) {
		
		try {
			// leer email
			responseEmail = utilitaryRepository.getEmail(templateName,operationNumber);

			if (responseEmail.getCodeResponse().equalsIgnoreCase("0000")) {
				List<Object> emailList = responseEmail.getList();
				if (!emailList.isEmpty()) {
					Object sharedObject = emailList.get(0);
					if (sharedObject instanceof EmailTemplate) {
						emailData = (EmailTemplate) sharedObject;
					}
				}

			}

		} catch (Exception ex) {
			responseEmail.setCodeResponse("0666");			
			responseEmail.setResponse(ex.getMessage());
			logger.error("0666 error postconstructor getEmail:" + ex);
		}
		
	}

	private void sendEmail() {
		
		try {	
		
		Properties p = new Properties();			
		p.put("mail.smtp.auth",true);
		p.put("mail.smtp.starttls.enable",true);
		p.put("mail.smtp.host","smtp.office365.com");
		p.put("mail.smtp.port","587");
		p.put("mail.smtp.connectiontimeout","6000");
		p.put("mail.smtp.timeout","6000");
		p.put("mail.smtp.writetimeout","6000");
				
		String MyAccountEmail = emailData.getFrom();
		String password = emailData.getKey();
		
		Session session= Session.getInstance(p, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {				
				return new PasswordAuthentication(MyAccountEmail, password);
			}
			
			
		});
		
		Message message = prepareMessage(session);
		
		Transport.send(message);
		
		}catch(Exception exx) {
			logger.error("Error send Email1:"+exx);
			logger.error("Error send Email1:"+exx.getMessage());
			
			///update state error
			try {
				utilitaryRepository.setSend(emailData.getEmailOrderId());
			}catch(Exception ex) {
				logger.error("Error send Email1 BD:"+exx);
			}
			
		}	
		
		
	}
	
	private Message prepareMessage(Session session) {
				
		try {		
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailData.getFrom()));
			//message.setRecipient(Message.RecipientType.TO, new InternetAddress("carloscj1803@gmail.com"));//new InternetAddress(emailData.getTo()
			
			if (emailData.getTo().indexOf(',') > 0) {
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailData.getTo()));
			} else {
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(emailData.getTo()));
            }
			
			message.addRecipient(Message.RecipientType.BCC, new InternetAddress(emailData.getCo()));
			
			message.setSubject(emailData.getSubject());
			message.setContent(emailData.getContent(),"text/html;charset=UTF-8");										
			
			return message;

			
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error("Error send email2:"+e);
			logger.error("Error send email2:"+e.getMessage());
		}
						
	return null;
	}
}
