package pe.gob.bnp.libreriavirtual.utilitary.dto;


public class MasterResponse {

	private String masterId;
	private String masterName;
	private String masterDescription;
	public String getMasterId() {
		return masterId;
	}
	public void setMasterId(String masterId) {
		this.masterId = masterId;
	}
	public String getMasterName() {
		return masterName;
	}
	public void setMasterName(String masterName) {
		this.masterName = masterName;
	}
	public String getMasterDescription() {
		return masterDescription;
	}
	public void setMasterDescription(String masterDescription) {
		this.masterDescription = masterDescription;
	}
	public String getMasterValue() {
		return masterValue;
	}
	public void setMasterValue(String masterValue) {
		this.masterValue = masterValue;
	}
	private String masterValue;
}
