package pe.gob.bnp.libreriavirtual.utilitary.common;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utility {
	
	private static final Logger logger = LoggerFactory.getLogger(Utility.class);	
	
	private static final String FORMAT_DATE = "dd/MM/yyyy";
	private static final String FORMAT_DATE_TWO = "yyyy-MM-dd";

	/* Dates */

	public static String parseDateToString(Date date) {
		String resultado = Constants.EMPTY_STRING;
		if (date == null)
			return resultado;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE);
		resultado = dateFormat.format(date);
		return resultado;
	}
	
	

	public static Date parseStringToDate(String date) {
		Date resultado = null;
		if (date == null)
			return resultado;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE);
		try {
			resultado = dateFormat.parse(date);
		} catch (ParseException e) {
			return null;
		}
		return resultado;
	}
	
	/*public static String parseDateToStringTwo(Date date) {
		String resultado = Constants.EMPTY_STRING;
		if (date == null)
			return resultado;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE_TWO);
		resultado = dateFormat.format(date);
		return resultado;
	}*/
	
	public static Date parseStringToDateTwo(String date) {
		Date resultado = null;
		if (date == null)
			return resultado;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE_TWO);
		try {
			resultado = dateFormat.parse(date);
		} catch (ParseException e) {
			return null;
		}
		return resultado;
	}

	public static Date getDate(java.sql.Date sqlDate) {
		if (sqlDate == null)
			return null;
		return new Date(sqlDate.getTime());
	}

	public static java.sql.Date getSQLDate(Date date) {
		if (date == null)
			return null;
		return new java.sql.Date(date.getTime());
	}

	public static String parseSqlDateToString(java.sql.Date sqlDate) {
		Date date = getDate(sqlDate);
		return parseDateToString(date);
	}
	
	/*public static String parseSqlDateToStringTwo(java.sql.Date sqlDate) {
		Date date = getDate(sqlDate);
		return parseDateToStringTwo(date);
	}*/

	public static java.sql.Date parseStringToSQLDate(String date) {
		Date resultado = null;
		if (date == null)
			return null;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE);
		try {
			resultado = dateFormat.parse(date);
		} catch (ParseException e) {
			return null;
		}
		return getSQLDate(resultado);
	}
	
	public static java.sql.Date parseStringToSQLDateTwo(String date) {
		Date resultado = null;
		if (date == null)
			return null;
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE_TWO);
		try {
			resultado = dateFormat.parse(date);
		} catch (ParseException e) {
			return null;
		}
		return getSQLDate(resultado);
	}
	
	/* Double */
	
	public static Double parseObjectToDouble(Object objeto) {
		BigDecimal numeroBD = new BigDecimal(Constants.ZERO_DOUBLE);
		numeroBD = (BigDecimal) objeto;
		Double id = numeroBD.doubleValue();
		return id;
	}
	
	public static Double parseStringToDouble(String numero) {
		Double resultado;
		if (numero == null || numero.trim().equals(Constants.EMPTY_STRING))
			return Constants.ZERO_DOUBLE;
		resultado = Double.parseDouble(numero);
		return resultado;
	}

	public static String parseDoubleToString(Double numero) {
		String resultado;
		if (numero == null || numero <= Constants.ZERO_DOUBLE)
			return Constants.EMPTY_STRING;
		resultado = String.valueOf(numero);
		return resultado;
	}

	/* Long */

	public static Long parseObjectToLong(Object objeto) {
		BigDecimal numeroBD = new BigDecimal(Constants.ZERO_LONG);
		numeroBD = (BigDecimal) objeto;
		Long id = numeroBD.longValue();
		return id;
	}

	public static Long parseStringToLong(String numero) {
		Long resultado;
		if (numero == null || numero.trim().equals(Constants.EMPTY_STRING))
			return Constants.ZERO_LONG;
		resultado = Long.parseLong(numero);
		return resultado;
	}

	public static String parseLongToString(Long numero) {
		String resultado;
		if (numero == null || numero <= Constants.ZERO_LONG)
			return Constants.EMPTY_STRING;
		resultado = String.valueOf(numero);
		return resultado;
	}

	/* Integer */

	public static Integer getInteger(Integer obj) {
		if (obj == null) {
			return Constants.ZERO_INTEGER;
		} else {
			return obj;
		}
	}

	public static String parseIntToString(Integer numero) {
		String resultado;
		if (numero == null || numero <= Constants.ZERO_INTEGER)
			return Constants.EMPTY_STRING;
		resultado = String.valueOf(numero);
		return resultado;
	}

	public static String parseIntToString2(Integer numero) {
		String resultado;
		if (numero == null || numero <= Constants.ZERO_INTEGER)
			return "0";
		resultado = String.valueOf(numero);
		return resultado;
	}

	public static Integer parseStringToInt(String numero) {
		int resultado;
		if (numero == null || numero.trim().equals(Constants.EMPTY_STRING))
			return Constants.ZERO_INTEGER;
		resultado = Integer.parseInt(numero);
		return resultado;
	}
	
	/*
	 * Is Numeric
	 */
	
	public static boolean isInteger(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}
	
	public static boolean isDecimal(String cadena){
		try {
			Double.parseDouble(cadena);
			return true;
		} catch (NumberFormatException nfe){
			return false;
		}
	}

	/* Empty */

	public static boolean isEmptyList(List<?> obj) {
		if ((obj == null) || (obj.size() == Constants.ZERO_INTEGER)) {
			return true;
		}
		return false;
	}

	public static boolean isEmpty(String string) {
		return string == null || string.trim().isEmpty();
	}

	public static boolean isEmptyOrNull(String string) {
		return (isEmpty(string) || (string.trim().equalsIgnoreCase(Constants.NULL_STRING)));
	}

	public static String trimString(String string) {
		return isEmptyOrNull(string) ? Constants.EMPTY_STRING : string.trim();
	}

	public static boolean isEquals(String obj1, String obj2) {
		if (obj1.equalsIgnoreCase(obj2)) {
			return true;
		}
		return false;
	}

	public static String getString(String obj) {
		if (obj == null) {
			return Constants.EMPTY_STRING;
		} else {
			return obj;
		}
	}

	/*
	 * Files
	 * 
	 */
	public static Boolean createFolder(String folderName) {
		Boolean result = false;
		try {
			File file = new File(folderName);
			if (!file.exists()) {
				file.mkdir();
				result = true;
			} else {
				result = true;
			}
		} catch (SecurityException sex) {
			result = false;
			sex.getMessage();
		} catch (Exception e) {
			result = false;
			e.getMessage();
		}
		return result;
	}

	/*
	 * generate random
	 * 
	 */

	public static String generateRandomString(int length) {
		int leftLimit = 48; // numeral '0'
		int rightLimit = 122; // letter 'z'	
		Random random = new Random();

		String generatedString = random.ints(leftLimit, rightLimit + 1)
				.filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97)).limit(length)
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
		
		return generatedString;
	}

	public static String generateRandomStringByUUIDNoDash() {
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	
	/*
	 * Encrypt pay-me
	 * */
	
	public static String getStringSHA(String cadena) {
		try {

			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.reset();
			md.update(cadena.getBytes("utf8"));
			String ddddpa = String.format("%0128x", new BigInteger(1, md.digest()));
		
			return ddddpa;
		} catch (Exception e) {
			
			return e.getMessage();
		}

	}
	
	/*
	 * caracteres speciales
	 */
	
	public static String replaceSpecial(String cadena) {
		//String special= "[-+¡¿|.^:?-_;°#$%/()=!',]";
		String special= "[°]";
		try {
			if(cadena!=null){
				cadena=cadena.contains("á")?cadena.replaceAll("á","a"):cadena;
				cadena=cadena.contains("é")?cadena.replaceAll("é","e"):cadena;
				cadena=cadena.contains("í")?cadena.replaceAll("í","i"):cadena;
				cadena=cadena.contains("ó")?cadena.replaceAll("ó","o"):cadena;
				cadena=cadena.contains("ú")?cadena.replaceAll("ú","u"):cadena;
				
				cadena=cadena.contains("Á")?cadena.replaceAll("Á","A"):cadena;
				cadena=cadena.contains("É")?cadena.replaceAll("É","E"):cadena;
				cadena=cadena.contains("Í")?cadena.replaceAll("Í","I"):cadena;
				cadena=cadena.contains("Ó")?cadena.replaceAll("Ó","O"):cadena;
				cadena=cadena.contains("Ú")?cadena.replaceAll("Ú","U"):cadena;
				
				cadena=cadena.contains("Ñ")?cadena.replaceAll("Ñ","N"):cadena;
				cadena=cadena.contains("ñ")?cadena.replaceAll("ñ","n"):cadena;
				
				cadena=cadena.contains("!")?cadena.replaceAll("!",""):cadena;	
				cadena=cadena.contains("*")?cadena.replaceAll("*",""):cadena;	
				cadena=cadena.contains("¨")?cadena.replaceAll("¨",""):cadena;	
				cadena=cadena.contains("~")?cadena.replaceAll("~",""):cadena;	
				cadena=cadena.contains("¿")?cadena.replaceAll("¿",""):cadena;	
				cadena=cadena.contains("?")?cadena.replaceAll("?",""):cadena;	
				//cadena=cadena.contains("'")?cadena.replaceAll("!",""):cadena;		
				
				cadena=cadena.replaceAll(special, "");
				
				//logger.info("cadena special:"+cadena);
				//System.out.println("cadena special:"+cadena);	
			}
			
			
			return cadena;
		} catch (Exception e) {
			
			logger.error("error replaceSpecial:"+e.getMessage());
			
			return e.getMessage();
		}

	}
	
	public static String substringSpecial(String cadena,int length) {
		
		try {
			
			if(cadena!=null && cadena.length()>length) {
				cadena=cadena.substring(0,length);
				
				//logger.info("cadena mayor a "+length+", reduccion... "+cadena);
				//System.out.println("cadena substringSpecial:"+cadena);	
				
			}			
			
			return cadena;
		} catch (Exception e) {
			
			logger.error("error substringSpecial:"+e.getMessage());
			
			return e.getMessage();
		}
	}

}
