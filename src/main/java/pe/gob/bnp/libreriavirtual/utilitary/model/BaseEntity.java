package pe.gob.bnp.libreriavirtual.utilitary.model;

import java.util.Date;

public class BaseEntity {
	protected Long id;
	protected String 	idEnabled;
	
	/** * AUDIT */
	protected Date 		createdDate;
	protected String	createdUser;
	protected Date 		updatedDate;
	protected String  	updatedUser;	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIdEnabled() {
		return idEnabled;
	}
	public void setIdEnabled(String idEnabled) {
		this.idEnabled = idEnabled;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedUser() {
		return createdUser;
	}
	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

}
