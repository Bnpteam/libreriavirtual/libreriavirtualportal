package pe.gob.bnp.libreriavirtual.utilitary.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

@Repository
public interface UtilitaryRepository {
	
	public ResponseTransaction listMaster(String type);

	public ResponseTransaction listDepartment();
	
	public ResponseTransaction listProvince(String codeDpt);
	
	public ResponseTransaction listDistrict(String codeDpt,String codeProv);
	
	public ResponseTransaction listCategories();
	
	public ResponseTransaction listAuthors();
	
	public ResponseTransaction listYears();
	
	public ResponseTransaction getEmail(String templateName, String operationNumber);
	
	public ResponseTransaction setSend(String emailOrderId);
}
