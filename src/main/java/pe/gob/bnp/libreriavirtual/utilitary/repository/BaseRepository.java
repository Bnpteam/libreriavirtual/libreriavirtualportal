package pe.gob.bnp.libreriavirtual.utilitary.repository;


import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;


public interface BaseRepository<T> {
	
	public ResponseTransaction persist(T entity);
	public ResponseTransaction update(T entity);

}
