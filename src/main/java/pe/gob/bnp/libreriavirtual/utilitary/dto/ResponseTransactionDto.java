package pe.gob.bnp.libreriavirtual.utilitary.dto;

import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

public class ResponseTransactionDto {
	
	private int httpStatus;
	private ResponseTransaction response;
	
	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public ResponseTransaction getResponse() {
		return response;
	}

	public void setResponse(ResponseTransaction response) {
		this.response = response;
	}
}
