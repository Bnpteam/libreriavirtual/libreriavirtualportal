package pe.gob.bnp.libreriavirtual.utilitary.dto;

public class ResponseDto {
	
	private Object response;

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

}
