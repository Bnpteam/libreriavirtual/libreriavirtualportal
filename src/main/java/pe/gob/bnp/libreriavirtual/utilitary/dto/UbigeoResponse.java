package pe.gob.bnp.libreriavirtual.utilitary.dto;


public class UbigeoResponse {
	
	private String name;
	
	private String codeDepartment;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCodeDepartment() {
		return codeDepartment;
	}

	public void setCodeDepartment(String codeDepartment) {
		this.codeDepartment = codeDepartment;
	}

	public String getCodeProvince() {
		return codeProvince;
	}

	public void setCodeProvince(String codeProvince) {
		this.codeProvince = codeProvince;
	}

	public String getCodeDistrict() {
		return codeDistrict;
	}

	public void setCodeDistrict(String codeDistrict) {
		this.codeDistrict = codeDistrict;
	}

	private String codeProvince;
	
	private String codeDistrict;

}