package pe.gob.bnp.libreriavirtual.utilitary.common;

public class Constants {
	
	public static final String 	EMPTY_STRING = "";
	public static final String  NULL_STRING="null";
	public static final String 	ACTIVE_STATE="1";
	public static final String 	INACTIVE_STATE="0";
	public static final Long 	ZERO_LONG=0L;
	public static final Integer ZERO_INTEGER=0;
	public static final Double  ZERO_DOUBLE=0.00;

}
