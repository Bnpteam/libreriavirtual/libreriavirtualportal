package pe.gob.bnp.libreriavirtual.utilitary.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.libreriavirtual.config.DataSource;
//import pe.gob.bnp.libreriavirtual.portal.order.dto.CalculateResponse;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;
import pe.gob.bnp.libreriavirtual.utilitary.dto.AuthorResponse;
import pe.gob.bnp.libreriavirtual.utilitary.dto.CategoryResponse;
import pe.gob.bnp.libreriavirtual.utilitary.dto.MasterResponse;
import pe.gob.bnp.libreriavirtual.utilitary.dto.UbigeoResponse;
import pe.gob.bnp.libreriavirtual.utilitary.dto.YearResponse;
import pe.gob.bnp.libreriavirtual.utilitary.model.EmailTemplate;
import pe.gob.bnp.libreriavirtual.utilitary.repository.UtilitaryRepository;

@Repository
public class UtilitaryRepositoryJDBC implements UtilitaryRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(UtilitaryRepositoryJDBC.class);	
	
	private String messageSuccess="Transacción exitosa.";
	
	@Override
	public ResponseTransaction listMaster(String type) {
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_GET_LISTA_X_TIPO(?,?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setString("P_TYPE", type);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					MasterResponse ubigeoResponse = new MasterResponse();
					ubigeoResponse.setMasterId(Utility.parseLongToString(rs.getLong("MASTER_ID")));
					ubigeoResponse.setMasterName(Utility.getString(rs.getString("MASTER_NAME")));
					ubigeoResponse.setMasterDescription(Utility.getString(rs.getString("MASTER_DESCRIPTION")));
					ubigeoResponse.setMasterValue(Utility.getString(rs.getString("MASTER_VALUE")));
					
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_GET_LISTA_X_TIPO: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;
	}

	@Override
	public ResponseTransaction listDepartment() {
		
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_GET_UBIGEOS_DPTO(?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
		ResultSet rs = null;
		try  (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
							
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					UbigeoResponse ubigeoResponse = new UbigeoResponse();
					ubigeoResponse.setName(Utility.getString(rs.getString("UBIGEO_DESCRIPTION")));
					ubigeoResponse.setCodeDepartment(Utility.getString(rs.getString("CODE_DEPARTMENT")));
					ubigeoResponse.setCodeProvince(Utility.getString(rs.getString("CODE_PROVINCE")));
					ubigeoResponse.setCodeDistrict(Utility.getString(rs.getString("CODE_DISTRICT")));
					
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("SP_GET_UBIGEOS_DPTO: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}

	@Override
	public ResponseTransaction listProvince(String codeDpt) {

        String dBTransaction = "{ Call PKG_API_UTILITY.SP_GET_UBIGEOS_PROV(?,?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setString("P_CODE_DEPARTMENT", codeDpt);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					UbigeoResponse ubigeoResponse = new UbigeoResponse();
					ubigeoResponse.setName(Utility.getString(rs.getString("UBIGEO_DESCRIPTION")));
					ubigeoResponse.setCodeDepartment(Utility.getString(rs.getString("CODE_DEPARTMENT")));
					ubigeoResponse.setCodeProvince(Utility.getString(rs.getString("CODE_PROVINCE")));
					ubigeoResponse.setCodeDistrict(Utility.getString(rs.getString("CODE_DISTRICT")));
					
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("SP_GET_UBIGEOS_PROV: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;
	}

	@Override
	public ResponseTransaction listDistrict(String codeDpt,String codeProv) {
		
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_GET_UBIGEOS_DIST(?,?,?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setString("P_CODE_DEPARTMENT", codeDpt);
			cstm.setString("P_CODE_PROVINCE", codeProv);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					UbigeoResponse ubigeoResponse = new UbigeoResponse();
					ubigeoResponse.setName(Utility.getString(rs.getString("UBIGEO_DESCRIPTION")));
					ubigeoResponse.setCodeDepartment(Utility.getString(rs.getString("CODE_DEPARTMENT")));
					ubigeoResponse.setCodeProvince(Utility.getString(rs.getString("CODE_PROVINCE")));
					ubigeoResponse.setCodeDistrict(Utility.getString(rs.getString("CODE_DISTRICT")));
					
					slidersResponse.add(ubigeoResponse);
				}	
				response.setList(slidersResponse);
			}
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("SP_GET_UBIGEOS_DIST: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
		
		}
		return response;
	}

	@Override
	public ResponseTransaction listCategories() {
		
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_LIST_CATEGORY(?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
							
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					CategoryResponse categoryResponse = new CategoryResponse();
					categoryResponse.setId(Utility.parseLongToString(rs.getLong("CATEGORY_ID")));
					categoryResponse.setName(Utility.getString(rs.getString("CATEGORY_NAME")));
										
					slidersResponse.add(categoryResponse);
				}	
				response.setList(slidersResponse);
			}
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_LIST_CATEGORY: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
		
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction listAuthors() {		
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_LIST_AUTHOR(?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
        ResultSet rs = null;
		try  (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
						
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					AuthorResponse authorResponse = new AuthorResponse();
					authorResponse.setId(Utility.parseLongToString(rs.getLong("AUTHOR_ID")));
					authorResponse.setName(Utility.getString(rs.getString("AUTHOR_NAME")));
										
					slidersResponse.add(authorResponse);
				}	
				response.setList(slidersResponse);
			}
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_LIST_AUTHOR: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction listYears() {
		
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_LIST_YEAR(?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
		
				
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					YearResponse yearResponse = new YearResponse();
					yearResponse.setId(Utility.parseLongToString(rs.getLong("YEAR_ID")));
					yearResponse.setName(Utility.getString(rs.getString("YEAR_NAME")));
										
					slidersResponse.add(yearResponse);
				}	
				response.setList(slidersResponse);
			}
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_LIST_YEAR: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}

	@Override
	public ResponseTransaction getEmail(String templateName, String operationNumber) {
		
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_GET_EMAIL(?,?,?,?)}";
        
        List<Object> sharedsResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setString("P_TEMPLATE_NAME", Utility.getString(templateName));			
			cstm.setString("P_OPERATION_NUMBER",Utility.getString(operationNumber));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					EmailTemplate sharedResponse = new EmailTemplate();
					sharedResponse.setId(Utility.parseLongToString(rs.getLong("EMAIL_ID")));
					sharedResponse.setName(Utility.getString(rs.getString("EMAIL_NAME")));
					sharedResponse.setContent(Utility.getString(rs.getString("EMAIL_CONTENT")));
					sharedResponse.setSubject(Utility.getString(rs.getString("EMAIL_SUBJECT")));
					sharedResponse.setFrom(Utility.getString(rs.getString("EMAIL_FROM")));
					sharedResponse.setTo(Utility.getString(rs.getString("EMAIL_TO")));
					sharedResponse.setCo(Utility.getString(rs.getString("EMAIL_CO")));
					sharedResponse.setActive(Utility.getString(rs.getString("EMAIL_ACTIVE")));
					sharedResponse.setKey(Utility.getString(rs.getString("EMAIL_KEY")));
					sharedResponse.setEmailOrderId(Utility.getString(rs.getString("EMAIL_ORDER_ID")));
					sharedsResponse.add(sharedResponse);				
				
				}	
				response.setList(sharedsResponse);					
			}
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_GET_EMAIL: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;
	}

	@Override
	public ResponseTransaction setSend(String emailOrderId) {

		
        String dBTransaction = "{ Call PKG_API_UTILITY.SP_SEND_EMAIL(?,?)}";/*8*/
             
     
        ResponseTransaction response = new ResponseTransaction();
      
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			con.setAutoCommit(false);
			cstm.setString("P_ORDER_EMAIL",Utility.getString(emailOrderId));			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);	
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				con.commit();
				response.setResponse(messageSuccess);
					
			}else {
				con.rollback();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_UTILITY.SP_SEND_EMAIL: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}
	
}
