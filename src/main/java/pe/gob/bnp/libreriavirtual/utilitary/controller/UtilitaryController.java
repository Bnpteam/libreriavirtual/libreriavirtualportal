package pe.gob.bnp.libreriavirtual.utilitary.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseHandler;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.dto.CategoryResponse;
import pe.gob.bnp.libreriavirtual.utilitary.dto.MasterResponse;
import pe.gob.bnp.libreriavirtual.utilitary.dto.UbigeoResponse;
import pe.gob.bnp.libreriavirtual.utilitary.exception.EntityNotFoundResultException;
import pe.gob.bnp.libreriavirtual.utilitary.service.UtilitaryService;

@RestController
@RequestMapping("/api")
@Api(value = "/api/utility")
@CrossOrigin(origins = "*")
public class UtilitaryController {

	private static final Logger logger = LoggerFactory.getLogger(UtilitaryController.class);	

	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	UtilitaryService utilitaryService;
	

	@RequestMapping(method = RequestMethod.GET, path="/masters/{type}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar maestras de información", response= MasterResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
		@ApiResponse(code = 200, message = "Transacción finalizó con éxito. {type::TIPO_DOCUMENTO,TIPO_MATERIAL,TIPO_ENTREGA,TIPO_PUBLICACION,TIPO_ENVIO,TIPO_PAIS,TIPO_REGION,TIPO_COMPROBANTE,TIPO_ESTADO_PEDIDO}"),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarMaestras(
	 @ApiParam(value = "nombre del tipo de opción", required = true)
	 @PathVariable String type){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = utilitaryService.searchMaster(type);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("maestras EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("maestras IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("maestras Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}	
		
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/departments", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar departamentos", response= UbigeoResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarUbigeoDepartamentos(){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = utilitaryService.searchDepartment();
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("departments EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("departments IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("departments Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}	
		
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/provinces/{codeDepartment}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar provincias", response= UbigeoResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarUbigeoProvincias(			
			@ApiParam(value = "Código de departamento", required = true)
			@PathVariable String codeDepartment){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = utilitaryService.searchProvince(codeDepartment);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("provinces EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("provinces IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("provinces Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}		
		
	}	
			
	
	@RequestMapping(method = RequestMethod.GET, path="/districts/{codeDepartment}/{codeProvince}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar distritos", response= UbigeoResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarUbigeoDistritos(			
			@ApiParam(value = "Código de departamento", required = true)
			@PathVariable String codeDepartment,
			@ApiParam(value = "Código de provincia", required = true)
			@PathVariable String codeProvince){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = utilitaryService.searchDistrict(codeDepartment,codeProvince);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("provinces EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("provinces IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("provinces Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/categories", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar categorias de ejemplares", response= CategoryResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> searchCategories(){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = utilitaryService.searchCategories();
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("categories EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("categories IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("categories Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}	
		
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/authors", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar autores", response= CategoryResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> searchAuthors(){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = utilitaryService.searchAuthors();
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("authors EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("authors IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("authors Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}	
		
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/years", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar años de publicacion", response= CategoryResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> searchYears(){
		ResponseTransaction response = new ResponseTransaction();
		try {
			response = utilitaryService.searchYears();
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("years EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("years IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("years Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}	
		
	}	

}
