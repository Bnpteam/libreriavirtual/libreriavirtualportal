package pe.gob.bnp.libreriavirtual.admin.tray.dto;


public class TrayOrderDetailResponse {
	
	private String orderDetailId;
	private String bookId;
	private String bookImage;
	private String bookTitle;
	public String getOrderDetailId() {
		return orderDetailId;
	}
	public void setOrderDetailId(String orderDetailId) {
		this.orderDetailId = orderDetailId;
	}
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getBookImage() {
		return bookImage;
	}
	public void setBookImage(String bookImage) {
		this.bookImage = bookImage;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	public String getBookSubtitle() {
		return bookSubtitle;
	}
	public void setBookSubtitle(String bookSubtitle) {
		this.bookSubtitle = bookSubtitle;
	}
	public String getBookAuthorId() {
		return bookAuthorId;
	}
	public void setBookAuthorId(String bookAuthorId) {
		this.bookAuthorId = bookAuthorId;
	}
	public String getBookAuthorName() {
		return bookAuthorName;
	}
	public void setBookAuthorName(String bookAuthorName) {
		this.bookAuthorName = bookAuthorName;
	}
	public String getBookMaterialId() {
		return bookMaterialId;
	}
	public void setBookMaterialId(String bookMaterialId) {
		this.bookMaterialId = bookMaterialId;
	}
	public String getBookMaterialName() {
		return bookMaterialName;
	}
	public void setBookMaterialName(String bookMaterialName) {
		this.bookMaterialName = bookMaterialName;
	}
	public String getBookYearId() {
		return bookYearId;
	}
	public void setBookYearId(String bookYearId) {
		this.bookYearId = bookYearId;
	}
	public String getBookYearName() {
		return bookYearName;
	}
	public void setBookYearName(String bookYearName) {
		this.bookYearName = bookYearName;
	}
	public String getBookSizeId() {
		return bookSizeId;
	}
	public void setBookSizeId(String bookSizeId) {
		this.bookSizeId = bookSizeId;
	}
	public String getBookSizeName() {
		return bookSizeName;
	}
	public void setBookSizeName(String bookSizeName) {
		this.bookSizeName = bookSizeName;
	}
	public String getBookPrice() {
		return bookPrice;
	}
	public void setBookPrice(String bookPrice) {
		this.bookPrice = bookPrice;
	}
	public String getOrderDetailQuantity() {
		return orderDetailQuantity;
	}
	public void setOrderDetailQuantity(String orderDetailQuantity) {
		this.orderDetailQuantity = orderDetailQuantity;
	}
	public String getOrderDetailSubtotal() {
		return orderDetailSubtotal;
	}
	public void setOrderDetailSubtotal(String orderDetailSubtotal) {
		this.orderDetailSubtotal = orderDetailSubtotal;
	}
	private String bookSubtitle;
	private String bookAuthorId;
	private String bookAuthorName;
	private String bookMaterialId;
	private String bookMaterialName;
	private String bookYearId;
	private String bookYearName;
	private String bookSizeId;
	private String bookSizeName;
	private String bookPrice;
	private String orderDetailQuantity;
	private String orderDetailSubtotal;	

}
