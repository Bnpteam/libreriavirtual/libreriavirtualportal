package pe.gob.bnp.libreriavirtual.admin.auth.dto;

import pe.gob.bnp.libreriavirtual.admin.auth.model.AdminUser;
import pe.gob.bnp.libreriavirtual.utilitary.common.Constants;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

public class AdminUserDto {
	
	private String id;
	private String name;
	private String domain;
	private String enabled;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public String getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(String updatedUser) {
		this.updatedUser = updatedUser;
	}

	public String getFlagModuleUserIn() {
		return flagModuleUserIn;
	}

	public void setFlagModuleUserIn(String flagModuleUserIn) {
		this.flagModuleUserIn = flagModuleUserIn;
	}

	public String getFlagModuleUserOut() {
		return flagModuleUserOut;
	}

	public void setFlagModuleUserOut(String flagModuleUserOut) {
		this.flagModuleUserOut = flagModuleUserOut;
	}

	public String getFlagModuleTrayOrder() {
		return flagModuleTrayOrder;
	}

	public void setFlagModuleTrayOrder(String flagModuleTrayOrder) {
		this.flagModuleTrayOrder = flagModuleTrayOrder;
	}

	public String getFlagModuleConsult() {
		return flagModuleConsult;
	}

	public void setFlagModuleConsult(String flagModuleConsult) {
		this.flagModuleConsult = flagModuleConsult;
	}

	public String getFlagModulePublication() {
		return flagModulePublication;
	}

	public void setFlagModulePublication(String flagModulePublication) {
		this.flagModulePublication = flagModulePublication;
	}

	public String getFlagModuleProduct() {
		return flagModuleProduct;
	}

	public void setFlagModuleProduct(String flagModuleProduct) {
		this.flagModuleProduct = flagModuleProduct;
	}

	public String getFlagModuleBook() {
		return flagModuleBook;
	}

	public void setFlagModuleBook(String flagModuleBook) {
		this.flagModuleBook = flagModuleBook;
	}

	public String getFlagModuleInventory() {
		return flagModuleInventory;
	}

	public void setFlagModuleInventory(String flagModuleInventory) {
		this.flagModuleInventory = flagModuleInventory;
	}

	public String getFlagModuleMaster() {
		return flagModuleMaster;
	}

	public void setFlagModuleMaster(String flagModuleMaster) {
		this.flagModuleMaster = flagModuleMaster;
	}

	public String getFlagModuleReport() {
		return flagModuleReport;
	}

	public void setFlagModuleReport(String flagModuleReport) {
		this.flagModuleReport = flagModuleReport;
	}

	public String getFlagModuleDashboard() {
		return flagModuleDashboard;
	}

	public void setFlagModuleDashboard(String flagModuleDashboard) {
		this.flagModuleDashboard = flagModuleDashboard;
	}

	private String createdUser;
	private String updatedUser;	
	
	private String flagModuleUserIn;
	private String flagModuleUserOut;
	private String flagModuleTrayOrder;
	private String flagModuleConsult;
	private String flagModulePublication;	
	private String flagModuleProduct;
	private String flagModuleBook;
	private String flagModuleInventory;	
	private String flagModuleMaster;
	private String flagModuleReport;
	private String flagModuleDashboard;
	

	
	public AdminUserDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.name = Constants.EMPTY_STRING;
		this.domain = Constants.EMPTY_STRING;
		this.enabled = Constants.EMPTY_STRING;
		this.createdUser=Constants.EMPTY_STRING;
		this.updatedUser=Constants.EMPTY_STRING;		
		
		this.flagModuleUserIn= Constants.EMPTY_STRING;
		this.flagModuleUserOut= Constants.EMPTY_STRING;
		this.flagModuleTrayOrder= Constants.EMPTY_STRING;
		this.flagModuleConsult= Constants.EMPTY_STRING;
		this.flagModulePublication= Constants.EMPTY_STRING;
		this.flagModuleProduct= Constants.EMPTY_STRING;
		this.flagModuleBook= Constants.EMPTY_STRING;
		this.flagModuleInventory= Constants.EMPTY_STRING;
		this.flagModuleMaster= Constants.EMPTY_STRING;
		this.flagModuleReport= Constants.EMPTY_STRING;
		this.flagModuleDashboard= Constants.EMPTY_STRING;
		
		
	}	

	public AdminUserDto(AdminUser user) {
		super();
		this.id =Utility.parseLongToString(user.getId());
		this.name = user.getUserName();
		this.domain= user.getUserDomain();
		this.enabled = user.getIdEnabled();
		this.createdUser=user.getCreatedUser();
		this.updatedUser=user.getUpdatedUser();
		
		
		
		this.flagModuleUserIn= user.getFlagModuleUserIn();
		this.flagModuleUserOut= user.getFlagModuleUserOut();
		this.flagModuleTrayOrder= user.getFlagModuleTrayOrder();
		this.flagModuleConsult= user.getFlagModuleConsult();
		this.flagModulePublication= user.getFlagModulePublication();
		this.flagModuleProduct= user.getFlagModuleProduct();
		this.flagModuleBook= user.getFlagModuleBook();
		this.flagModuleInventory= user.getFlagModuleInventory();
		this.flagModuleMaster= user.getFlagModuleMaster();
		this.flagModuleReport=  user.getFlagModuleReport();
		this.flagModuleDashboard=  user.getFlagModuleDashboard();
		
	}
}
