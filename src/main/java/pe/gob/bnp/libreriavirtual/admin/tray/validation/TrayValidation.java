package pe.gob.bnp.libreriavirtual.admin.tray.validation;

import pe.gob.bnp.libreriavirtual.admin.tray.dto.StateOrderDto;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

public class TrayValidation {
	
	private static String messageUserSave = "No se encontraron datos para actualizar el pedido";	
	private static String messageOrderSave = "Se debe ingresar el id del pedido";	
	private static String messageStateActualSave = "Se debe ingresar el estado actual del pedido";	
	private static String messageStateNewSave = "Se debe ingresar el nuevo estado del pedido";	
	private static String messageAdminSave = "Se debe ingresar el usuario administrador";	
	private static String messageStatesSave="Se debe ingresar diferentes estados";
	
	public static Notification validationStateUpdate(StateOrderDto stateOrderDto) {

		Notification notification = new Notification();

		if (stateOrderDto == null) {
			notification.addError(messageUserSave);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(stateOrderDto.getOrderId())) {
			notification.addError(messageOrderSave);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(stateOrderDto.getStateActualId())) {
			notification.addError(messageStateActualSave);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(stateOrderDto.getStateNewId())) {
			notification.addError(messageStateNewSave);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(stateOrderDto.getAdminId())) {
			notification.addError(messageAdminSave);
			return notification;
		}
		
		if (stateOrderDto.getStateActualId().equalsIgnoreCase(stateOrderDto.getStateNewId())) {
			notification.addError(messageStatesSave);
			return notification;
		}

		return notification;
	}
	
	

}
