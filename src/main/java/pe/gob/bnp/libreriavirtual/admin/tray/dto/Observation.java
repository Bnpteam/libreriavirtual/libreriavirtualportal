package pe.gob.bnp.libreriavirtual.admin.tray.dto;


public class Observation {
		
	private String observationId;
	private String observationDescription;
	public String getObservationId() {
		return observationId;
	}
	public void setObservationId(String observationId) {
		this.observationId = observationId;
	}
	public String getObservationDescription() {
		return observationDescription;
	}
	public void setObservationDescription(String observationDescription) {
		this.observationDescription = observationDescription;
	}
	public String getUserAdmin() {
		return userAdmin;
	}
	public void setUserAdmin(String userAdmin) {
		this.userAdmin = userAdmin;
	}
	private String userAdmin;

}
