package pe.gob.bnp.libreriavirtual.admin.auth.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.libreriavirtual.admin.auth.dto.AdminCredential;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.UserRequest;
import pe.gob.bnp.libreriavirtual.admin.auth.model.AdminUser;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.repository.BaseRepository;

@Repository
public interface AdminUserRepository  extends BaseRepository<AdminUser>{
	
	public ResponseTransaction loginAdmin(AdminCredential credential);
	
	public ResponseTransaction persistAdmin(AdminUser entity);
	
	public ResponseTransaction updateAdmin(AdminUser entity);
	
	public ResponseTransaction readAdmin(Long id);
	
	public ResponseTransaction list(UserRequest userAdmin);

}
