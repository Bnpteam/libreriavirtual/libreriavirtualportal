package pe.gob.bnp.libreriavirtual.admin.tray.service;


import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.libreriavirtual.admin.tray.dto.StateOrderDto;
import pe.gob.bnp.libreriavirtual.admin.tray.exception.TrayException;
import pe.gob.bnp.libreriavirtual.admin.tray.repository.TrayRepository;
import pe.gob.bnp.libreriavirtual.admin.tray.validation.TrayValidation;
import pe.gob.bnp.libreriavirtual.portal.order.exception.OrderException;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.service.UtilitaryService;

@Service
public class TrayService {
	
	private static final Logger logger = LoggerFactory.getLogger(TrayService.class);	
	
	@Autowired
	TrayRepository trayRepository;
	
	@Autowired
	UtilitaryService utilityService;
	
	public ResponseTransaction listTrayOrders(String dateIni,String dateFin,String nroOrder,String nroDocument,String stateId) throws IOException {

		ResponseTransaction response = trayRepository.listOrder(dateIni,dateFin,nroOrder,nroDocument,stateId);
		
		return TrayException.setMessageResponseListOrder(response);
	}
	
	public ResponseTransaction updateState(Long id, StateOrderDto stateOrderDto) {

		Notification notification = TrayValidation.validationStateUpdate(stateOrderDto);
		ResponseTransaction response = new ResponseTransaction();
		
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		//Address user = addressMapper.reverseMapperUpdate(id, addressDto);	
		stateOrderDto.setOrderId(id+"");
		
		response = trayRepository.updateSate(stateOrderDto);
		
		if (response!= null && response.getCodeResponse()!=null && response.getCodeResponse().equals("0000")){
			if(stateOrderDto.getStateNewId().equalsIgnoreCase("27")) {//pendiente entrega
				logger.info("Enviando email EMAIL_INWAY... ini");						
				String templateName="EMAIL_INWAY";						
				utilityService.sendEmailLV(templateName,stateOrderDto.getOrderId());
				logger.info("Enviando email EMAIL_INWAY... fin");
			}
			
			if(stateOrderDto.getStateNewId().equalsIgnoreCase("29")) {//entregado
				logger.info("Enviando email EMAIL_DELIVERED... ini");						
				String templateName="EMAIL_DELIVERED";						
				utilityService.sendEmailLV(templateName,stateOrderDto.getOrderId());
				logger.info("Enviando email EMAIL_DELIVERED... fin");
			}
			
		}
		
		
		
		return TrayException.setMessageResponseSateUpdate(response);

	}	
	
	public ResponseTransaction getOrderCab(Long orderId) throws IOException {

		ResponseTransaction response = trayRepository.getOrderCab(orderId);
		
		return OrderException.setMessageResponseGetOrder(response);
	}
	
	public ResponseTransaction getOrderDetail(Long orderId) throws IOException {

		ResponseTransaction response = trayRepository.getOrderDetail(orderId);
		
		return TrayException.setMessageResponseGetOrder(response);
	}
	
	public ResponseTransaction getOrderObservation(Long orderId) throws IOException {

		ResponseTransaction response = trayRepository.getOrderObservation(orderId);
		
		return TrayException.setMessageResponseGetOrder(response);
	}
	
	public ResponseTransaction getStates(Long stateId) throws IOException {

		ResponseTransaction response = trayRepository.getStates(stateId);
		
		return TrayException.setMessageResponseGetState(response);
	}
}
