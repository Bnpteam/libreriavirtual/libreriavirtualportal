package pe.gob.bnp.libreriavirtual.admin.auth.exception;

import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

public class AdminUserException {
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";
	
	public static final  String error0002LoginAdmin ="Error 0002:El dominio ingresado no existe.";
	public static final  String error0003LoginAdmin ="Error 0003: El usuario esta inactivo.";
	
	public static final  String error0001Admin ="El nombre del Usuario ya se encuentra registrado.";
	public static final  String error0002Admin ="El dominio del Usuario ya se encuentra registrado.";
	public static final  String error0003Admin ="No se puede realizar la actualización: El id del Usuario no pertenece a ningun usuario registrado.";

	
	public static ResponseTransaction setMessageResponseLoginAdmin(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) { 
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002LoginAdmin);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003LoginAdmin);
		}

		return response;
	} 
	
	public static ResponseTransaction setMessageResponseSaveAdmin(ResponseTransaction response) {
		
		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001Admin);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002Admin);
		}

		return response;
	}
	
	public static ResponseTransaction setMessageResponseUpdateAdmin(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001Admin);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002Admin);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003Admin);
		}

		return response;
	}

}
