package pe.gob.bnp.libreriavirtual.admin.auth.service;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.libreriavirtual.admin.auth.dto.AdminCredential;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.AdminUserDto;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.UserRequest;
import pe.gob.bnp.libreriavirtual.admin.auth.exception.AdminUserException;
import pe.gob.bnp.libreriavirtual.admin.auth.mapper.AdminUserMapper;
import pe.gob.bnp.libreriavirtual.admin.auth.model.AdminUser;
import pe.gob.bnp.libreriavirtual.admin.auth.repository.AdminUserRepository;
import pe.gob.bnp.libreriavirtual.admin.auth.validation.AdminUserValidation;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

@Service
public class AuthAdminService {
	
	@Autowired
	AdminUserRepository userRepository;
	
	@Autowired
	AdminUserMapper adminUserMapper;

	public ResponseTransaction loginAdmin(AdminCredential credential){
		
		ResponseTransaction response = new ResponseTransaction();
		
		Notification notification = AdminUserValidation.validation(credential);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}	
		
		ResponseTransaction responseAD= this.validarUsuarioenActiveDirectory(credential);
		
		Notification notificationAD = AdminUserValidation.validation(responseAD);
		if (notificationAD.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notificationAD.errorMessage());
			return response;
			//throw new IllegalArgumentException(notificationAD.errorMessage());
		}
		
		response=userRepository.loginAdmin(credential);
		
		return AdminUserException.setMessageResponseLoginAdmin(response);	
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	public ResponseTransaction validarUsuarioenActiveDirectory(AdminCredential credencial){//aqui viene la integración con Active Directory
		
		ResponseTransaction response = null;

		@SuppressWarnings("rawtypes")
		Hashtable credenciales = new Hashtable(11);
		DirContext contexto = null;
		try {
			credenciales.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
			credenciales.put(Context.SECURITY_AUTHENTICATION,"simple");
			//if( credencial.getDomain()!=null &&  credencial.getDomain().contains("@bnp.gob.pe"))
			//	credenciales.put(Context.SECURITY_PRINCIPAL, credencial.getDomain());
			//else
				credenciales.put(Context.SECURITY_PRINCIPAL, credencial.getDomain()+"@bnp.gob.pe");
			credenciales.put(Context.SECURITY_CREDENTIALS, credencial.getPassword());
			credenciales.put(Context.PROVIDER_URL, "ldap://172.16.88.203:389");
			
			contexto = new InitialDirContext(credenciales);
			response = new ResponseTransaction();
			if (contexto!=null) {
				response.setCodeResponse("0000");
				response.setResponse("El usuario y contraseña son correctos.");
			}else {
				response.setCodeResponse("0001");
				response.setResponse("El nombre de usuario o contraseña no es correcto.");
			}
		} catch (NamingException e) {
			response = new ResponseTransaction();
			this.obtenerMotivoRechazoEnAD(response,new String(e.toString()));
			//System.out.println("Error en e.getMessage: "+e.getMessage());
			/*e.printStackTrace();*/
		} finally {
			if (contexto!=null) {
				try {
					contexto.close();
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}
		}
		return response;
	}	
	
	public void obtenerMotivoRechazoEnAD(ResponseTransaction response, String cadena) {
		int pos = cadena.indexOf("data ");
		String descripcionRechazo = "";
		if (pos>0) {
			String campo1= cadena.substring(pos+5,pos+5+3).concat("");
			if (campo1.equals("52e")){
				//System.out.println("Valor 52e SI es igual a "+campo1);
				descripcionRechazo = "El nombre de usuario o contraseña no es correcto.";
			}	
			if (campo1.equals("775")){
				//System.out.println("Valor 52e SI es igual a "+campo1);
				descripcionRechazo = "La cuenta a que se hace referencia está bloqueada y no se puede utilizar.";
			}			
			if (campo1.equals("525​")){
				descripcionRechazo = "Usuario no existe.";
			}

			if (campo1.equals("530​") || campo1.equals("531​")){
				descripcionRechazo = "Intento de logueo no permitido.";
			}			
			if (campo1.equals("532​")){
				descripcionRechazo = "Contraseña expirada.";
			}	
			if (campo1.equals("533​")){
				descripcionRechazo = "Cuenta deshabilitada.";
			}			
			if (campo1.equals("701​")){
				descripcionRechazo = "Cuenta expirada.";
			}
			if (campo1.equals("773​")){
				descripcionRechazo = "Debe resetear su contraseña.";
			}				
		
			response.setCodeResponse(campo1);
			response.setResponse(descripcionRechazo);
		}else {
			response.setCodeResponse("999");
			response.setResponse("No se pudo obtener él código de rechazo");
		}
	}	
	
	
	public ResponseTransaction signupAdmin(AdminUserDto userDto) {

		Notification notification = AdminUserValidation.validation(userDto);
		ResponseTransaction response = new ResponseTransaction();

		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		AdminUser user = adminUserMapper.reverseMapperSave(userDto);
		
		response = userRepository.persistAdmin(user);
		
		return AdminUserException.setMessageResponseSaveAdmin(response);

	}
	
	public ResponseTransaction updateAdmin(Long id, AdminUserDto userDto) {

		Notification notification = AdminUserValidation.validation(userDto);
		ResponseTransaction response = new ResponseTransaction();
		
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		AdminUser user = adminUserMapper.reverseMapperUpdate(id, userDto);	
		
		response = userRepository.updateAdmin(user);	
		
		return AdminUserException.setMessageResponseUpdateAdmin(response);

	}
	
	public ResponseTransaction getAdmin(Long id) {
		
		ResponseTransaction response = new ResponseTransaction();
		
		Notification notification = AdminUserValidation.validation(id);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}
		return userRepository.readAdmin(id);
	}
	
	
	public ResponseTransaction searchAll(String keyword){
		 UserRequest userRequest = new UserRequest();
		userRequest.setKeyword(keyword);
		
		Notification notification = AdminUserValidation.validation(userRequest);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}		
		return userRepository.list(userRequest);
	}
	

}
