package pe.gob.bnp.libreriavirtual.admin.tray.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.libreriavirtual.admin.tray.dto.StateOrderDto;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

@Repository
public interface TrayRepository {
	
	public ResponseTransaction listOrder(String dateIni,String dateFin,String nroOrder,String nroDocument,String stateId);
	
	public ResponseTransaction updateSate(StateOrderDto entity);
	
	public ResponseTransaction getOrderCab(Long id);
	
	public ResponseTransaction getOrderDetail(Long id);
	
	public ResponseTransaction getOrderObservation(Long id);
	
	public ResponseTransaction getStates(Long stateId);

}
