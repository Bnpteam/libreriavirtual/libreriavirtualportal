package pe.gob.bnp.libreriavirtual.admin.auth.dto;

public class UserRequest {
		
	private String keyword;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

}
