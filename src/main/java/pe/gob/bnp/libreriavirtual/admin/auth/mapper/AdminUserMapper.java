package pe.gob.bnp.libreriavirtual.admin.auth.mapper;

import org.springframework.stereotype.Component;

import pe.gob.bnp.libreriavirtual.admin.auth.dto.AdminUserDto;
import pe.gob.bnp.libreriavirtual.admin.auth.model.AdminUser;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

@Component
public class AdminUserMapper {
	
	public AdminUser reverseMapperSave(AdminUserDto userDto) {
		AdminUser user = new AdminUser();	
		
		user.setUserName(userDto.getName());;
		user.setUserDomain(userDto.getDomain());
		user.setCreatedUser(userDto.getCreatedUser());
		
		
		//Module Usuarios Internos
		if(userDto.getFlagModuleUserIn().contains("true")){
			userDto.setFlagModuleUserIn("1");
		}
		if(userDto.getFlagModuleUserIn().contains("false")) {
			userDto.setFlagModuleUserIn("0");
		}
		
		//Module Usuarios Externos
		if(userDto.getFlagModuleUserOut().contains("true")){
			userDto.setFlagModuleUserOut("1");
		}
		if(userDto.getFlagModuleUserOut().contains("false")) {
			userDto.setFlagModuleUserOut("0");
		}
		
		//Module Bandeja Pedidos
		if(userDto.getFlagModuleTrayOrder().contains("true")){
			userDto.setFlagModuleTrayOrder("1");
		}
		if(userDto.getFlagModuleTrayOrder().contains("false")) {
			userDto.setFlagModuleTrayOrder("0");
		}
		
		//Module Consultas
		if(userDto.getFlagModuleConsult().contains("true")){
			userDto.setFlagModuleConsult("1");
		}
		if(userDto.getFlagModuleConsult().contains("false")) {
			userDto.setFlagModuleConsult("0");
		}
		
		//Module Publicaciones
		if(userDto.getFlagModulePublication().contains("true")){
			userDto.setFlagModulePublication("1");
		}
		if(userDto.getFlagModulePublication().contains("false")) {
			userDto.setFlagModulePublication("0");
		}
		
		//Module Productos
		if(userDto.getFlagModuleProduct().contains("true")){
			userDto.setFlagModuleProduct("1");
		}
		if(userDto.getFlagModuleProduct().contains("false")) {
			userDto.setFlagModuleProduct("0");
		}
		
		//Module Libros
		if(userDto.getFlagModuleBook().contains("true")){
			userDto.setFlagModuleBook("1");
		}
		if(userDto.getFlagModuleBook().contains("false")) {
			userDto.setFlagModuleBook("0");
		}
		
		//Module Inventarios
		if(userDto.getFlagModuleInventory().contains("true")){
			userDto.setFlagModuleInventory("1");
		}
		if(userDto.getFlagModuleInventory().contains("false")) {
			userDto.setFlagModuleInventory("0");
		}
		
		//Module maestras
		if(userDto.getFlagModuleMaster().contains("true")){
			userDto.setFlagModuleMaster("1");
		}
		if(userDto.getFlagModuleMaster().contains("false")) {
			userDto.setFlagModuleMaster("0");
		}
		
		//Module reportes
		if(userDto.getFlagModuleReport().contains("true")){
			userDto.setFlagModuleReport("1");
		}
		if(userDto.getFlagModuleReport().contains("false")) {
			userDto.setFlagModuleReport("0");
		}
		
		//Module dashboard
		if(userDto.getFlagModuleDashboard().contains("true")){
			userDto.setFlagModuleDashboard("1");
		}
		if(userDto.getFlagModuleDashboard().contains("false")) {
			userDto.setFlagModuleDashboard("0");
		}			
		
		user.setFlagModuleUserIn(userDto.getFlagModuleUserIn());
		user.setFlagModuleUserOut(userDto.getFlagModuleUserOut());
		user.setFlagModuleTrayOrder(userDto.getFlagModuleTrayOrder());
		user.setFlagModuleConsult(userDto.getFlagModuleConsult());	
		user.setFlagModulePublication(userDto.getFlagModulePublication());
		
		user.setFlagModuleProduct(userDto.getFlagModuleProduct());
		user.setFlagModuleBook(userDto.getFlagModuleBook());
		user.setFlagModuleInventory(userDto.getFlagModuleInventory());
		user.setFlagModuleMaster(userDto.getFlagModuleMaster());	
		user.setFlagModuleReport(userDto.getFlagModuleReport());
		user.setFlagModuleDashboard(userDto.getFlagModuleDashboard());
		
		
		return user;
	}
	

	
	public AdminUser reverseMapperUpdate(Long id,AdminUserDto userDto) {
		AdminUser user = new AdminUser();	
		
		userDto.setId(Utility.parseLongToString(id));
		user.setId(Utility.parseStringToLong(userDto.getId()));
		
		user.setUserName(userDto.getName());;
		user.setUserDomain(userDto.getDomain());
		user.setIdEnabled(userDto.getEnabled());	
		user.setUpdatedUser(userDto.getUpdatedUser());
		

		//Module Usuarios Internos
		if(userDto.getFlagModuleUserIn().contains("true")){
			userDto.setFlagModuleUserIn("1");
		}
		if(userDto.getFlagModuleUserIn().contains("false")) {
			userDto.setFlagModuleUserIn("0");
		}
		
		//Module Usuarios Externos
		if(userDto.getFlagModuleUserOut().contains("true")){
			userDto.setFlagModuleUserOut("1");
		}
		if(userDto.getFlagModuleUserOut().contains("false")) {
			userDto.setFlagModuleUserOut("0");
		}
		
		//Module Bandeja Pedidos
		if(userDto.getFlagModuleTrayOrder().contains("true")){
			userDto.setFlagModuleTrayOrder("1");
		}
		if(userDto.getFlagModuleTrayOrder().contains("false")) {
			userDto.setFlagModuleTrayOrder("0");
		}
		
		//Module Consultas
		if(userDto.getFlagModuleConsult().contains("true")){
			userDto.setFlagModuleConsult("1");
		}
		if(userDto.getFlagModuleConsult().contains("false")) {
			userDto.setFlagModuleConsult("0");
		}
		
		//Module Publicaciones
		if(userDto.getFlagModulePublication().contains("true")){
			userDto.setFlagModulePublication("1");
		}
		if(userDto.getFlagModulePublication().contains("false")) {
			userDto.setFlagModulePublication("0");
		}
		
		//Module Productos
		if(userDto.getFlagModuleProduct().contains("true")){
			userDto.setFlagModuleProduct("1");
		}
		if(userDto.getFlagModuleProduct().contains("false")) {
			userDto.setFlagModuleProduct("0");
		}
		
		//Module Libros
		if(userDto.getFlagModuleBook().contains("true")){
			userDto.setFlagModuleBook("1");
		}
		if(userDto.getFlagModuleBook().contains("false")) {
			userDto.setFlagModuleBook("0");
		}
		
		//Module Inventarios
		if(userDto.getFlagModuleInventory().contains("true")){
			userDto.setFlagModuleInventory("1");
		}
		if(userDto.getFlagModuleInventory().contains("false")) {
			userDto.setFlagModuleInventory("0");
		}
		
		//Module maestras
		if(userDto.getFlagModuleMaster().contains("true")){
			userDto.setFlagModuleMaster("1");
		}
		if(userDto.getFlagModuleMaster().contains("false")) {
			userDto.setFlagModuleMaster("0");
		}
		
		//Module reportes
		if(userDto.getFlagModuleReport().contains("true")){
			userDto.setFlagModuleReport("1");
		}
		if(userDto.getFlagModuleReport().contains("false")) {
			userDto.setFlagModuleReport("0");
		}
		
		//Module dashboard
		if(userDto.getFlagModuleDashboard().contains("true")){
			userDto.setFlagModuleDashboard("1");
		}
		if(userDto.getFlagModuleDashboard().contains("false")) {
			userDto.setFlagModuleDashboard("0");
		}			
		
		user.setFlagModuleUserIn(userDto.getFlagModuleUserIn());
		user.setFlagModuleUserOut(userDto.getFlagModuleUserOut());
		user.setFlagModuleTrayOrder(userDto.getFlagModuleTrayOrder());
		user.setFlagModuleConsult(userDto.getFlagModuleConsult());	
		user.setFlagModulePublication(userDto.getFlagModulePublication());
		
		user.setFlagModuleProduct(userDto.getFlagModuleProduct());
		user.setFlagModuleBook(userDto.getFlagModuleBook());
		user.setFlagModuleInventory(userDto.getFlagModuleInventory());
		user.setFlagModuleMaster(userDto.getFlagModuleMaster());	
		user.setFlagModuleReport(userDto.getFlagModuleReport());
		user.setFlagModuleDashboard(userDto.getFlagModuleDashboard());
		
		return user;
	}

}
