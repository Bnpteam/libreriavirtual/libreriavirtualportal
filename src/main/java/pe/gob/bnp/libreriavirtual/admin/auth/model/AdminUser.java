package pe.gob.bnp.libreriavirtual.admin.auth.model;

import pe.gob.bnp.libreriavirtual.utilitary.common.Constants;
import pe.gob.bnp.libreriavirtual.utilitary.model.BaseEntity;

public class AdminUser extends BaseEntity{
	
	private String userName;
	private String userDomain;
	
	private String flagModuleUserIn;
	private String flagModuleUserOut;
	private String flagModuleTrayOrder;
	private String flagModuleConsult;
	private String flagModulePublication;	
	private String flagModuleProduct;
	private String flagModuleBook;
	private String flagModuleInventory;	
	private String flagModuleMaster;
	private String flagModuleReport;
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserDomain() {
		return userDomain;
	}

	public void setUserDomain(String userDomain) {
		this.userDomain = userDomain;
	}

	public String getFlagModuleUserIn() {
		return flagModuleUserIn;
	}

	public void setFlagModuleUserIn(String flagModuleUserIn) {
		this.flagModuleUserIn = flagModuleUserIn;
	}

	public String getFlagModuleUserOut() {
		return flagModuleUserOut;
	}

	public void setFlagModuleUserOut(String flagModuleUserOut) {
		this.flagModuleUserOut = flagModuleUserOut;
	}

	public String getFlagModuleTrayOrder() {
		return flagModuleTrayOrder;
	}

	public void setFlagModuleTrayOrder(String flagModuleTrayOrder) {
		this.flagModuleTrayOrder = flagModuleTrayOrder;
	}

	public String getFlagModuleConsult() {
		return flagModuleConsult;
	}

	public void setFlagModuleConsult(String flagModuleConsult) {
		this.flagModuleConsult = flagModuleConsult;
	}

	public String getFlagModulePublication() {
		return flagModulePublication;
	}

	public void setFlagModulePublication(String flagModulePublication) {
		this.flagModulePublication = flagModulePublication;
	}

	public String getFlagModuleProduct() {
		return flagModuleProduct;
	}

	public void setFlagModuleProduct(String flagModuleProduct) {
		this.flagModuleProduct = flagModuleProduct;
	}

	public String getFlagModuleBook() {
		return flagModuleBook;
	}

	public void setFlagModuleBook(String flagModuleBook) {
		this.flagModuleBook = flagModuleBook;
	}

	public String getFlagModuleInventory() {
		return flagModuleInventory;
	}

	public void setFlagModuleInventory(String flagModuleInventory) {
		this.flagModuleInventory = flagModuleInventory;
	}

	public String getFlagModuleMaster() {
		return flagModuleMaster;
	}

	public void setFlagModuleMaster(String flagModuleMaster) {
		this.flagModuleMaster = flagModuleMaster;
	}

	public String getFlagModuleReport() {
		return flagModuleReport;
	}

	public void setFlagModuleReport(String flagModuleReport) {
		this.flagModuleReport = flagModuleReport;
	}

	public String getFlagModuleDashboard() {
		return flagModuleDashboard;
	}

	public void setFlagModuleDashboard(String flagModuleDashboard) {
		this.flagModuleDashboard = flagModuleDashboard;
	}

	private String flagModuleDashboard;
	
	public AdminUser() {
		super();
		this.userName = Constants.EMPTY_STRING;
		this.userDomain = Constants.EMPTY_STRING;
		
		this.flagModuleUserIn= Constants.EMPTY_STRING;
		this.flagModuleUserOut= Constants.EMPTY_STRING;
		this.flagModuleTrayOrder= Constants.EMPTY_STRING;
		this.flagModuleConsult=Constants.EMPTY_STRING;
		
		this.flagModulePublication= Constants.EMPTY_STRING;
		this.flagModuleProduct= Constants.EMPTY_STRING;
		this.flagModuleBook= Constants.EMPTY_STRING;
		this.flagModuleInventory=Constants.EMPTY_STRING;
		
		this.flagModuleMaster= Constants.EMPTY_STRING;
		this.flagModuleReport= Constants.EMPTY_STRING;
		this.flagModuleDashboard= Constants.EMPTY_STRING;
		
	}
}
