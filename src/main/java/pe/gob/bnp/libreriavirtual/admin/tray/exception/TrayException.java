package pe.gob.bnp.libreriavirtual.admin.tray.exception;

import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

public class TrayException {
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";
	public static final  String error0002 ="El cliente no existe";
	public static final  String error0008 ="El id del pedido no existe";
	public static final  String errorState0008 ="El id del pedido no existe";
	public static final  String errorState0011 ="El id del estado no existe";
	public static final  String errorState0012 ="El id del estado no existe";
	public static final  String errorState0013 ="El usuario administrador no existe o esta desactivado";
	public static final  String errorState0014 ="El estado actual del pedido ha sido actualizado, refrescar la consulta de pedidos...";
	
	public static ResponseTransaction setMessageResponseListOrder(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}		
		
		return response;
	}
	
	public static ResponseTransaction setMessageResponseSateUpdate(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0008")) {
			response.setResponse(errorState0008);
		}
		
		if (response.getCodeResponse().equals("0011")) {
			response.setResponse(errorState0011);
		}
		
		if (response.getCodeResponse().equals("0012")) {
			response.setResponse(errorState0012);
		}
		
		if (response.getCodeResponse().equals("0013")) {
			response.setResponse(errorState0013);
		}
		
		if (response.getCodeResponse().equals("0014")) {
			response.setResponse(errorState0014);
		}

		return response;
	}
	
	public static ResponseTransaction setMessageResponseGetOrder(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0008")) {
			response.setResponse(error0008);
		}		
		
		return response;
	}
	
	public static ResponseTransaction setMessageResponseGetState(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0011")) {
			response.setResponse(errorState0011);
		}		
		
		return response;
	}

}
