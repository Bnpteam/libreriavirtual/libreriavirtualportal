package pe.gob.bnp.libreriavirtual.admin.tray.dto;


public class TrayOrderObservationResponse {
	
	private String orderId;
	private String observationId;
	private String observationDescription;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getObservationId() {
		return observationId;
	}
	public void setObservationId(String observationId) {
		this.observationId = observationId;
	}
	public String getObservationDescription() {
		return observationDescription;
	}
	public void setObservationDescription(String observationDescription) {
		this.observationDescription = observationDescription;
	}
	public String getAdminId() {
		return adminId;
	}
	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}
	public String getAdminName() {
		return adminName;
	}
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	private String adminId;
	private String adminName;
	

}
