package pe.gob.bnp.libreriavirtual.admin.auth.dto;

public class UserDetailAdminResponse {
	
	private String id;
	private String name;
	private String domain;
	private String enabled;
	private String  createdDate;
	private String lastLoginDate;
	
	private String flagModuleUserIn;
	private String flagModuleUserOut;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getLastLoginDate() {
		return lastLoginDate;
	}
	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	public String getFlagModuleUserIn() {
		return flagModuleUserIn;
	}
	public void setFlagModuleUserIn(String flagModuleUserIn) {
		this.flagModuleUserIn = flagModuleUserIn;
	}
	public String getFlagModuleUserOut() {
		return flagModuleUserOut;
	}
	public void setFlagModuleUserOut(String flagModuleUserOut) {
		this.flagModuleUserOut = flagModuleUserOut;
	}
	public String getFlagModuleTrayOrder() {
		return flagModuleTrayOrder;
	}
	public void setFlagModuleTrayOrder(String flagModuleTrayOrder) {
		this.flagModuleTrayOrder = flagModuleTrayOrder;
	}
	public String getFlagModuleConsult() {
		return flagModuleConsult;
	}
	public void setFlagModuleConsult(String flagModuleConsult) {
		this.flagModuleConsult = flagModuleConsult;
	}
	public String getFlagModulePublication() {
		return flagModulePublication;
	}
	public void setFlagModulePublication(String flagModulePublication) {
		this.flagModulePublication = flagModulePublication;
	}
	public String getFlagModuleProduct() {
		return flagModuleProduct;
	}
	public void setFlagModuleProduct(String flagModuleProduct) {
		this.flagModuleProduct = flagModuleProduct;
	}
	public String getFlagModuleBook() {
		return flagModuleBook;
	}
	public void setFlagModuleBook(String flagModuleBook) {
		this.flagModuleBook = flagModuleBook;
	}
	public String getFlagModuleInventory() {
		return flagModuleInventory;
	}
	public void setFlagModuleInventory(String flagModuleInventory) {
		this.flagModuleInventory = flagModuleInventory;
	}
	public String getFlagModuleMaster() {
		return flagModuleMaster;
	}
	public void setFlagModuleMaster(String flagModuleMaster) {
		this.flagModuleMaster = flagModuleMaster;
	}
	public String getFlagModuleReport() {
		return flagModuleReport;
	}
	public void setFlagModuleReport(String flagModuleReport) {
		this.flagModuleReport = flagModuleReport;
	}
	public String getFlagModuleDashboard() {
		return flagModuleDashboard;
	}
	public void setFlagModuleDashboard(String flagModuleDashboard) {
		this.flagModuleDashboard = flagModuleDashboard;
	}
	private String flagModuleTrayOrder;
	private String flagModuleConsult;
	private String flagModulePublication;	
	private String flagModuleProduct;
	private String flagModuleBook;
	private String flagModuleInventory;	
	private String flagModuleMaster;
	private String flagModuleReport;
	private String flagModuleDashboard;
}
