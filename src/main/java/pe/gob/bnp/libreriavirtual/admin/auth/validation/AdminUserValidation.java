package pe.gob.bnp.libreriavirtual.admin.auth.validation;

import pe.gob.bnp.libreriavirtual.admin.auth.dto.AdminCredential;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.AdminUserDto;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.UserRequest;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

public class AdminUserValidation {
	
	private static String messageUserLogin = "No se encontraron datos de acceso";
	private static String messageUserSave = "No se encontraron datos del Usuario";
	private static String messageNameSave = "Se debe ingresar un nombre de Usuario";	
	private static String messageDomainSave = "Se debe ingresar un dominio de Usuario";
	private static String messageUserSearchAll = "No se encontró el filtro de búsqueda";
	private static String messageUserGet = "El id debe ser mayor a cero";
	
	
	public static Notification validation(UserRequest dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageUserSearchAll);
			return notification;
		}

		return notification;
	}
	
	public static Notification validation(AdminUserDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageUserSave);
			return notification;
		}

		if (Utility.isEmptyOrNull(dto.getName())) {
			notification.addError(messageNameSave);
		}
		
		if (Utility.isEmptyOrNull(dto.getDomain())) {
			notification.addError(messageDomainSave);
		}

		return notification;
	}
	
	public static Notification validation(AdminCredential credential) {

		Notification notification = new Notification();

		if (credential == null) {
			notification.addError(messageUserLogin);
			return notification;
		}

		if (Utility.isEmptyOrNull(credential.getDomain())) {
			notification.addError(messageDomainSave);
		}

		return notification;
	}
	
	public static Notification validation(ResponseTransaction response) {
		Notification notification = new Notification();
		if (response == null) {
			notification.addError("ResponseTx es nulo");
			return notification;
		}

		if (response.getCodeResponse()== null || !response.getCodeResponse().equals("0000")) {
			notification.addError(response.getResponse());
			return notification;
		}
		return notification;
	}
	
	public static Notification validation(Long id) {
		Notification notification = new Notification();
		if (id <=0L) {
			notification.addError(messageUserGet);
			return notification;
		}		
		return notification;
	}

}
