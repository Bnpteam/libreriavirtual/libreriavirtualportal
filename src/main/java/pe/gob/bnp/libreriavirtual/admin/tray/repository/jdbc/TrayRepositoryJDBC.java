package pe.gob.bnp.libreriavirtual.admin.tray.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.StateOrderDto;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.TrayOrderCabResponse;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.TrayOrderDetailResponse;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.TrayOrderObservationResponse;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.TrayOrderUpdateResponse;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.TrayOrdersResponse;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.TrayStateResponse;
import pe.gob.bnp.libreriavirtual.admin.tray.repository.TrayRepository;
import pe.gob.bnp.libreriavirtual.config.DataSource;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

@Repository
public class TrayRepositoryJDBC   implements TrayRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(TrayRepositoryJDBC.class);	
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction listOrder(String dateIni, String dateFin, String nroOrder, String nroDocument, String stateId) {
		
		String dBTransaction = "{ Call PKG_API_TRAY.SP_LIST_ORDER(?,?,?,?,?,?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
		
			cstm.setString("P_DATE_INI", Utility.getString(dateIni));
			cstm.setString("P_DATE_FIN", Utility.getString(dateFin));
			cstm.setString("P_ORDER_NRO",Utility.getString(nroOrder));
			cstm.setString("P_DOCUMENT_NRO",Utility.getString(nroDocument));
			cstm.setInt("P_ORDER_STATE",Utility.parseStringToInt(Utility.getString(stateId)));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					TrayOrdersResponse sliderResponse = new TrayOrdersResponse();
					sliderResponse.setId(Utility.parseLongToString(rs.getLong("ORDER_ID")));
					sliderResponse.setDate(Utility.getString(rs.getString("ORDER_DATE")));					
					sliderResponse.setNumber(Utility.getString(rs.getString("ORDER_NRO")));					
					sliderResponse.setTotal(Utility.getString(rs.getString("ORDER_TOTAL")));
					sliderResponse.setStateId(Utility.getString(rs.getString("ORDER_STATE_ID")));
					sliderResponse.setStateName(Utility.getString(rs.getString("ORDER_STATE_NAME")));
					sliderResponse.setClientName(Utility.getString(rs.getString("CLIENT_NAME")));
					sliderResponse.setClientNro(Utility.getString(rs.getString("CLIENT_NRO")));					
					
					slidersResponse.add(sliderResponse);
				}
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("PKG_API_TRAY.SP_LIST_ORDER: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		} 
		return response;
	}

	@Override
	public ResponseTransaction updateSate(StateOrderDto entity) {
		
        String dBTransaction = "{ Call PKG_API_TRAY.SP_UPDATE_STATE_ORDER(?,?,?,?,?,?,?)}";/*7*/
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();       
		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			con.setAutoCommit(false);
			cstm.setLong("P_ORDER_ID",  Utility.parseStringToLong(entity.getOrderId()));			
			cstm.setLong("P_STATE_ACTUAL_ID",  Utility.parseStringToLong(entity.getStateActualId()));
			cstm.setLong("P_STATE_NEW_ID",  Utility.parseStringToLong(entity.getStateNewId()));
			cstm.setString("P_OBSERVATION", Utility.getString(entity.getObservation()));
			cstm.setLong("P_ADMIN_ID",   Utility.parseStringToLong(entity.getAdminId()));				
				
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				con.commit();

				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					TrayOrderUpdateResponse orderResponse = new TrayOrderUpdateResponse();
					
					response.setId(Utility.getString(rs.getString("ORDER_ID")));
					orderResponse.setOrderId(Utility.getString(rs.getString("ORDER_ID")));
					orderResponse.setOrderNro(Utility.getString(rs.getString("ORDER_NRO")));					
					orderResponse.setClientName(Utility.getString(rs.getString("NAME_CLIENT")));
					orderResponse.setEmailName(Utility.getString(rs.getString("EMAIL_CLIENT")));
					orderResponse.setCountryName(Utility.getString(rs.getString("ORDER_FOREIGN_COUNTRY")));
					orderResponse.setCountryAddress(Utility.getString(rs.getString("ORDER_FOREIGN_ADDRESS")));
					orderResponse.setEmailFlag(Utility.getString(rs.getString("ENVIO_EMAIL")));			
					
					slidersResponse.add(orderResponse);
				}
				response.setList(slidersResponse);
			}else {
				con.rollback();				
			}
			
			if (rs != null) {
				rs.close();
			}
			
		}catch(SQLException e) {
			logger.error("  PKG_API_TRAY.SP_UPDATE_STATE_ORDER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;
	}
	
	@Override
	public ResponseTransaction getOrderCab(Long orderId) {
		
		String dBTransaction = "{ Call PKG_API_TRAY.SP_GET_ORDERCAB(?,?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_ORDER_ID", orderId);	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				if (rs.next()) {
					TrayOrderCabResponse sliderResponse = new TrayOrderCabResponse();
					sliderResponse.setId(Utility.parseLongToString(rs.getLong("ORDER_ID")));
					sliderResponse.setDate(Utility.getString(rs.getString("ORDER_DATE")));
					sliderResponse.setBooks(Utility.getString(rs.getString("ORDER_BOOKS")));
					sliderResponse.setNumber(Utility.getString(rs.getString("ORDER_NRO")));
					sliderResponse.setQuantity(Utility.getString(rs.getString("ORDER_QUANTITY")));	
					sliderResponse.setSubtotal(Utility.getString(rs.getString("ORDER_SUBTOTAL")));
					sliderResponse.setShipping(Utility.getString(rs.getString("ORDER_SHIPPING")));
					sliderResponse.setIgv(Utility.getString(rs.getString("ORDER_IGV")));
					sliderResponse.setTotal(Utility.getString(rs.getString("ORDER_TOTAL")));
					sliderResponse.setStateId(Utility.getString(rs.getString("ORDER_STATE_ID")));
					sliderResponse.setStateName(Utility.getString(rs.getString("ORDER_STATE_NAME")));
					
					sliderResponse.setVoucherType(Utility.getString(rs.getString("VOUCHER_TYPE")));
					sliderResponse.setVoucherNro(Utility.getString(rs.getString("VOUCHER_NRO")));
					sliderResponse.setVoucherRuc(Utility.getString(rs.getString("VOUCHER_RUC")));
					sliderResponse.setVoucherRazonSocial(Utility.getString(rs.getString("VOUCHER_RS")));
					sliderResponse.setVoucherDni(Utility.getString(rs.getString("VOUCHER_DNI")));
					sliderResponse.setVoucherName(Utility.getString(rs.getString("VOUCHER_NAME")));
					
					sliderResponse.setDeliveryType(Utility.getString(rs.getString("DELIVERY_TYPE")));
					
					sliderResponse.setRegionType(Utility.getString(rs.getString("REGION_TYPE")));
					sliderResponse.setCountryName(Utility.getString(rs.getString("COUNTRY_NAME")));
					sliderResponse.setCountryAddress(Utility.getString(rs.getString("COUNTRY_ADDRESS")));
					sliderResponse.setClientName(Utility.getString(rs.getString("CLIENT_NAME")));
					sliderResponse.setClientNro(Utility.getString(rs.getString("CLIENT_NRO")));
					
					sliderResponse.setClientEmail(Utility.getString(rs.getString("CLIENT_EMAIL")));
					sliderResponse.setContactCellphone(Utility.getString(rs.getString("CONTACT_CELLPHONE")));
					sliderResponse.setContactName(Utility.getString(rs.getString("CONTACT_NAME")));
					slidersResponse.add(sliderResponse);
				}
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("PKG_API_ORDER.SP_GET_ORDERCAB: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		}
		return response;
	}

	@Override
	public ResponseTransaction getOrderDetail(Long orderId) {
		
		String dBTransaction = "{ Call PKG_API_TRAY.SP_GET_ORDER(?,?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();
		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_ORDER_ID", orderId);	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					TrayOrderDetailResponse sliderResponse = new TrayOrderDetailResponse();
					sliderResponse.setOrderDetailId(Utility.parseLongToString(rs.getLong("ORDER_DETAIL_ID")));
					sliderResponse.setBookId(Utility.parseLongToString(rs.getLong("BOOK_ID")));
					sliderResponse.setBookImage(Utility.getString(rs.getString("BOOK_IMAGE")));
					sliderResponse.setBookTitle(Utility.getString(rs.getString("BOOK_TITLE")));
					sliderResponse.setBookSubtitle(Utility.getString(rs.getString("BOOK_SUBTITLE")));
					sliderResponse.setBookAuthorId(Utility.getString(rs.getString("BOOK_AUTHOR_ID")));						
					sliderResponse.setBookAuthorName(Utility.getString(rs.getString("BOOK_AUTHOR_NAME")));
					sliderResponse.setBookMaterialId(Utility.getString(rs.getString("BOOK_MATERIAL_ID")));
					sliderResponse.setBookMaterialName(Utility.getString(rs.getString("BOOK_MATERIAL_NAME")));
					sliderResponse.setBookYearId(Utility.getString(rs.getString("BOOK_YEAR_ID")));
					sliderResponse.setBookYearName(Utility.getString(rs.getString("BOOK_YEAR_NAME")));
					sliderResponse.setBookSizeId(Utility.getString(rs.getString("BOOK_SIZE_ID")));
					sliderResponse.setBookSizeName(Utility.getString(rs.getString("BOOK_SIZE_NAME")));
					sliderResponse.setBookPrice(Utility.getString(rs.getString("BOOK_PRICE")));					
					sliderResponse.setOrderDetailQuantity(Utility.getString(rs.getString("ORDER_DETAIL_QUANTITY")));
					sliderResponse.setOrderDetailSubtotal(Utility.getString(rs.getString("ORDER_DETAIL_SUBTOTAL")));
					
					slidersResponse.add(sliderResponse);
				}
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error(" PKG_API_TRAY.SP_GET_ORDER: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		} 
		return response;
	}

	@Override
	public ResponseTransaction getOrderObservation(Long orderId) {
		
		String dBTransaction = "{ Call PKG_API_TRAY.SP_LIST_OBSERVATION(?,?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_ORDER_ID", orderId);	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					TrayOrderObservationResponse sliderResponse = new TrayOrderObservationResponse();
					sliderResponse.setOrderId(Utility.parseLongToString(rs.getLong("ORDER_ID")));
					sliderResponse.setObservationId(Utility.parseLongToString(rs.getLong("OBSERVATION_ID")));
					sliderResponse.setObservationDescription(Utility.getString(rs.getString("OBSERVATION_DESCRIPTION")));
					sliderResponse.setAdminId(Utility.parseLongToString(rs.getLong("ADMIN_ID")));
					sliderResponse.setAdminName(Utility.getString(rs.getString("ADMIN_NAME")));					
					
					slidersResponse.add(sliderResponse);
				}
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error(" PKG_API_TRAY.SP_LIST_OBSERVATION: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		} 
		return response;
	}

	@Override
	public ResponseTransaction getStates(Long stateId) {
		
		String dBTransaction = "{ Call PKG_API_TRAY.SP_LIST_STATE(?,?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_STATE_ID", stateId);	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					TrayStateResponse sliderResponse = new TrayStateResponse();
					sliderResponse.setStateId(Utility.parseLongToString(rs.getLong("STATE_ID")));
					sliderResponse.setStateDescription(Utility.getString(rs.getString("STATE_DESCRIPTION")));
									
					
					slidersResponse.add(sliderResponse);
				}
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error(" PKG_API_TRAY.SP_LIST_STATE: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		} 
		
		return response;
	}
	
	

}
