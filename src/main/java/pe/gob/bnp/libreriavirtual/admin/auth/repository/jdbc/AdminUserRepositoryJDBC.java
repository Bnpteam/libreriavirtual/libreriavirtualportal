package pe.gob.bnp.libreriavirtual.admin.auth.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.LoginAdminResponse;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.AdminCredential;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.UserAdminResponse;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.UserDetailAdminResponse;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.UserRequest;
import pe.gob.bnp.libreriavirtual.admin.auth.model.AdminUser;
import pe.gob.bnp.libreriavirtual.admin.auth.repository.AdminUserRepository;
import pe.gob.bnp.libreriavirtual.config.DataSource;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

@Repository
public class AdminUserRepositoryJDBC  implements AdminUserRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(AdminUserRepositoryJDBC.class);	

	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction loginAdmin(AdminCredential credential) {
		
        String dBTransaction = "{ Call PKG_API_AUTH.SP_LOGIN(?,?,?)}";
             
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			con.setAutoCommit(false);
			cstm.setString("P_DOMAIN", credential.getDomain());
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				con.commit();
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					LoginAdminResponse userResponse = new LoginAdminResponse(); 
					userResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					userResponse.setName(Utility.getString(rs.getString("USER_NAME")));
					userResponse.setSurname(Utility.getString(rs.getString("USER_SURNAME")));
					userResponse.setEmail(Utility.getString(rs.getString("USER_EMAIL")));
					userResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));
					
					usersResponse.add(userResponse);
				}	
				response.setList(usersResponse);			
			}else {
				con.rollback();
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_AUTH.SP_LOGIN: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}

	@Override
	public ResponseTransaction persistAdmin(AdminUser entity) {
		
        String dBTransaction = "{ Call PKG_API_AUTH.SP_PERSIST_USER(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";/*16*/
        ResponseTransaction response = new ResponseTransaction();
		
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			con.setAutoCommit(false);
			cstm.setString("P_NAME", Utility.getString(entity.getUserName()));
			cstm.setString("P_DOMAIN", Utility.getString(entity.getUserDomain()));
			
			cstm.setString("P_FLAG_USER_IN",Utility.getString(entity.getFlagModuleUserIn()));	
			cstm.setString("P_FLAG_USER_OUT",Utility.getString(entity.getFlagModuleUserOut()));	
			cstm.setString("P_FLAG_TRAY_ORDER",Utility.getString(entity.getFlagModuleTrayOrder()));	
			cstm.setString("P_FLAG_CONSULT",Utility.getString(entity.getFlagModuleConsult()));	
			cstm.setString("P_FLAG_PUBLICATION",Utility.getString(entity.getFlagModulePublication()));	
			cstm.setString("P_FLAG_PRODUCT",Utility.getString(entity.getFlagModuleProduct()));	
			cstm.setString("P_FLAG_BOOK",Utility.getString(entity.getFlagModuleBook()));	
			cstm.setString("P_FLAG_INVENTORY",Utility.getString(entity.getFlagModuleInventory()));	
			cstm.setString("P_FLAG_MASTER",Utility.getString(entity.getFlagModuleMaster()));	
			cstm.setString("P_FLAG_REPORT",Utility.getString(entity.getFlagModuleReport()));	
			cstm.setString("P_FLAG_DASHBOARD",Utility.getString(entity.getFlagModuleDashboard()));	
						
			cstm.setLong("P_CREATED_USER", Utility.parseStringToLong(entity.getCreatedUser()));			
					
			cstm.registerOutParameter("S_USER_ID",OracleTypes.NUMBER);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				con.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_USER_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				con.rollback();				
			}
			
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_PERSIST_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;
	}

	@Override
	public ResponseTransaction updateAdmin(AdminUser entity) {
		
        String dBTransaction = "{ Call PKG_API_AUTH.SP_UPDATE_USER(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?}";/*18*/
        ResponseTransaction response = new ResponseTransaction();       
	
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			con.setAutoCommit(false);
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_NAME", Utility.getString(entity.getUserName()));
			cstm.setString("P_DOMAIN",Utility.getString(entity.getUserDomain()));
			
			cstm.setString("P_ENABLED",Utility.getString(entity.getIdEnabled()));					
			cstm.setString("P_FLAG_USER_IN",Utility.getString(entity.getFlagModuleUserIn()));	
			cstm.setString("P_FLAG_USER_OUT",Utility.getString(entity.getFlagModuleUserOut()));	
			cstm.setString("P_FLAG_TRAY_ORDER",Utility.getString(entity.getFlagModuleTrayOrder()));	
			cstm.setString("P_FLAG_CONSULT",Utility.getString(entity.getFlagModuleConsult()));	
			cstm.setString("P_FLAG_PUBLICATION",Utility.getString(entity.getFlagModulePublication()));	
			cstm.setString("P_FLAG_PRODUCT",Utility.getString(entity.getFlagModuleProduct()));	
			cstm.setString("P_FLAG_BOOK",Utility.getString(entity.getFlagModuleBook()));	
			cstm.setString("P_FLAG_INVENTORY",Utility.getString(entity.getFlagModuleInventory()));	
			cstm.setString("P_FLAG_MASTER",Utility.getString(entity.getFlagModuleMaster()));	
			cstm.setString("P_FLAG_REPORT",Utility.getString(entity.getFlagModuleReport()));	
			cstm.setString("P_FLAG_DASHBOARD",Utility.getString(entity.getFlagModuleDashboard()));	
			
			cstm.setLong("P_UPDATED_USER", Utility.parseStringToLong(entity.getUpdatedUser()));	
			
			cstm.registerOutParameter("S_USER_ID",OracleTypes.NUMBER);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				con.commit();

				Long id=  Utility.parseObjectToLong(cstm.getObject("S_USER_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				con.rollback();				
			}
			
			
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_UPDATE_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		
		return response;
	}

	@Override
	public ResponseTransaction list(UserRequest userRequest) {
		
        String dBTransaction = "{ Call PKG_API_AUTH.SP_LIST_USER(?,?,?)}";
        
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setString("P_KEYWORD", Utility.getString(userRequest.getKeyword()));			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					UserAdminResponse userResponse = new UserAdminResponse();
					userResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					userResponse.setName(Utility.getString(rs.getString("USER_NAME")));
					userResponse.setDomain(Utility.getString(rs.getString("USER_DOMAIN")));
					userResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));
					userResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					userResponse.setLastLoginDate(Utility.parseDateToString(Utility.getDate(rs.getDate("LAST_LOGIN_DATE"))));
				
					usersResponse.add(userResponse);
				}
				response.setList(usersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_LIST_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction readAdmin(Long id) {
		
        String dBTransaction = "{ Call PKG_API_AUTH.SP_GET_USER(?,?,?)}";
             
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();      
		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_USER_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					UserDetailAdminResponse userResponse = new UserDetailAdminResponse(); 
					userResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					userResponse.setName(Utility.getString(rs.getString("USER_NAME")));
					userResponse.setDomain(Utility.getString(rs.getString("USER_DOMAIN")));
					userResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));
					userResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					userResponse.setLastLoginDate(Utility.parseDateToString(Utility.getDate(rs.getDate("LAST_LOGIN_DATE"))));
					
					String flagModuleUserIn=Utility.getString(rs.getString("FLAG_USER_IN")).equalsIgnoreCase("1")?"true":"false";
					String flagModuleUserOut=Utility.getString(rs.getString("FLAG_USER_OUT")).equalsIgnoreCase("1")?"true":"false";
					String flagModuleTrayOrder=Utility.getString(rs.getString("FLAG_TRAY_ORDER")).equalsIgnoreCase("1")?"true":"false";
					String flagModuleConsult=Utility.getString(rs.getString("FLAG_CONSULT")).equalsIgnoreCase("1")?"true":"false";
					String flagModulePublication=Utility.getString(rs.getString("FLAG_PUBLICATION")).equalsIgnoreCase("1")?"true":"false";
					String flagModuleProduct=Utility.getString(rs.getString("FLAG_PRODUCT")).equalsIgnoreCase("1")?"true":"false";
					String flagModuleBook=Utility.getString(rs.getString("FLAG_BOOK")).equalsIgnoreCase("1")?"true":"false";
					String flagModuleInventory=Utility.getString(rs.getString("FLAG_INVENTORY")).equalsIgnoreCase("1")?"true":"false";
					
					String flagModuleMaster=Utility.getString(rs.getString("FLAG_MASTER")).equalsIgnoreCase("1")?"true":"false";
					String flagModuleReport=Utility.getString(rs.getString("FLAG_REPORT")).equalsIgnoreCase("1")?"true":"false";
					String flagModuleDashboard=Utility.getString(rs.getString("FLAG_DASHBOARD")).equalsIgnoreCase("1")?"true":"false";	
					
					
					userResponse.setFlagModuleUserIn(flagModuleUserIn);
					userResponse.setFlagModuleUserOut(flagModuleUserOut);
					userResponse.setFlagModuleTrayOrder(flagModuleTrayOrder);
					userResponse.setFlagModuleConsult(flagModuleConsult);
					userResponse.setFlagModulePublication(flagModulePublication);
					userResponse.setFlagModuleProduct(flagModuleProduct);
					userResponse.setFlagModuleBook(flagModuleBook);
					userResponse.setFlagModuleInventory(flagModuleInventory);
					
					userResponse.setFlagModuleMaster(flagModuleMaster);
					userResponse.setFlagModuleReport(flagModuleReport);
					userResponse.setFlagModuleDashboard(flagModuleDashboard);
				
					
					usersResponse.add(userResponse);
				}	
				response.setList(usersResponse);			
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_GET_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}

	@Override
	public ResponseTransaction persist(AdminUser entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTransaction update(AdminUser entity) {
		// TODO Auto-generated method stub
		return null;
	}
}
