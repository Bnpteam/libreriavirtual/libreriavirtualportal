package pe.gob.bnp.libreriavirtual.admin.tray.dto;


public class StateOrderDto {
	
	private String orderId;
	private String stateActualId;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getStateActualId() {
		return stateActualId;
	}
	public void setStateActualId(String stateActualId) {
		this.stateActualId = stateActualId;
	}
	public String getStateNewId() {
		return stateNewId;
	}
	public void setStateNewId(String stateNewId) {
		this.stateNewId = stateNewId;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	public String getAdminId() {
		return adminId;
	}
	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}
	private String stateNewId;
	private String observation;
	private String adminId;
	
}
