package pe.gob.bnp.libreriavirtual.admin.tray.dto;

public class TrayOrdersResponse {
	
	private String id;
	private String date;	
	private String number;	
	private String total;
	private String stateId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getClientNro() {
		return clientNro;
	}
	public void setClientNro(String clientNro) {
		this.clientNro = clientNro;
	}
	private String stateName;	
	private String clientName;
	private String clientNro;

}
