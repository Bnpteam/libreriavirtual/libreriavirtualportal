package pe.gob.bnp.libreriavirtual.admin.tray.dto;


public class TrayOrderCabResponse {
	
	private String id;
	private String date;
	private String books;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getBooks() {
		return books;
	}
	public void setBooks(String books) {
		this.books = books;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	public String getShipping() {
		return shipping;
	}
	public void setShipping(String shipping) {
		this.shipping = shipping;
	}
	public String getIgv() {
		return igv;
	}
	public void setIgv(String igv) {
		this.igv = igv;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getVoucherNro() {
		return voucherNro;
	}
	public void setVoucherNro(String voucherNro) {
		this.voucherNro = voucherNro;
	}
	public String getVoucherRuc() {
		return voucherRuc;
	}
	public void setVoucherRuc(String voucherRuc) {
		this.voucherRuc = voucherRuc;
	}
	public String getVoucherRazonSocial() {
		return voucherRazonSocial;
	}
	public void setVoucherRazonSocial(String voucherRazonSocial) {
		this.voucherRazonSocial = voucherRazonSocial;
	}
	public String getVoucherDni() {
		return voucherDni;
	}
	public void setVoucherDni(String voucherDni) {
		this.voucherDni = voucherDni;
	}
	public String getVoucherName() {
		return voucherName;
	}
	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public String getRegionType() {
		return regionType;
	}
	public void setRegionType(String regionType) {
		this.regionType = regionType;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryAddress() {
		return countryAddress;
	}
	public void setCountryAddress(String countryAddress) {
		this.countryAddress = countryAddress;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getClientNro() {
		return clientNro;
	}
	public void setClientNro(String clientNro) {
		this.clientNro = clientNro;
	}
	public String getClientEmail() {
		return clientEmail;
	}
	public void setClientEmail(String clientEmail) {
		this.clientEmail = clientEmail;
	}
	public String getContactCellphone() {
		return contactCellphone;
	}
	public void setContactCellphone(String contactCellphone) {
		this.contactCellphone = contactCellphone;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	private String number;
	private String quantity;
	private String subtotal;
	private String shipping;
	private String igv;
	private String total;
	private String stateId;
	private String stateName;
	//voucher
	private String voucherType;
	private String voucherNro;	
	private String voucherRuc;
	private String voucherRazonSocial;
	private String voucherDni;
	private String voucherName;
	
	private String deliveryType;	
	private String regionType;
	private String countryName;
	private String countryAddress;
	private String clientName;
	private String clientNro;
	
	private String clientEmail;
	private String contactCellphone;
	private String contactName;

}
