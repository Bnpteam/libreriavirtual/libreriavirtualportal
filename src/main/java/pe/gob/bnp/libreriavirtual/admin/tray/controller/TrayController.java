package pe.gob.bnp.libreriavirtual.admin.tray.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.StateOrderDto;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.TrayOrderCabResponse;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.TrayOrderDetailResponse;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.TrayOrderObservationResponse;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.TrayOrderUpdateResponse;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.TrayOrdersResponse;
import pe.gob.bnp.libreriavirtual.admin.tray.dto.TrayStateResponse;
import pe.gob.bnp.libreriavirtual.admin.tray.service.TrayService;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseHandler;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api/admin/tray")
@Api(value = "/api/admin/tray")
@CrossOrigin(origins = "*")
public class TrayController {
	
	private static final Logger logger = LoggerFactory.getLogger(TrayController.class);	

	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	TrayService trayService;

	
	@RequestMapping(method = RequestMethod.GET, path="/orders", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "bandeja de pedidos - Administrador", response= TrayOrdersResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> listOrder(
			@RequestParam(value = "dateIni", 	 defaultValue = "", required=false) String  dateIni,
			@RequestParam(value = "dateFin", 	 defaultValue = "", required=false) String  dateFin,
			@RequestParam(value = "nroOrder", 	 defaultValue = "", required=false) String nroOrder,
			@RequestParam(value = "nroDocument", defaultValue = "", required=false) String nroDocument,
			@RequestParam(value = "stateId", 	 defaultValue = "0", required=false) String  stateId
			){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = trayService.listTrayOrders(dateIni,dateFin,nroOrder,nroDocument,stateId);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos orderss tray: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("orders/"+dateIni+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("orders/"+dateIni+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("orders/"+dateIni+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.PUT,path="/order/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar estado pedido", response= TrayOrderUpdateResponse.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud")
	})		
	public ResponseEntity<Object> update(
			@ApiParam(value = "Estructura JSON de direcciones", required = true)
			@PathVariable Long orderId, @RequestBody StateOrderDto stateOrderDto) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = trayService.updateState(orderId,stateOrderDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos put orderss tray: " + tiempo);		
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/order/"+orderId+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/order/"+orderId+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/order/orderCab/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "admin cabecera pedido", response= TrayOrderCabResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> getOrderCab(
			@ApiParam(value = "id pedido", required = true)
			@PathVariable Long orderId){
		ResponseTransaction response=new ResponseTransaction();
		try {
			
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = trayService.getOrderCab(orderId);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get orderCab tray: " + tiempo);
			
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("tray/order/orderCab/"+orderId+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("tray/order/orderCab/"+orderId+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("tray/order/orderCab/"+orderId+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	
	@RequestMapping(method = RequestMethod.GET, path="/order/orderDet/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "admin detalle pedido", response= TrayOrderDetailResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> getOrderDetail(
			@ApiParam(value = "id pedido", required = true)
			@PathVariable Long orderId){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = trayService.getOrderDetail(orderId);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get orderDet tray: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("tray/order/orderDet/"+orderId+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("tray/order/orderDet/"+orderId+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("tray/order/orderDet/"+orderId+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/observations/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "observaciones x pedido", response= TrayOrderObservationResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> getObservations(
			@ApiParam(value = "id pedido", required = true)
			@PathVariable Long orderId){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = trayService.getOrderObservation(orderId);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get observations tray: " + tiempo);
			
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("observations/"+orderId+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("observations/"+orderId+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("observations/"+orderId+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/states/{stateId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "estados disponibles x id estado", response= TrayStateResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> getOrder(
			@ApiParam(value = "id estado", required = true)
			@PathVariable Long stateId){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = trayService.getStates(stateId);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get states tray: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("states/"+stateId+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("states/"+stateId+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("states/"+stateId+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	

}
