package pe.gob.bnp.libreriavirtual.admin.auth.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.AdminCredential;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.AdminUserDto;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.UserAdminResponse;
import pe.gob.bnp.libreriavirtual.admin.auth.dto.UserDetailAdminResponse;
import pe.gob.bnp.libreriavirtual.admin.auth.service.AuthAdminService;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseHandler;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api/admin/auth")
@Api(value = "/api/admin/auth")
@CrossOrigin(origins = "*")
public class AuthAdminController {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthAdminController.class);
	
	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	AuthAdminService authAdminService;
	
	
	//login
	@RequestMapping(method = RequestMethod.POST, path = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Login de usuario, debe estar habilitado dominio", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 400, message = "Solicitud contiene errores"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"),
	})	
	public ResponseEntity<Object> login(
			@ApiParam(value = "objeto contiene el domain ingresado por el usuario", required = true)
			@RequestBody  AdminCredential credential){ 
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();			
			
			response = authAdminService.loginAdmin(credential);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos post login admin: " + tiempo);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("login IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,"Error Autenticacion "+ex.getMessage());
		} catch (Throwable ex) {
			logger.error("login Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	
	}		

		
	@RequestMapping(method = RequestMethod.POST,path="/signup", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar usuario BNP", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> signup(
			@ApiParam(value = "Estructura JSON de usuario BNP", required = true)
			@RequestBody AdminUserDto userDto) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = authAdminService.signupAdmin(userDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos post signup admin: " + tiempo);	
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("signup IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("signup Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
	
	@RequestMapping(method = RequestMethod.PUT,path="/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar usuario BNP", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> update(
			@ApiParam(value = "Estructura JSON de usuario BNP", required = true)
			@PathVariable Long id,@RequestBody AdminUserDto userDto) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = authAdminService.updateAdmin(id,userDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get order: " + tiempo);	
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/user/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/user/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
		
	@RequestMapping(method = RequestMethod.GET, path="/users",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar usuarios BNP", response= UserAdminResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> searchAll(
			@RequestParam(value = "keyword", defaultValue = "", required=false) String  keyword){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = authAdminService.searchAll(keyword);			

			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get users: " + tiempo);	
			if (response == null || response.getList().size()==0 ) {
				logger.info("/users: keyword:"+keyword+" No se encontró datos.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros.");//);
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/users EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontraron registros."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/users IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/users Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "leer usuario BNP", response= UserDetailAdminResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> get(
			@ApiParam(value = "id Usuario BNP", required = true)
			@PathVariable Long id){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = authAdminService.getAdmin(id);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get user/id: " + tiempo);	
			
			if (response == null || response.getList().size()==0) {
				logger.info("/user: id:"+id+ "No se encontró datos.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/user/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/user/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/user/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}		

}
