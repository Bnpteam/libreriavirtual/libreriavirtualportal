package pe.gob.bnp.libreriavirtual.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	private String basePackage = "pe.gob.bnp.libreriavirtual";
	private String description = "API Libreria Virtual - Implementado por OTIE - BNP";
	private String title = " Libreria Virtual API";
	private String license = "BNP";
	private String url = "http://www.bnp.gob.pe";
	private String version = "1.0";

	@Bean
	public Docket digitalServicesApi() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage(basePackage))
				.paths(Predicates.not(PathSelectors.regex("/error"))).build().apiInfo(getApiInfo());
	}

	private ApiInfo getApiInfo() {
		return new ApiInfoBuilder().title(title).description(description).license(license).licenseUrl(url)
				.version(version).build();
	}

}
