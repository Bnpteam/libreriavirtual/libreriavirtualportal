package pe.gob.bnp.libreriavirtual.config;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DataSource {

	 private static HikariConfig config = new HikariConfig();
	    private static HikariDataSource ds;

	    //TEST
	    
	   /* static {
	        config.setJdbcUrl( "jdbc:oracle:thin:@172.16.88.154:1521:dbdev" );
	        config.setUsername( "TIENDA_VIRTUAL" );
	        config.setPassword( "dev_tiendav2711ual#" );
	        config.addDataSourceProperty( "cachePrepStmts" , "true" );
	        config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
	        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
	        ds = new HikariDataSource( config );
	    }
	    */
	    
	    //PROD
	    
	    static {
	        config.setJdbcUrl( "jdbc:oracle:thin:@172.16.89.202:1521:dbprod" );
	        config.setUsername( "TIENDA_VIRTUAL" );
	        config.setPassword( "tiendvirut43dewx1" );
	        config.addDataSourceProperty( "cachePrepStmts" , "true" );
	        config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
	        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
	        ds = new HikariDataSource( config );
	    }
	    

	    private DataSource() {}

	    public static Connection getConnection() throws SQLException {
	        return ds.getConnection();
	    }
}
