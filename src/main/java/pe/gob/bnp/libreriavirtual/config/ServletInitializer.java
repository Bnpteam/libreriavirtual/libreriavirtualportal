package pe.gob.bnp.libreriavirtual.config;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import pe.gob.bnp.libreriavirtual.LibreriavirtualApplication;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(LibreriavirtualApplication.class);
	}
	
	
	

}
