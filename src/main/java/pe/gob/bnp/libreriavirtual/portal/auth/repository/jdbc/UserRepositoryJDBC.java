package pe.gob.bnp.libreriavirtual.portal.auth.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.libreriavirtual.config.DataSource;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.EmailResponse;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.LoginResponse;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.PortalCredential;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserPublicResponse;
import pe.gob.bnp.libreriavirtual.portal.auth.model.User;
import pe.gob.bnp.libreriavirtual.portal.auth.repository.UserRepository;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

@Repository
public class UserRepositoryJDBC  implements UserRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(UserRepositoryJDBC.class);	

	private String messageSuccess="Transacción exitosa.";
	
	@Override
	public ResponseTransaction loginPortal(PortalCredential credential) {
		
        String dBTransaction = "{ Call PKG_API_AUTH.SP_PUBLIC_LOGIN(?,?,?,?,?)}";
             
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			con.setAutoCommit(false);
			cstm.setString("P_EMAIL", credential.getEmail());
			cstm.setString("P_PASSWORD", credential.getPassword());
			cstm.setString("P_RED_SOCIAL",(Utility.parseStringToInt(credential.getFlagRedSocial())).toString());
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				con.commit();
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					LoginResponse userResponse = new LoginResponse(); 
					userResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					userResponse.setName(Utility.getString(rs.getString("USER_NAME")));
					userResponse.setSurname(Utility.getString(rs.getString("USER_SURNAME")));
					userResponse.setEmail(Utility.getString(rs.getString("USER_EMAIL")));
					userResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));
					
					usersResponse.add(userResponse);
				}	
				response.setList(usersResponse);			
			}else {
				con.rollback();
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("SP_PUBLIC_LOGIN: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction persistCuenta(User entity) {
		
        String dBTransaction = "{ Call PKG_API_AUTH.SP_PERSIST_PUBLIC_USER(?,?,?,?,?,?,?,?,?,?,?,?)}";/*12*/
        ResponseTransaction response = new ResponseTransaction();

		ResultSet rs = null;
		 List<Object> resourcesResponse = new ArrayList<>();
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			con.setAutoCommit(false);
			cstm.setString("P_NAME", Utility.getString(entity.getUserName()));
			cstm.setString("P_SURNAME", Utility.getString(entity.getUserSurname()));
			cstm.setString("P_EMAIL",Utility.getString(entity.getUserEmail()));	
			cstm.setString("P_PASSWORD",Utility.getString(entity.getUserPassword()));	
			cstm.setDate("P_BIRTHDATE",Utility.parseStringToSQLDate(Utility.parseDateToString(entity.getBirthDate())));
			cstm.setLong("P_TYPE_DOCUMENT",entity.getTypeDocument());	
			cstm.setString("P_DOCUMENT",Utility.getString(entity.getDocument()));	
			cstm.setString("P_PHOTO",Utility.getString(entity.getPhoto()));
			cstm.setString("P_PROVIDER",Utility.getString(entity.getProvider()));
					
			cstm.registerOutParameter("S_USER_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);		
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				con.commit();						
				
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_USER_ID"));			
				response.setId(String.valueOf(id));			
				response.setResponse(messageSuccess);
				
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {					
					EmailResponse resourceResponse = new EmailResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));						
					resourceResponse.setNombre(Utility.getString(rs.getString("USER_NAME")));					
					resourceResponse.setCorreo(Utility.getString(rs.getString("USER_EMAIL")));	
					resourceResponse.setUrl(Utility.getString(rs.getString("USER_URL")));	
					resourceResponse.setFlagActivo(Utility.getString(rs.getString("USER_ACTIVE")));	
									
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}else {
				con.rollback();				
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_AUTH.SP_PERSIST_PUBLIC_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;
	}
		
	@Override
	public ResponseTransaction recoveryPassword(String tokenPassword,String email) {
		
        String dBTransaction = "{ Call PKG_API_AUTH.SP_RECOVERY_PASSWORD(?,?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();
		ResultSet rs = null;
		 List<Object> resourcesResponse = new ArrayList<>();
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			con.setAutoCommit(false);
			cstm.setString("P_TOKEN", Utility.getString(tokenPassword));
			cstm.setString("P_EMAIL", Utility.getString(email));					
			cstm.registerOutParameter("S_USER_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);		
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {				
				con.commit();
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_USER_ID"));				
				response.setId(String.valueOf(id));				
				response.setResponse(messageSuccess);
				
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {					
					EmailResponse resourceResponse = new EmailResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));						
					resourceResponse.setNombre(Utility.getString(rs.getString("USER_NAME")));					
					resourceResponse.setCorreo(Utility.getString(rs.getString("USER_EMAIL")));	
					resourceResponse.setUrl(Utility.getString(rs.getString("USER_URL")));	
					resourceResponse.setFlagActivo(Utility.getString(rs.getString("USER_ACTIVE")));	
									
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
				
				
			}else {
				con.rollback();				
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_AUTH.SP_RECOVERY_PASSWORD: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;
	}

	@Override
	public ResponseTransaction updateToken(User entity) {
		
        String dBTransaction = "{ Call PKG_API_AUTH.SP_UPDATE_TOKEN_PUBLIC_USER(?,?,?,?)}";
        ResponseTransaction response = new ResponseTransaction();       
		
		
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			con.setAutoCommit(false);
			cstm.setString("P_TOKEN", entity.getTokenPassword());
			cstm.setString("P_PASSWORD", Utility.getString(entity.getUserPassword()));						
			cstm.registerOutParameter("S_USER_ID",OracleTypes.NUMBER);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {				
				con.commit();					
				
				Long id=  Utility.parseObjectToLong(cstm.getObject("S_USER_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				con.rollback();				
			}
			
			
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_UPDATE_TOKEN_PUBLIC_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		
		return response;
	}
		
	@Override
	public ResponseTransaction update(User entity) {
		
        String dBTransaction = "{ Call PKG_API_AUTH.SP_UPDATE_PUBLIC_USER(?,?,?,?,?,?,?,?,?,?,?)}";/*11*/
        ResponseTransaction response = new ResponseTransaction();       
	
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			con.setAutoCommit(false);
			cstm.setLong("P_ID", entity.getId());
			cstm.setString("P_NAME", Utility.getString(entity.getUserName()));
			cstm.setString("P_SURNAME", Utility.getString(entity.getUserSurname()));
			cstm.setString("P_EMAIL", Utility.getString(entity.getUserEmail()));
			cstm.setDate("P_BIRTHDATE",Utility.parseStringToSQLDate(Utility.parseDateToString(entity.getBirthDate())));	
			cstm.setLong("P_TYPE_DOCUMENT",entity.getTypeDocument());	
			cstm.setString("P_DOCUMENT",Utility.getString(entity.getDocument()));	
			cstm.setString("P_PASSWORD_OLD",Utility.getString(entity.getUserPassword()));	
			cstm.setString("P_PASSWORD_NEW",Utility.getString(entity.getUserPasswordNew()));	
			
			cstm.registerOutParameter("S_USER_ID",OracleTypes.NUMBER);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				con.commit();

				Long id=  Utility.parseObjectToLong(cstm.getObject("S_USER_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				con.rollback();				
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_UPDATE_PUBLIC_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;
	}
	
	@Override
	public ResponseTransaction persist(User entity) {
		// TODO Auto-generated method stub
		return null;
	}	
	
	@Override
	public ResponseTransaction read(Long id) {
	
        String dBTransaction = "{ Call PKG_API_AUTH.SP_GET_PUBLIC_USER(?,?,?)}";
             
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		ResultSet rs = null;
		try  (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
		
			cstm.setLong("P_USER_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					UserPublicResponse userPublicResponse = new UserPublicResponse();
					userPublicResponse.setId(Utility.parseLongToString(rs.getLong("USER_ID")));
					userPublicResponse.setName(Utility.getString(rs.getString("USER_NAME")));
					userPublicResponse.setSurname(Utility.getString(rs.getString("USER_SURNAME")));
					userPublicResponse.setEmail(Utility.getString(rs.getString("USER_EMAIL")));
					userPublicResponse.setBirthDate(Utility.parseSqlDateToString(rs.getDate("USER_BIRTHDATE")));
					userPublicResponse.setTypeDocumentId(Utility.parseLongToString(rs.getLong("USER_TYPE_DOCUMENT_ID")));
					userPublicResponse.setTypeDocumentName(Utility.getString(rs.getString("USER_TYPE_DOCUMENT_NAME")));
					userPublicResponse.setDocument(Utility.getString(rs.getString("USER_DOCUMENT")));
					userPublicResponse.setPhoto(Utility.getString(rs.getString("USER_PHOTO")));
					userPublicResponse.setProvider(Utility.getString(rs.getString("USER_PROVIDER")));
					userPublicResponse.setEnabled(Utility.getString(rs.getString("USER_ENABLED")));					
					userPublicResponse.setCreatedDate(Utility.parseDateToString(Utility.getDate(rs.getDate("CREATED_DATE"))));
					userPublicResponse.setLastLoginDate(Utility.parseDateToString(Utility.getDate(rs.getDate("LAST_LOGIN_DATE"))));
				
					
					usersResponse.add(userPublicResponse);
				}	
				response.setList(usersResponse);			
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_AUTH.SP_GET_PUBLIC_USER: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		
		return response;	
	}
	
}
