package pe.gob.bnp.libreriavirtual.portal.order.dto;

public class CalculateResponse {
	
	private String subtotalCost;
	private String shippingCost;
	private String igvCost;
	public String getSubtotalCost() {
		return subtotalCost;
	}
	public void setSubtotalCost(String subtotalCost) {
		this.subtotalCost = subtotalCost;
	}
	public String getShippingCost() {
		return shippingCost;
	}
	public void setShippingCost(String shippingCost) {
		this.shippingCost = shippingCost;
	}
	public String getIgvCost() {
		return igvCost;
	}
	public void setIgvCost(String igvCost) {
		this.igvCost = igvCost;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	private String totalCost;
	
	
	

}
