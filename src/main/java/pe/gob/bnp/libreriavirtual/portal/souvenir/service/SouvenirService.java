package pe.gob.bnp.libreriavirtual.portal.souvenir.service;

import java.io.IOException;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.ProductDto;
import pe.gob.bnp.libreriavirtual.portal.souvenir.exception.SouvenirException;
import pe.gob.bnp.libreriavirtual.portal.souvenir.repository.SouvenirRepository;
import pe.gob.bnp.libreriavirtual.portal.souvenir.validation.ProductValidation;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

@Service
public class SouvenirService {
	
	//private static final Logger logger = LoggerFactory.getLogger(SouvenirService.class);	
	
	@Autowired
	SouvenirRepository souvenirRepository;
	
	@Autowired
	SouvenirService souvenirService;
	
	public ResponseTransaction listProducts(ProductDto productDto) {

		ResponseTransaction response = new ResponseTransaction();
		Notification notification = ProductValidation.validation(productDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			
			return response;
		}
		
	
		response = souvenirRepository.listProducts(productDto);

		return SouvenirException.setMessageResponseProductList(response);
	}
	
	
	public ResponseTransaction listGroup(Long submenuId) throws IOException {

		ResponseTransaction response = souvenirRepository.listGroup(submenuId);
		
		return SouvenirException.setMessageResponseGroupList(response);
	}
	
	public ResponseTransaction listTopic() throws IOException {

		ResponseTransaction response = souvenirRepository.listTopic();
		
		return response;
	}
	
	public ResponseTransaction listNovelties(Long submenuId) throws IOException {

		ResponseTransaction response = souvenirRepository.listNovelties(submenuId);
		
		return response;
	}

}
