package pe.gob.bnp.libreriavirtual.portal.souvenir.validation;

import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.ProductDto;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

public class ProductValidation {
	
	private static String messageProductList = "No se encontraron datos ";
	private static String messageProductListMenu= "Se debe seleccionar submenu";
	private static String messageProductListGroup= "Se debe ingresar producto";
	private static String messageProductListTopic= "Se debe ingresar tema";
	
	private static String messageCourseListPage= "Se debe ingresar el nro. de página";
	private static String messageCourseListSize= "Se debe ingresar el nro. de registros";
	
	
	public static Notification validation(ProductDto productDto) {
		Notification notification = new Notification();

		if (productDto == null) {
			notification.addError(messageProductList);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(productDto.getSubmenuId())) {
			notification.addError(messageProductListMenu);
		}

		if (Utility.isEmptyOrNull(productDto.getGroupId())) {
			notification.addError(messageProductListGroup);
		}
		
		if (Utility.isEmptyOrNull(productDto.getTopicId())) {
			notification.addError(messageProductListTopic);
		}
		
		
		if (Utility.isEmptyOrNull(productDto.getPage())) {
			notification.addError(messageCourseListPage);
		}
		
		if (Utility.isEmptyOrNull(productDto.getSize())) {
			notification.addError(messageCourseListSize);
		}
		
	

		return notification;
	}


}
