package pe.gob.bnp.libreriavirtual.portal.order.dto;

import java.util.List;



public class StockDto {
	
	private List<OrderDetailDto> detailBookAndProduct;

	public List<OrderDetailDto> getDetailBookAndProduct() {
		return detailBookAndProduct;
	}

	public void setDetailBookAndProduct(List<OrderDetailDto> detailBookAndProduct) {
		this.detailBookAndProduct = detailBookAndProduct;
	}

}
