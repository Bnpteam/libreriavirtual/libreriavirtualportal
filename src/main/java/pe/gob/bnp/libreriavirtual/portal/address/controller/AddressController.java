package pe.gob.bnp.libreriavirtual.portal.address.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.libreriavirtual.portal.address.dto.AddressDto;
import pe.gob.bnp.libreriavirtual.portal.address.dto.AddressResponse;
import pe.gob.bnp.libreriavirtual.portal.address.dto.AddressesResponse;
import pe.gob.bnp.libreriavirtual.portal.address.service.AddressService;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseHandler;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api/portal/address")
@Api(value = "/api/portal/address")
@CrossOrigin(origins = "*")
public class AddressController {
	
	private static final Logger logger = LoggerFactory.getLogger(AddressController.class);	

	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	AddressService addressService;
	
	

	@RequestMapping(method = RequestMethod.GET, path="/addresses/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "direcciones registradas x usuario ", response= AddressesResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> listAddress(
			@ApiParam(value = "id usuario", required = true)
			@PathVariable Long userId){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = addressService.listAddress(userId);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos addresses: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("addresses/"+userId+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("addresses/"+userId+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("addresses/"+userId+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/address/{addressId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "detalle direccion x id", response= AddressResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> getAdress(
			@ApiParam(value = "id direccion", required = true)
			@PathVariable Long addressId){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = addressService.getAddress(addressId);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get address: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("address/"+addressId+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("address/"+addressId+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("address/"+addressId+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.POST,path="/address", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar direccion", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> saveAddress(
			@ApiParam(value = "Estructura JSON de direcciones", required = true)
			@RequestBody AddressDto addressDto) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = addressService.save(addressDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos post address: " + tiempo);
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("save address IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("save address Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}

	@RequestMapping(method = RequestMethod.PUT,path="/address/{addressId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar o eliminar direccion", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud")
	})		
	public ResponseEntity<Object> update(
			@ApiParam(value = "Estructura JSON de direcciones", required = true)
			@PathVariable Long addressId, @RequestBody AddressDto addressDto) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = addressService.update(addressId,addressDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos put address: " + tiempo);
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/address/"+addressId+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/address/"+addressId+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
}
