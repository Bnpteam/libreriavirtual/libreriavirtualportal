package pe.gob.bnp.libreriavirtual.portal.payment.repository;

import org.springframework.stereotype.Repository;
import pe.gob.bnp.libreriavirtual.portal.payment.dto.RequestPaymentDto;
import pe.gob.bnp.libreriavirtual.portal.payment.dto.ResponsePaymentDto;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

@Repository
public interface PaymentRepository {

	public ResponseTransaction vposRequest(RequestPaymentDto RequestPaymentDto);
	
	public ResponseTransaction vposResponse(ResponsePaymentDto ResponsePaymentDto);
	
	public ResponseTransaction getPasarellaResponse(String operationNumber,String applicationId);
}
