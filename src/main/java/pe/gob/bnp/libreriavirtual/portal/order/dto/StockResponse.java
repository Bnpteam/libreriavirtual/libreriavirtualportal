package pe.gob.bnp.libreriavirtual.portal.order.dto;


public class StockResponse {
	
	private String bookId;
	public String getBookId() {
		return bookId;
	}
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	public String getBookSize() {
		return bookSize;
	}
	public void setBookSize(String bookSize) {
		this.bookSize = bookSize;
	}
	public String getBookStock() {
		return bookStock;
	}
	public void setBookStock(String bookStock) {
		this.bookStock = bookStock;
	}
	private String bookTitle;
	private String bookSize;
	private String bookStock;

}
