package pe.gob.bnp.libreriavirtual.portal.address.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.libreriavirtual.portal.address.model.Address;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.repository.BaseRepository;

@Repository
public interface AddressRepository extends BaseRepository<Address>{
	
	public ResponseTransaction listAddress(Long userId);
	
	public ResponseTransaction getAddress(Long addressId);

}
