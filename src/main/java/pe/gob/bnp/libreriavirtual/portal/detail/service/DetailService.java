package pe.gob.bnp.libreriavirtual.portal.detail.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.libreriavirtual.portal.detail.exception.DetailException;
import pe.gob.bnp.libreriavirtual.portal.detail.repository.DetailRepository;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

@Service
public class DetailService {
	
	@Autowired
	DetailRepository detailRepository;
	
	public ResponseTransaction searchBook(Long bookId) throws IOException {

		ResponseTransaction response = detailRepository.readBook(bookId);
		
		return DetailException.setMessageResponseBook(response);
	}
	
	public ResponseTransaction searchProduct(Long productId) throws IOException {

		ResponseTransaction response = detailRepository.readProduct(productId);
		
		return DetailException.setMessageResponseBook(response);
	}
	
	public ResponseTransaction listRecommended(Long bookId) throws IOException {
	
		ResponseTransaction response = detailRepository.listRecommended(bookId);

		return DetailException.setMessageResponseRecommended(response);
	}
	
	
	public ResponseTransaction listRecommendedProduct(Long productId) throws IOException {
		
		ResponseTransaction response = detailRepository.listRecommendedProduct(productId);

		return DetailException.setMessageResponseRecommended(response);
	}

}
