package pe.gob.bnp.libreriavirtual.portal.souvenir.exception;

import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

public class SouvenirException {
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";
	public static final  String error0002 ="El submenu no existe";
	
	public static final  String error0005ProductList="El id del grupo de producto no existe";
	public static final  String error0006ProductList="El id del tema no existe";
	
	
	public static ResponseTransaction setMessageResponseGroupList(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}		
		
		return response;
	}
	
	
	public static ResponseTransaction setMessageResponseProductList(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}
		
		
		if (response.getCodeResponse().equals("0005")) {
			response.setResponse(error0005ProductList);
		}
		
		if (response.getCodeResponse().equals("0006")) {
			response.setResponse(error0006ProductList);
		}
					
		
		return response;
	}

}
