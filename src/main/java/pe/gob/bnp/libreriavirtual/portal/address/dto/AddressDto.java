package pe.gob.bnp.libreriavirtual.portal.address.dto;


public class AddressDto {
	
	private String id;
	private String userId;
	private String identifier;
	private String description;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getCodeDepartment() {
		return codeDepartment;
	}
	public void setCodeDepartment(String codeDepartment) {
		this.codeDepartment = codeDepartment;
	}
	public String getCodeProvince() {
		return codeProvince;
	}
	public void setCodeProvince(String codeProvince) {
		this.codeProvince = codeProvince;
	}
	public String getCodeDistrict() {
		return codeDistrict;
	}
	public void setCodeDistrict(String codeDistrict) {
		this.codeDistrict = codeDistrict;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getPersonContact() {
		return personContact;
	}
	public void setPersonContact(String personContact) {
		this.personContact = personContact;
	}
	public String getCellphoneContact() {
		return cellphoneContact;
	}
	public void setCellphoneContact(String cellphoneContact) {
		this.cellphoneContact = cellphoneContact;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	private String countryId;
	private String codeDepartment;
	private String codeProvince;
	private String codeDistrict;
	private String reference;
	private String personContact;
	private String cellphoneContact;	
	private String enabled;

}
