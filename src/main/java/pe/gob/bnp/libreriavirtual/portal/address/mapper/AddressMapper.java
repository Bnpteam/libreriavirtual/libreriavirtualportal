package pe.gob.bnp.libreriavirtual.portal.address.mapper;

import org.springframework.stereotype.Component;

import pe.gob.bnp.libreriavirtual.portal.address.dto.AddressDto;
import pe.gob.bnp.libreriavirtual.portal.address.model.Address;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

@Component
public class AddressMapper {	
	
	public Address reverseMapperSave(AddressDto addressDto) {
		Address user = new Address();	
				
		user.setUserId(Utility.parseStringToLong(addressDto.getUserId()));
		
		user.setDescription(addressDto.getDescription());
		user.setIdentifier(addressDto.getIdentifier());
		user.setCountryId(Utility.parseStringToLong(addressDto.getCountryId()));
		user.setCodeDepartment(addressDto.getCodeDepartment());
		user.setCodeProvince(addressDto.getCodeProvince());		
		user.setCodeDistrict(addressDto.getCodeDistrict());	
	
		user.setReference(addressDto.getReference());
		user.setPersonContact(addressDto.getPersonContact());
		user.setCellphoneContact(addressDto.getCellphoneContact());
		
		return user;
	}
	

	public Address reverseMapperUpdate(Long id,AddressDto addressDto) {
		Address user = new Address();	
		
		addressDto.setId(Utility.parseLongToString(id));
		user.setId(Utility.parseStringToLong(addressDto.getId()));
		
		user.setUserId(Utility.parseStringToLong(addressDto.getUserId()));
		user.setDescription(addressDto.getDescription());
		user.setIdentifier(addressDto.getIdentifier());
		user.setCountryId(Utility.parseStringToLong(addressDto.getCountryId()));
		user.setCodeDepartment(addressDto.getCodeDepartment());
		user.setCodeProvince(addressDto.getCodeProvince());		
		user.setCodeDistrict(addressDto.getCodeDistrict());	
	
		user.setReference(addressDto.getReference());
		user.setPersonContact(addressDto.getPersonContact());
		user.setCellphoneContact(addressDto.getCellphoneContact());
		user.setIdEnabled(addressDto.getEnabled());
		
		return user;
	}
	
	

}
