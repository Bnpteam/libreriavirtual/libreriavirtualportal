package pe.gob.bnp.libreriavirtual.portal.auth.dto;


public class PortalCredential {

	private String email;

	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFlagRedSocial() {
		return flagRedSocial;
	}

	public void setFlagRedSocial(String flagRedSocial) {
		this.flagRedSocial = flagRedSocial;
	}

	private String flagRedSocial;

	public PortalCredential() {
		super();
		this.email = "";
		this.password = "";
	}

}
