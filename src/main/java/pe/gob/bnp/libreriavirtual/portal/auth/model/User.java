package pe.gob.bnp.libreriavirtual.portal.auth.model;

import java.util.Date;

import pe.gob.bnp.libreriavirtual.utilitary.common.Constants;
import pe.gob.bnp.libreriavirtual.utilitary.model.BaseEntity;

public class User  extends BaseEntity{
	
	private String userName;
	private String userSurname;	
	private String userEmail;
	private String userPassword;
	private Date   birthDate;
	private String tokenPassword;
	private Long   typeDocument;
	private String document;
	private String photo;
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserSurname() {
		return userSurname;
	}

	public void setUserSurname(String userSurname) {
		this.userSurname = userSurname;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getTokenPassword() {
		return tokenPassword;
	}

	public void setTokenPassword(String tokenPassword) {
		this.tokenPassword = tokenPassword;
	}

	public Long getTypeDocument() {
		return typeDocument;
	}

	public void setTypeDocument(Long typeDocument) {
		this.typeDocument = typeDocument;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getUserPasswordNew() {
		return userPasswordNew;
	}

	public void setUserPasswordNew(String userPasswordNew) {
		this.userPasswordNew = userPasswordNew;
	}

	private String provider;
	private String userPasswordNew;

	public User() {
		super();
		this.userName = Constants.EMPTY_STRING;
		this.userSurname = Constants.EMPTY_STRING;		
		this.userEmail= Constants.EMPTY_STRING;
		this.userPassword= Constants.EMPTY_STRING;
		this.userPasswordNew= Constants.EMPTY_STRING;
		this.birthDate= new Date();
		this.tokenPassword= Constants.EMPTY_STRING;		
		this.typeDocument= Constants.ZERO_LONG;
		this.document= Constants.EMPTY_STRING;
		this.photo= Constants.EMPTY_STRING;
		this.provider= Constants.EMPTY_STRING;
	}
	
}
