package pe.gob.bnp.libreriavirtual.portal.souvenir.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.ProductDto;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

@Repository
public interface SouvenirRepository {
	
	public ResponseTransaction listGroup(Long submenuId);
	
	public ResponseTransaction listTopic();
	
	public ResponseTransaction listNovelties(Long submenuId);
	
	public ResponseTransaction listProducts(ProductDto productDto);

}
