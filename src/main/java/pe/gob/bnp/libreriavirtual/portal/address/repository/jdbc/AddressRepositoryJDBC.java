package pe.gob.bnp.libreriavirtual.portal.address.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.libreriavirtual.config.DataSource;
import pe.gob.bnp.libreriavirtual.portal.address.dto.AddressResponse;
import pe.gob.bnp.libreriavirtual.portal.address.dto.AddressesResponse;
import pe.gob.bnp.libreriavirtual.portal.address.model.Address;
import pe.gob.bnp.libreriavirtual.portal.address.repository.AddressRepository;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

@Repository
public class AddressRepositoryJDBC implements AddressRepository {

	private static final Logger logger = LoggerFactory.getLogger(AddressRepositoryJDBC.class);

	private String messageSuccess = "Transacción exitosa.";

	@Override
	public ResponseTransaction persist(Address entity) {
		
        String dBTransaction = "{ Call PKG_API_ADDRESS.SP_PERSIST_ADDRESS(?,?,?,?,?,?,?,?,?,?,?,?)}";/*12*/
        ResponseTransaction response = new ResponseTransaction();       
	
		
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			con.setAutoCommit(false);
			cstm.setLong("P_USER_ID", entity.getUserId());
			cstm.setString("P_IDENTIFIER", Utility.getString(entity.getIdentifier()));
			cstm.setLong("P_COUNTRY",  entity.getCountryId());
			cstm.setString("P_CODE_DEPARTMENT",Utility.getString(entity.getCodeDepartment()));
			cstm.setString("P_CODE_PROVINCE",Utility.getString(entity.getCodeProvince()));
			cstm.setString("P_CODE_DISTRICT",Utility.getString(entity.getCodeDistrict()));	
			cstm.setString("P_DESCRIPTION", Utility.getString(entity.getDescription()));
			cstm.setString("P_PERSON_CONTACT",Utility.getString(entity.getPersonContact()));	
			cstm.setString("P_CELLPHONE_CONTACT",Utility.getString(entity.getCellphoneContact()));	
			cstm.setString("P_REFERENCE",Utility.getString(entity.getReference()));		
			
			cstm.registerOutParameter("S_ADDRESS_ID",OracleTypes.NUMBER);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				con.commit();

				Long id=  Utility.parseObjectToLong(cstm.getObject("S_ADDRESS_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				con.rollback();				
			}
		}catch(SQLException e) {
			logger.error("  PKG_API_ADDRESS.SP_PERSIST_ADDRESS: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		
		return response;
	}

	@Override
	public ResponseTransaction update(Address entity) {
		
        String dBTransaction = "{ Call PKG_API_ADDRESS.SP_UPDATE_ADDRESS(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";/*14*/
        ResponseTransaction response = new ResponseTransaction();       
				
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			con.setAutoCommit(false);
			cstm.setLong("P_ID",  entity.getId());			
			cstm.setLong("P_USER_ID",  entity.getUserId());
			cstm.setString("P_IDENTIFIER", Utility.getString(entity.getIdentifier()));
			cstm.setLong("P_COUNTRY",  entity.getCountryId());			
			cstm.setString("P_CODE_DEPARTMENT",Utility.getString(entity.getCodeDepartment()));
			cstm.setString("P_CODE_PROVINCE",Utility.getString(entity.getCodeProvince()));
			cstm.setString("P_CODE_DISTRICT",Utility.getString(entity.getCodeDistrict()));	
			cstm.setString("P_DESCRIPTION", Utility.getString(entity.getDescription()));			
			cstm.setString("P_PERSON_CONTACT",Utility.getString(entity.getPersonContact()));	
			cstm.setString("P_CELLPHONE_CONTACT",Utility.getString(entity.getCellphoneContact()));	
			cstm.setString("P_REFERENCE",Utility.getString(entity.getReference()));	
			cstm.setString("P_ENABLED",Utility.getString(entity.getIdEnabled()));	
			
			cstm.registerOutParameter("S_ADDRESS_ID",OracleTypes.NUMBER);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				con.commit();

				Long id=  Utility.parseObjectToLong(cstm.getObject("S_ADDRESS_ID"));
				response.setId(String.valueOf(id));
				response.setResponse(messageSuccess);
			}else {
				con.rollback();				
			}
		}catch(SQLException e) {
			logger.error("  PKG_API_ADDRESS.SP_UPDATE_ADDRESS: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;
	}

	@Override
	public ResponseTransaction listAddress(Long userId) {
		
		String dBTransaction = "{ Call PKG_API_ADDRESS.SP_LIST_ADDRESS(?,?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();
		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_USER_ID", userId);	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					AddressesResponse sliderResponse = new AddressesResponse();
					sliderResponse.setId(Utility.parseLongToString(rs.getLong("ADDRESS_ID")));
					sliderResponse.setUserId(Utility.getString(rs.getString("ADDRESS_USER_ID")));
					sliderResponse.setUserName(Utility.getString(rs.getString("ADDRESS_USER_NAME")));
					sliderResponse.setCountryId(Utility.getString(rs.getString("ADDRESS_COUNTRY_ID")));
					sliderResponse.setCountryName(Utility.getString(rs.getString("ADDRESS_COUNTRY_NAME")));					
					sliderResponse.setCodeDepartment(Utility.getString(rs.getString("CODE_DEPARTMENT")));
					sliderResponse.setDepartment(Utility.getString(rs.getString("DEPARTMENT")));
					sliderResponse.setCodeProvince(Utility.getString(rs.getString("CODE_PROVINCE")));
					sliderResponse.setProvince(Utility.getString(rs.getString("PROVINCE")));
					sliderResponse.setCodeDistrict(Utility.getString(rs.getString("CODE_DISTRICT")));
					sliderResponse.setDistrict(Utility.getString(rs.getString("DISTRICT")));					
					sliderResponse.setIdentifier(Utility.getString(rs.getString("ADDRESS_IDENTIFIER")));
					sliderResponse.setDescription(Utility.getString(rs.getString("ADDRESS_DESCRIPTION")));
					sliderResponse.setPersonContact(Utility.getString(rs.getString("ADDRESS_PERSON_CONTACT")));
					sliderResponse.setCellphoneContact(Utility.getString(rs.getString("ADDRESS_CELLPHONE")));					
					sliderResponse.setReference(Utility.getString(rs.getString("ADDRESS_REFERENCE")));
					
					slidersResponse.add(sliderResponse);
				}
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("PKG_API_ADDRESS.SP_LIST_ADDRESS: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		} 
		return response;
	}

	@Override
	public ResponseTransaction getAddress(Long addressId) {
		
		String dBTransaction = "{ Call PKG_API_ADDRESS.SP_GET_ADDRESS(?,?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			
			cstm.setLong("P_ADDRESS_ID", addressId);	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					AddressResponse sliderResponse = new AddressResponse();
					sliderResponse.setId(Utility.parseLongToString(rs.getLong("ADDRESS_ID")));
					sliderResponse.setUserId(Utility.getString(rs.getString("ADDRESS_USER_ID")));
					sliderResponse.setUserName(Utility.getString(rs.getString("ADDRESS_USER_NAME")));
					sliderResponse.setCountryId(Utility.getString(rs.getString("ADDRESS_COUNTRY_ID")));
					sliderResponse.setCountryName(Utility.getString(rs.getString("ADDRESS_COUNTRY_NAME")));						
					sliderResponse.setCodeDepartment(Utility.getString(rs.getString("CODE_DEPARTMENT")));
					sliderResponse.setDepartment(Utility.getString(rs.getString("DEPARTMENT")));
					sliderResponse.setCodeProvince(Utility.getString(rs.getString("CODE_PROVINCE")));
					sliderResponse.setProvince(Utility.getString(rs.getString("PROVINCE")));
					sliderResponse.setCodeDistrict(Utility.getString(rs.getString("CODE_DISTRICT")));
					sliderResponse.setDistrict(Utility.getString(rs.getString("DISTRICT")));					
					sliderResponse.setIdentifier(Utility.getString(rs.getString("ADDRESS_IDENTIFIER")));
					sliderResponse.setDescription(Utility.getString(rs.getString("ADDRESS_DESCRIPTION")));
					sliderResponse.setPersonContact(Utility.getString(rs.getString("ADDRESS_PERSON_CONTACT")));
					sliderResponse.setCellphoneContact(Utility.getString(rs.getString("ADDRESS_CELLPHONE")));					
					sliderResponse.setReference(Utility.getString(rs.getString("ADDRESS_REFERENCE")));
					
					slidersResponse.add(sliderResponse);
				}
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error(" PKG_API_ADDRESS.SP_GET_ADDRESS: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		}
		return response;
	}

}
