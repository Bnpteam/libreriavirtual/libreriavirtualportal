package pe.gob.bnp.libreriavirtual.portal.auth.mapper;

import org.springframework.stereotype.Component;

import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserDto;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserUpdateDto;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserxTokenDto;
import pe.gob.bnp.libreriavirtual.portal.auth.model.User;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

@Component
public class UserMapper {

	public User reverseMapperSave(UserDto userDto) {
		User user = new User();	
		
		user.setUserName(userDto.getName());
		user.setUserSurname(userDto.getSurname());
		user.setUserEmail(userDto.getEmail());
		user.setUserPassword(userDto.getPassword());		
		user.setBirthDate(Utility.parseStringToDate(userDto.getBirthDate()));	
		user.setTypeDocument(Utility.parseStringToLong(userDto.getTypeDocument()));
		user.setDocument(userDto.getDocument());
		user.setPhoto(userDto.getPhoto());
		user.setProvider(userDto.getProvider());
		return user;
	}
	
	public User reverseMapperTokenUpdate(String token,UserxTokenDto userDto) {
		User user = new User();	
		
		user.setTokenPassword(token);	
		user.setUserPassword(userDto.getPassword());			
		
		return user;
	}
	
	public User reverseMapperUpdate(Long id,UserUpdateDto userDto) {
		User user = new User();	
		
		userDto.setId(Utility.parseLongToString(id));
		user.setId(Utility.parseStringToLong(userDto.getId()));
		user.setUserName(userDto.getName());
		user.setUserSurname(userDto.getSurname());
		user.setUserEmail(userDto.getEmail());	
		user.setBirthDate(Utility.parseStringToDate(userDto.getBirthDate()));	
		user.setTypeDocument(Utility.parseStringToLong(userDto.getTypeDocument()));		
		user.setDocument(userDto.getDocument());
		user.setPhoto(userDto.getPhoto());
		user.setProvider(userDto.getProvider());
		user.setUserPassword(userDto.getPasswordOld());
		user.setUserPasswordNew(userDto.getPasswordNew());
		return user;
	}

}
