package pe.gob.bnp.libreriavirtual.portal.detail.exception;

import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

public class DetailException {
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";
	public static final  String error0002 ="El id del ejemplar no existe o no esta publicado";
	public static final  String error0003 ="La categoria asociada no existe";
	
	
	public static ResponseTransaction setMessageResponseRecommended(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003);
		}
		
		return response;
	}
	
	public static ResponseTransaction setMessageResponseBook(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}
		
	
		return response;
	}

}
