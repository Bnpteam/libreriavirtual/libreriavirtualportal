package pe.gob.bnp.libreriavirtual.portal.address.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.libreriavirtual.portal.address.dto.AddressDto;
import pe.gob.bnp.libreriavirtual.portal.address.exception.AddressException;
import pe.gob.bnp.libreriavirtual.portal.address.mapper.AddressMapper;
import pe.gob.bnp.libreriavirtual.portal.address.model.Address;
import pe.gob.bnp.libreriavirtual.portal.address.repository.AddressRepository;
import pe.gob.bnp.libreriavirtual.portal.address.validation.AddressValidation;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

@Service
public class AddressService {
	
	@Autowired
	AddressRepository addressRepository;
	
	@Autowired
	AddressMapper addressMapper;
	
	public ResponseTransaction listAddress(Long userId) throws IOException {

		ResponseTransaction response = addressRepository.listAddress(userId);
		
		return AddressException.setMessageResponseListAddress(response);
	}
	
	public ResponseTransaction getAddress(Long addressId) throws IOException {

		ResponseTransaction response = addressRepository.getAddress(addressId);
		
		return AddressException.setMessageResponseAddress(response);
	}
	
	public ResponseTransaction save(AddressDto addressDto) {

		Notification notification = AddressValidation.validation(addressDto);
		ResponseTransaction response = new ResponseTransaction();

		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		Address address = addressMapper.reverseMapperSave(addressDto);
		
		response = addressRepository.persist(address);
		
		return AddressException.setMessageResponseSave(response);

	}
	

	public ResponseTransaction update(Long id, AddressDto addressDto) {

		Notification notification = AddressValidation.validationUpdate(addressDto);
		ResponseTransaction response = new ResponseTransaction();
		
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		Address user = addressMapper.reverseMapperUpdate(id, addressDto);	
		
		response = addressRepository.update(user);	
		
		return AddressException.setMessageResponseUpdate(response);

	}

}
