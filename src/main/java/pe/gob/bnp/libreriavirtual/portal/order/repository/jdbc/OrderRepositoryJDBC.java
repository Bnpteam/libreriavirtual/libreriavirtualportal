package pe.gob.bnp.libreriavirtual.portal.order.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.libreriavirtual.config.DataSource;
import pe.gob.bnp.libreriavirtual.portal.order.dto.CalculateDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.CalculateResponse;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderCabResponse;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderDetailResponse;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderForeignResponse;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderResponse;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrdersResponse;
import pe.gob.bnp.libreriavirtual.portal.order.dto.StockResponse;
import pe.gob.bnp.libreriavirtual.portal.order.model.Order;
import pe.gob.bnp.libreriavirtual.portal.order.model.OrderDetail;
import pe.gob.bnp.libreriavirtual.portal.order.repository.OrderRepository;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

@Repository
public class OrderRepositoryJDBC implements OrderRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(OrderRepositoryJDBC.class);	
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction calculate(CalculateDto calculateDto) {
		
	        String dBTransaction = "{ Call PKG_API_ORDER.SP_CALCULATE(?,?,?,?,?,?,?,?)}";/*8*/
	             
	        List<Object> usersResponse = new ArrayList<>();
	        ResponseTransaction response = new ResponseTransaction();
	      
			ResultSet rs = null;
			try (Connection con = DataSource.getConnection();
					CallableStatement cstm = con.prepareCall(dBTransaction);){
				
				con.setAutoCommit(false);
				cstm.setString("P_AMOUNT",Utility.getString(calculateDto.getAmount()));
				cstm.setString("P_QUANTITY", Utility.getString(calculateDto.getQuantity()));
				cstm.setString("P_WEIGHT", Utility.getString(calculateDto.getWeight()));
				
				cstm.setString("P_TYPE_DELIVERY_ID", Utility.getString(calculateDto.getTypeDeliveryId()));
				cstm.setString("P_TYPE_SHIPPING_ID", Utility.getString(calculateDto.getTypeShippingId()));
				cstm.setString("P_ADDRESS_ID", Utility.getString(calculateDto.getAddressId()));
				
				cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
				cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
				cstm.execute();
				
				String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
				response.setCodeResponse(codigoResultado);
				if (Utility.getString(codigoResultado).equals("0000")) {
					con.commit();
					response.setResponse(messageSuccess);
					rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
					if (rs.next()) {
						CalculateResponse userResponse = new CalculateResponse(); 
						userResponse.setSubtotalCost(Utility.getString(rs.getString("AMOUNT")));
						userResponse.setShippingCost(Utility.getString(rs.getString("SHIPPING_COST")));
						userResponse.setIgvCost(Utility.getString(rs.getString("IGV_COST")));
						userResponse.setTotalCost(Utility.getString(rs.getString("TOTAL_COST")));
					
						
						usersResponse.add(userResponse);
					}	
					response.setList(usersResponse);			
				}else {
					con.rollback();
				}
				if (rs != null) {
					rs.close();
				}
			}catch(SQLException e) {
				logger.error("PKG_API_ORDER.SP_CALCULATE: "+e.getMessage());
				response.setCodeResponse(String.valueOf(e.getErrorCode()));			
				response.setResponse(e.getMessage());
				
			}
			return response;	
	}

	@Override
	public ResponseTransaction persist(Order entity) {
		
		String dBTransaction = "{ Call PKG_API_ORDER.SP_PERSIST_ORDER(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";/* 17 */
		
		List<Object> usersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			con.setAutoCommit(false);
			cstm.setString("P_CLIENT_ID", Utility.getString(entity.getClientId()));
			cstm.setDouble("P_ORDER_AMOUNT_SUBTOTAL", entity.getSubtotalCost());
			cstm.setDouble("P_ORDER_AMOUNT_SHIPPING", entity.getShippingCost());
			cstm.setDouble("P_ORDER_AMOUNT_IGV", entity.getIgvCost());
			cstm.setDouble("P_ORDER_AMOUNT_TOTAL",entity.getTotalCost());
			
			cstm.setInt("P_TYPE_REGION_ID", entity.getTypeRegionId());
			cstm.setInt("P_TYPE_DELIVERY_ID", entity.getTypeDeliveryId());
			cstm.setInt("P_TYPE_SHIPPING_ID", entity.getTypeShippingId());
			cstm.setInt("P_ADDRESS_ID", entity.getAddressId());
			cstm.setInt("P_TYPE_VOUCHER_ID", entity.getTypeVoucherId());
			
			cstm.setString("P_RUC", entity.getRuc());
			cstm.setString("P_RAZON_SOCIAL", entity.getRazonSocial());
			cstm.setString("P_FISCAL_ADDRESS", entity.getFiscalAddress());
			cstm.setString("P_NRO_DOCUMENTO", entity.getNroDocumento());
			cstm.setString("P_FULL_NAME", entity.getFullName());
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				con.commit();
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					
					OrderResponse orderResponse = new OrderResponse(); 
					response.setId(Utility.getString(rs.getString("ORDER_ID")));
					orderResponse.setOrderId(Utility.getString(rs.getString("ORDER_ID")));
					orderResponse.setOrderNro(Utility.getString(rs.getString("ORDER_NRO")));
					
					orderResponse.setAcquirerId(Utility.getString(rs.getString("ACQUIRER_ID")));
					orderResponse.setIdCommerce(Utility.getString(rs.getString("ID_COMMERCE")));
					orderResponse.setPurchaseOperationNumber(Utility.getString(rs.getString("PURCHASE_OPERATION_NUMBER")));
					orderResponse.setPurchaseAmount(Utility.getString(rs.getString("PURCHASE_AMOUNT")));
					orderResponse.setPurchaseCurrencyCode(Utility.getString(rs.getString("PURCHASE_CURRENCY_CODE")));
					orderResponse.setLanguage(Utility.getString(rs.getString("PASARELLA_LANGUAGE")));
					orderResponse.setShippingFirstName(Utility.getString(rs.getString("SHIPPING_FIRSTNAME")));
					orderResponse.setShippingLastName(Utility.getString(rs.getString("SHIPPING_LASTNAME")));
					orderResponse.setShippingEmail(Utility.getString(rs.getString("SHIPPING_EMAIL")));
					orderResponse.setShippingAddress(Utility.getString(rs.getString("SHIPPING_ADDRESS")));
					orderResponse.setShippingZIP(Utility.getString(rs.getString("SHIPPING_ZIP")));
					orderResponse.setShippingCity(Utility.getString(rs.getString("SHIPPING_CITY")));
					orderResponse.setShippingState(Utility.getString(rs.getString("SHIPPING_STATE")));
					orderResponse.setShippingCountry(Utility.getString(rs.getString("SHIPPING_COUNTRY")));
					orderResponse.setUserCommerce(Utility.getString(rs.getString("USER_COMMERCE")));
					orderResponse.setUserCodePayme(Utility.getString(rs.getString("USER_CODE_PAYME")));
					orderResponse.setDescriptionProducts(Utility.getString(rs.getString("DESCRIPTION_PRODUCTS")));
					orderResponse.setProgrammingLanguage(Utility.getString(rs.getString("PROGRAMMING_LANGUAGE")));
					
					String purchaseVerification=Utility.getString(rs.getString("PURCHASE_VERIFICATION"));
					String cryptPassarellaPOS=Utility.getStringSHA(purchaseVerification);
					
					
					
					///formatear data///
					String descriptionProducts=orderResponse.getDescriptionProducts();
					String firstName=orderResponse.getShippingFirstName();
					String lastName=orderResponse.getShippingLastName();
					String email=orderResponse.getShippingEmail();
					String address=orderResponse.getShippingAddress();
					String city=orderResponse.getShippingCity();
					String state=orderResponse.getShippingState();
					String country=orderResponse.getShippingCountry();			
					
					
					//validar caracteres especiales
					descriptionProducts=Utility.replaceSpecial(descriptionProducts);
					firstName=Utility.replaceSpecial(firstName);
					lastName=Utility.replaceSpecial(lastName);
					email=Utility.replaceSpecial(email);
					address=Utility.replaceSpecial(address);
					city=Utility.replaceSpecial(city);
					state=Utility.replaceSpecial(state);
					country=Utility.replaceSpecial(country);
					
					//validar tamaño
					descriptionProducts=Utility.substringSpecial(descriptionProducts,30);
					firstName=Utility.substringSpecial(firstName,30);
					lastName=Utility.substringSpecial(lastName,50);
					email=Utility.substringSpecial(email,50);
					address=Utility.substringSpecial(address,50);
					city=Utility.substringSpecial(city,50);
					state=Utility.substringSpecial(state,15);
					country=Utility.substringSpecial(country,2);	
					
					orderResponse.setDescriptionProducts(descriptionProducts);
					orderResponse.setShippingFirstName(firstName);
					orderResponse.setShippingLastName(lastName);
					orderResponse.setShippingEmail(email);
					orderResponse.setShippingAddress(address);
					orderResponse.setShippingCity(city);
					orderResponse.setShippingState(state);
					orderResponse.setShippingCountry(country);
					orderResponse.setPurchaseVerification(cryptPassarellaPOS);
					
					
					usersResponse.add(orderResponse);
				}	
				response.setList(usersResponse);	
			} else {
				con.rollback();
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("PKG_API_ORDER.SP_PERSIST_ORDER: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		}
		return response;
	}

	@Override
	public ResponseTransaction update(Order entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTransaction persistDetail(Long id, OrderDetail detail) {
		
		String dBTransaction = "{ Call PKG_API_ORDER.SP_PERSIST_ORDER_DETAIL(?,?,?,?,?,?,?)}";/*7*/
		ResponseTransaction response = new ResponseTransaction();
		
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
		
			con.setAutoCommit(false);
			cstm.setLong("P_ORDER_ID", id);
			cstm.setLong("P_BOOK_ID", detail.getBookId());
			cstm.setLong("P_PRODUCT_ID", detail.getProductId());
			cstm.setLong("P_SIZE_ID", detail.getSizeId());
			cstm.setLong("P_QUANTITY", detail.getQuantity());
			
			cstm.registerOutParameter("S_ORDER_DETAIL_ID", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				con.commit();
				Long detailId = Utility.parseObjectToLong(cstm.getObject("S_ORDER_DETAIL_ID"));
				response.setId(String.valueOf(detailId));
				response.setResponse(messageSuccess);
			} else {
				con.rollback();
				logger.error("Error: No se pudo insertar el registro detalle del curso");
				response.setCodeResponse("0007");
				response.setResponse("Error: No se pudo insertar el registro detalle del curso");
			}
								
			
		} catch (SQLException e) {
			logger.error("PKG_API_ORDER.SP_PERSIST_ORDER_DETAIL: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		} 
		return response;
	}

	@Override
	public ResponseTransaction listOrder(Long userId) {
		String dBTransaction = "{ Call PKG_API_ORDER.SP_LIST_ORDER(?,?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_USER_ID", userId);	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					OrdersResponse sliderResponse = new OrdersResponse();
					sliderResponse.setId(Utility.parseLongToString(rs.getLong("ORDER_ID")));
					sliderResponse.setDate(Utility.getString(rs.getString("ORDER_DATE")));
					sliderResponse.setBooks(Utility.getString(rs.getString("ORDER_BOOKS")));
					sliderResponse.setNumber(Utility.getString(rs.getString("ORDER_NRO")));
					sliderResponse.setQuantity(Utility.getString(rs.getString("ORDER_QUANTITY")));	
					sliderResponse.setSubtotal(Utility.getString(rs.getString("ORDER_SUBTOTAL")));
					sliderResponse.setShipping(Utility.getString(rs.getString("ORDER_SHIPPING")));
					sliderResponse.setIgv(Utility.getString(rs.getString("ORDER_IGV")));
					sliderResponse.setTotal(Utility.getString(rs.getString("ORDER_TOTAL")));
					sliderResponse.setStateId(Utility.getString(rs.getString("ORDER_STATE_ID")));
					sliderResponse.setStateName(Utility.getString(rs.getString("ORDER_STATE_NAME")));
					
					sliderResponse.setVoucherType(Utility.getString(rs.getString("VOUCHER_TYPE")));
					sliderResponse.setVoucherNro(Utility.getString(rs.getString("VOUCHER_NRO")));
					sliderResponse.setDeliveryType(Utility.getString(rs.getString("DELIVERY_TYPE")));
					
					sliderResponse.setRegionType(Utility.getString(rs.getString("REGION_TYPE")));
					sliderResponse.setCountryName(Utility.getString(rs.getString("COUNTRY_NAME")));
					sliderResponse.setCountryAddress(Utility.getString(rs.getString("COUNTRY_ADDRESS")));
					
					slidersResponse.add(sliderResponse);
				}
				response.setList(slidersResponse);
			}
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("PKG_API_ORDER.SP_LIST_ORDER: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		}
		return response;
	}

	@Override
	public ResponseTransaction getOrderDetail(Long orderId) {
		
		String dBTransaction = "{ Call PKG_API_ORDER.SP_GET_ORDER(?,?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_ORDER_ID", orderId);	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					OrderDetailResponse sliderResponse = new OrderDetailResponse();
					sliderResponse.setOrderDetailId(Utility.parseLongToString(rs.getLong("ORDER_DETAIL_ID")));
					sliderResponse.setBookId(Utility.parseLongToString(rs.getLong("BOOK_ID")));
					sliderResponse.setBookImage(Utility.getString(rs.getString("BOOK_IMAGE")));
					sliderResponse.setBookTitle(Utility.getString(rs.getString("BOOK_TITLE")));
					sliderResponse.setBookSubtitle(Utility.getString(rs.getString("BOOK_SUBTITLE")));
					sliderResponse.setBookAuthorId(Utility.getString(rs.getString("BOOK_AUTHOR_ID")));						
					sliderResponse.setBookAuthorName(Utility.getString(rs.getString("BOOK_AUTHOR_NAME")));
					sliderResponse.setBookMaterialId(Utility.getString(rs.getString("BOOK_MATERIAL_ID")));
					sliderResponse.setBookMaterialName(Utility.getString(rs.getString("BOOK_MATERIAL_NAME")));
					sliderResponse.setBookYearId(Utility.getString(rs.getString("BOOK_YEAR_ID")));
					sliderResponse.setBookYearName(Utility.getString(rs.getString("BOOK_YEAR_NAME")));
					sliderResponse.setBookSizeId(Utility.getString(rs.getString("BOOK_SIZE_ID")));
					sliderResponse.setBookSizeName(Utility.getString(rs.getString("BOOK_SIZE_NAME")));
					sliderResponse.setBookPrice(Utility.getString(rs.getString("BOOK_PRICE")));					
					sliderResponse.setOrderDetailQuantity(Utility.getString(rs.getString("ORDER_DETAIL_QUANTITY")));
					sliderResponse.setOrderDetailSubtotal(Utility.getString(rs.getString("ORDER_DETAIL_SUBTOTAL")));
					
					slidersResponse.add(sliderResponse);
				}
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error(" PKG_API_ORDER.SP_GET_ORDER: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		} 
		return response;
	}

	@Override
	public ResponseTransaction persistForeign(Order entity) {
		
		String dBTransaction = "{ Call PKG_API_ORDER.SP_PERSIST_ORDER_FOREIGN(?,?,?,?,?,?,?,?,?)}";/* 9 */
		
		List<Object> usersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();
		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			con.setAutoCommit(false);
			cstm.setString("P_CLIENT_ID", Utility.getString(entity.getClientId()));
			cstm.setDouble("P_ORDER_AMOUNT_SUBTOTAL", entity.getSubtotalCost());
			cstm.setDouble("P_ORDER_AMOUNT_SHIPPING", entity.getShippingCost());
			cstm.setDouble("P_ORDER_AMOUNT_IGV", entity.getIgvCost());
			cstm.setDouble("P_ORDER_AMOUNT_TOTAL",entity.getTotalCost());
			
			cstm.setInt("P_ORDER_FOREIGN_COUNTRY", entity.getCountryId());
			cstm.setString("P_ORDER_FOREIGN_ADDRESS", entity.getCountryAddress());
			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (response.getCodeResponse().equals("0000")) {
				con.commit();
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					
					OrderForeignResponse orderResponse = new OrderForeignResponse(); 
					response.setId(Utility.getString(rs.getString("ORDER_ID")));
					orderResponse.setOrderId(Utility.getString(rs.getString("ORDER_ID")));
					orderResponse.setOrderNro(Utility.getString(rs.getString("ORDER_NRO")));					
					orderResponse.setClientName(Utility.getString(rs.getString("NAME_CLIENT")));
					orderResponse.setEmailName(Utility.getString(rs.getString("EMAIL_CLIENT")));
					orderResponse.setCountryName(Utility.getString(rs.getString("ORDER_FOREIGN_COUNTRY")));
					orderResponse.setCountryAddress(Utility.getString(rs.getString("ORDER_FOREIGN_ADDRESS")));
					orderResponse.setEmailFlag(Utility.getString(rs.getString("ENVIO_EMAIL")));					
					
					usersResponse.add(orderResponse);
				}	
				response.setList(usersResponse);	
			} else {
				con.rollback();
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("PKG_API_ORDER.SP_PERSIST_ORDER_FOREIGN: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		} 
		return response;
	}

	@Override
	public ResponseTransaction getOrderCab(Long orderId) {
		
		String dBTransaction = "{ Call PKG_API_ORDER.SP_GET_ORDERCAB(?,?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_ORDER_ID", orderId);	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				if (rs.next()) {
					OrderCabResponse sliderResponse = new OrderCabResponse();
					sliderResponse.setId(Utility.parseLongToString(rs.getLong("ORDER_ID")));
					sliderResponse.setDate(Utility.getString(rs.getString("ORDER_DATE")));
					sliderResponse.setBooks(Utility.getString(rs.getString("ORDER_BOOKS")));
					sliderResponse.setNumber(Utility.getString(rs.getString("ORDER_NRO")));
					sliderResponse.setQuantity(Utility.getString(rs.getString("ORDER_QUANTITY")));	
					sliderResponse.setSubtotal(Utility.getString(rs.getString("ORDER_SUBTOTAL")));
					sliderResponse.setShipping(Utility.getString(rs.getString("ORDER_SHIPPING")));
					sliderResponse.setIgv(Utility.getString(rs.getString("ORDER_IGV")));
					sliderResponse.setTotal(Utility.getString(rs.getString("ORDER_TOTAL")));
					sliderResponse.setStateId(Utility.getString(rs.getString("ORDER_STATE_ID")));
					sliderResponse.setStateName(Utility.getString(rs.getString("ORDER_STATE_NAME")));
					
					sliderResponse.setVoucherType(Utility.getString(rs.getString("VOUCHER_TYPE")));
					sliderResponse.setVoucherNro(Utility.getString(rs.getString("VOUCHER_NRO")));
					sliderResponse.setDeliveryType(Utility.getString(rs.getString("DELIVERY_TYPE")));
					
					sliderResponse.setRegionType(Utility.getString(rs.getString("REGION_TYPE")));
					sliderResponse.setCountryName(Utility.getString(rs.getString("COUNTRY_NAME")));
					sliderResponse.setCountryAddress(Utility.getString(rs.getString("COUNTRY_ADDRESS")));
					
					slidersResponse.add(sliderResponse);
				}
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("PKG_API_ORDER.SP_GET_ORDERCAB: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		} 
		return response;
	}

	@Override
	public ResponseTransaction validateStock(OrderDetail detail) {
		
        String dBTransaction = "{ Call PKG_API_ORDER.SP_STOCK(?,?,?,?,?,?)}";/*6*/
             
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setString("P_BOOK_ID",Utility.parseLongToString(detail.getBookId()));
			cstm.setString("P_PRODUCT_ID",Utility.parseLongToString(detail.getProductId()));
			cstm.setString("P_SIZE_ID",Utility.parseLongToString(detail.getSizeId()));
			cstm.setString("P_QUANTITY", Utility.parseLongToString(detail.getQuantity()));
			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")||Utility.getString(codigoResultado).equals("0019")) {
				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					StockResponse userResponse = new StockResponse(); 
					userResponse.setBookId(Utility.getString(rs.getString("BOOK_ID")));
					userResponse.setBookStock(Utility.getString(rs.getString("BOOK_STOCK")));
					userResponse.setBookTitle(Utility.getString(rs.getString("BOOK_TITLE")));
					userResponse.setBookSize(Utility.getString(rs.getString("BOOK_SIZE")));				
					
					usersResponse.add(userResponse);
				}	
				response.setList(usersResponse);			
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_ORDER.SP_STOCK: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}

	
}
