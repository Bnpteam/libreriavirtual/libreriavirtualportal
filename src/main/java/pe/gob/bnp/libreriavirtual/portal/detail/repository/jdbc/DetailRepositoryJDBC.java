package pe.gob.bnp.libreriavirtual.portal.detail.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.libreriavirtual.config.DataSource;
import pe.gob.bnp.libreriavirtual.portal.detail.dto.BookDetailResponse;
import pe.gob.bnp.libreriavirtual.portal.detail.dto.ProductDetailResponse;
import pe.gob.bnp.libreriavirtual.portal.detail.dto.RecommendedProductResponse;
import pe.gob.bnp.libreriavirtual.portal.detail.dto.RecommendedResponse;
import pe.gob.bnp.libreriavirtual.portal.detail.dto.SizeResponse;
import pe.gob.bnp.libreriavirtual.portal.detail.repository.DetailRepository;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

@Repository
public class DetailRepositoryJDBC  implements DetailRepository {

	private static final Logger logger = LoggerFactory.getLogger(DetailRepositoryJDBC.class);

	private String messageSuccess = "Transacción exitosa.";
	
	@Override
	public ResponseTransaction readBook(Long bookId) {
		
        String dBTransaction = "{ Call PKG_API_DETAIL.SP_GET_BOOK(?,?,?)}";             
        
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_BOOK_ID", bookId);
		
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {					
					
					BookDetailResponse sliderResponse = new BookDetailResponse();
				    sliderResponse.setId(Utility.parseLongToString(rs.getLong("BOOK_ID")));
					sliderResponse.setTitle(Utility.getString(rs.getString("BOOK_TITLE")));
					sliderResponse.setSubtitle(Utility.getString(rs.getString("BOOK_SUBTITLE")));
					sliderResponse.setDescription(Utility.getString(rs.getString("BOOK_DESCRIPTION")));
					sliderResponse.setAuthorId(Utility.getString(rs.getString("BOOK_AUTHOR_ID")));
					sliderResponse.setAuthorName(Utility.getString(rs.getString("BOOK_AUTHOR_NAME")));
					sliderResponse.setPublicationId(Utility.getString(rs.getString("BOOK_PUBLICATION_ID")));
					sliderResponse.setPublicationName(Utility.getString(rs.getString("BOOK_PUBLICATION_NAME")));
					sliderResponse.setMaterialId(Utility.getString(rs.getString("BOOK_MATERIAL_ID")));
					sliderResponse.setMaterialName(Utility.getString(rs.getString("BOOK_MATERIAL_NAME")));
					sliderResponse.setCategoryId(Utility.getString(rs.getString("BOOK_CATEGORY_ID")));
					sliderResponse.setCategoryName(Utility.getString(rs.getString("BOOK_CATEGORY_NAME")));
					sliderResponse.setYearId(Utility.getString(rs.getString("BOOK_YEAR_ID")));
					sliderResponse.setYearName(Utility.getString(rs.getString("BOOK_YEAR_NAME")));
					//sliderResponse.setPrice(Utility.parseDoubleToString(rs.getDouble("BOOK_PRICE")));
					sliderResponse.setPrice(Utility.getString(rs.getString("BOOK_PRICE")));
					sliderResponse.setQuantity(Utility.parseLongToString(rs.getLong("BOOK_QUANTITY")));
					sliderResponse.setStock(Utility.getString(rs.getString("BOOK_STOCK")));
					sliderResponse.setPages(Utility.getString(rs.getString("BOOK_PAGES")));
					sliderResponse.setMeasure(Utility.getString(rs.getString("BOOK_MEASURE")));
					sliderResponse.setWeight(Utility.getString(rs.getString("BOOK_WEIGHT")));
					sliderResponse.setWeightView(Utility.getString(rs.getString("BOOK_WEIGHT_VIEW")));
					sliderResponse.setImage(Utility.getString(rs.getString("BOOK_IMAGE")));
					//sliderResponse.setTotal(Utility.getString(rs.getString("TOTAL_RECORDS")));
				
					resourcesResponse.add(sliderResponse);
				}

				response.setList(resourcesResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_DETAIL.SP_GET_BOOK: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}
	
	
	@Override
	public ResponseTransaction listRecommended(Long bookId) {
        String dBTransaction = "{ Call PKG_API_DETAIL.SP_LIST_RECOMMENDED(?,?,?)}";
        
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			
			cstm.setLong("P_BOOK_ID", bookId);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					RecommendedResponse resourceResponse = new RecommendedResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("BOOK_ID")));
					resourceResponse.setTitle(Utility.getString(rs.getString("BOOK_TITLE")));
					resourceResponse.setSubtitle(Utility.getString(rs.getString("BOOK_SUBTITLE")));
					resourceResponse.setAuthor(Utility.getString(rs.getString("BOOK_AUTHOR")));
					resourceResponse.setMaterial(Utility.getString(rs.getString("BOOK_MATERIAL")));
					resourceResponse.setPrice(Utility.getString(rs.getString("BOOK_PRICE")));
					resourceResponse.setQuantity(Utility.parseLongToString(rs.getLong("BOOK_QUANTITY")));
					resourceResponse.setStock(Utility.getString(rs.getString("BOOK_STOCK")));
					resourceResponse.setImage(Utility.getString(rs.getString("BOOK_IMAGE")));
					
					
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_DETAIL.SP_LIST_RECOMMENDED: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;
	}


	@Override
	public ResponseTransaction listRecommendedProduct(Long productId) {
	
        String dBTransaction = "{ Call PKG_API_DETAIL.SP_LIST_RECOMMENDED_PRODUCT(?,?,?)}";
        
        List<Object> resourcesResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
						
			cstm.setLong("P_PRODUCT_ID", productId);			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					RecommendedProductResponse resourceResponse = new RecommendedProductResponse();
					resourceResponse.setId(Utility.parseLongToString(rs.getLong("PRODUCT_ID")));
					resourceResponse.setTitle(Utility.getString(rs.getString("PRODUCT_TITLE")));
					resourceResponse.setSubtitle(Utility.getString(rs.getString("PRODUCT_SUBTITLE")));
				
					resourceResponse.setPrice(Utility.getString(rs.getString("PRODUCT_PRICE")));
					resourceResponse.setQuantity(Utility.parseLongToString(rs.getLong("PRODUCT_QUANTITY")));
					resourceResponse.setStock(Utility.getString(rs.getString("PRODUCT_STOCK")));
					resourceResponse.setImage(Utility.getString(rs.getString("PRODUCT_IMAGE")));
					
					
					resourcesResponse.add(resourceResponse);
				}
				response.setList(resourcesResponse);
			}
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_DETAIL.SP_LIST_RECOMMENDED_PRODUCT: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;
	}
	
	
	@Override
	public ResponseTransaction readProduct(Long productId) {
		
        String dBTransaction = "{ Call PKG_API_DETAIL.SP_GET_PRODUCT(?,?,?,?)}";
        //ProductDetailResponse sliderResponse = new ProductDetailResponse();
        List<Object> slider1Response = new ArrayList<>();
        ProductDetailResponse sliderResponse = new ProductDetailResponse();
        ResponseTransaction response = new ResponseTransaction();
        
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_PRODUCT_ID", productId);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);		
			cstm.registerOutParameter("S_C_CURSOR_SIZE",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				if (rs.next()) {
					
				    sliderResponse.setId(Utility.parseLongToString(rs.getLong("PRODUCT_ID")));
					sliderResponse.setTitle(Utility.getString(rs.getString("PRODUCT_TITLE")));
					sliderResponse.setSubtitle(Utility.getString(rs.getString("PRODUCT_SUBTITLE")));
					sliderResponse.setDescription(Utility.getString(rs.getString("PRODUCT_DESCRIPTION")));
					sliderResponse.setGroupId(Utility.getString(rs.getString("PRODUCT_GROUP_ID")));
					sliderResponse.setGroupName(Utility.getString(rs.getString("PRODUCT_GROUP_NAME")));
					sliderResponse.setTopicId(Utility.getString(rs.getString("PRODUCT_TOPIC_ID")));
					sliderResponse.setTopicName(Utility.getString(rs.getString("PRODUCT_TOPIC_NAME")));
					
					//sliderResponse.setPrice(Utility.parseDoubleToString(rs.getDouble("BOOK_PRICE")));
					sliderResponse.setPrice(Utility.getString(rs.getString("PRODUCT_PRICE")));
					sliderResponse.setQuantity(Utility.parseLongToString(rs.getLong("PRODUCT_QUANTITY")));
					sliderResponse.setStock(Utility.getString(rs.getString("PRODUCT_STOCK")));
					sliderResponse.setPaper(Utility.getString(rs.getString("PRODUCT_PAPER")));
					sliderResponse.setPages(Utility.getString(rs.getString("PRODUCT_PAGES")));
					sliderResponse.setGrammage(Utility.getString(rs.getString("PRODUCT_GRAMMAGE")));
					sliderResponse.setMeasure(Utility.getString(rs.getString("PRODUCT_MEASURE")));
					sliderResponse.setMaterial(Utility.getString(rs.getString("PRODUCT_MATERIAL")));
					sliderResponse.setLife(Utility.getString(rs.getString("PRODUCT_LIFE")));		
					
					sliderResponse.setWeight(Utility.getString(rs.getString("PRODUCT_WEIGHT")));
					sliderResponse.setWeightView(Utility.getString(rs.getString("PRODUCT_WEIGHT_VIEW")));
					sliderResponse.setImage(Utility.getString(rs.getString("PRODUCT_IMAGE")));	
					
				}	
				if (rs != null) {
					rs.close();
				}	
				
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_SIZE");
				List<SizeResponse> buttonDto = new ArrayList<>();
				while (rs.next()) {
					SizeResponse entidad = new SizeResponse();
					entidad.setId(Utility.parseLongToString(rs.getLong("SIZE_ID")));								
					entidad.setName(Utility.getString(rs.getString("SIZE_NAME")));
					entidad.setDescription(Utility.getString(rs.getString("SIZE_DESCRIPTION")));	
					entidad.setOrder(Utility.parseLongToString(rs.getLong("SIZE_ORDER")));					
					buttonDto.add(entidad);
				}	
				sliderResponse.setSize(buttonDto);		
				slider1Response.add(sliderResponse);
				response.setList(slider1Response);
				
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("get PKG_API_DETAIL.SP_GET_PRODUCT: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}

}
