package pe.gob.bnp.libreriavirtual.portal.address.validation;

import pe.gob.bnp.libreriavirtual.portal.address.dto.AddressDto;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

public class AddressValidation {
	
	private static String messageUserSave = "No se encontraron datos de la direccion";	
	private static String messageSurnameSave = "Se debe ingresar un usuario";		
	
	
	public static Notification validation(AddressDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageUserSave);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getUserId())) {
			notification.addError(messageSurnameSave);
		}
		
				

		return notification;
	}
	
	public static Notification validationUpdate(AddressDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageUserSave);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getUserId())) {
			notification.addError(messageSurnameSave);
		}	

		return notification;
	}

}
