package pe.gob.bnp.libreriavirtual.portal.start.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.libreriavirtual.portal.start.dto.BookDto;
import pe.gob.bnp.libreriavirtual.portal.start.dto.MenusResponse;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

@Repository
public interface StartRepository {
	
	public ResponseTransaction listNovelties();
	
	public ResponseTransaction listBooks(BookDto bookDto);
	
	public ResponseTransaction listMenus();
	
	public MenusResponse readMenu(Long id);

}


