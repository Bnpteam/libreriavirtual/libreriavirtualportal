package pe.gob.bnp.libreriavirtual.portal.auth.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.libreriavirtual.portal.auth.dto.PortalCredential;
import pe.gob.bnp.libreriavirtual.portal.auth.model.User;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.repository.BaseRepository;

@Repository
public interface UserRepository extends BaseRepository<User>{	

	public ResponseTransaction loginPortal(PortalCredential credential);
	
	public ResponseTransaction recoveryPassword(String token,String email);
	
	public ResponseTransaction persistCuenta(User entity);	
	
	public ResponseTransaction updateToken(User entity);
	
	public ResponseTransaction read(Long id);	
	
}
