package pe.gob.bnp.libreriavirtual.portal.address.model;


import pe.gob.bnp.libreriavirtual.utilitary.common.Constants;
import pe.gob.bnp.libreriavirtual.utilitary.model.BaseEntity;

public class Address extends BaseEntity{
	
	private Long   userId;
	private String description;
	private String identifier;
	private Long   countryId;
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public String getCodeDepartment() {
		return codeDepartment;
	}

	public void setCodeDepartment(String codeDepartment) {
		this.codeDepartment = codeDepartment;
	}

	public String getCodeProvince() {
		return codeProvince;
	}

	public void setCodeProvince(String codeProvince) {
		this.codeProvince = codeProvince;
	}

	public String getCodeDistrict() {
		return codeDistrict;
	}

	public void setCodeDistrict(String codeDistrict) {
		this.codeDistrict = codeDistrict;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getPersonContact() {
		return personContact;
	}

	public void setPersonContact(String personContact) {
		this.personContact = personContact;
	}

	public String getCellphoneContact() {
		return cellphoneContact;
	}

	public void setCellphoneContact(String cellphoneContact) {
		this.cellphoneContact = cellphoneContact;
	}

	private String codeDepartment;
	private String codeProvince;
	private String codeDistrict;
	private String reference;
	private String personContact;
	private String cellphoneContact;	
	
	public Address() {
		super();
		this.userId= Constants.ZERO_LONG;
		this.description = Constants.EMPTY_STRING;
		this.identifier = Constants.EMPTY_STRING;		
		this.codeDepartment= Constants.EMPTY_STRING;
		this.codeProvince= Constants.EMPTY_STRING;
		this.codeDistrict= Constants.EMPTY_STRING;
		this.reference= Constants.EMPTY_STRING;		
		
		this.personContact= Constants.EMPTY_STRING;
		this.cellphoneContact= Constants.EMPTY_STRING;
		this.countryId= Constants.ZERO_LONG;
	}
	

}
