package pe.gob.bnp.libreriavirtual.portal.auth.validation;

import pe.gob.bnp.libreriavirtual.portal.auth.dto.PortalCredential;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.RecoveryPasswordRequest;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserDto;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserUpdateDto;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserxTokenDto;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

public class UserValidation {
	
	private static String messageUserSave = "No se encontraron datos del Usuario";
	private static String messageUserLogin = "No se encontraron datos de acceso";
	
	private static String messageUserPassword = "No se encontraron datos";
	private static String messageNameSave = "Se debe ingresar un nombre de Usuario";	
	private static String messageSurnameSave = "Se debe ingresar un apellido de Usuario";	
	private static String messageEmailSave = "Se debe ingresar un email de Usuario";	
	private static String messageToken= "Se debe ingresar el token de recuperación de contraseña";
	private static String messagePasswordSave = "Se debe ingresar el password de Usuario";
	private static String messageUserGet = "El id debe ser mayor a cero";
	
	
	public static Notification validation(PortalCredential credential) {

		Notification notification = new Notification();

		if (credential == null) {
			notification.addError(messageUserLogin);
			return notification;
		}

		if (Utility.isEmptyOrNull(credential.getEmail())) {
			notification.addError(messageEmailSave);
		}
		
		if (credential.getFlagRedSocial()==null) {
			if (Utility.isEmptyOrNull(credential.getPassword())) {
				notification.addError(messagePasswordSave);
			}
		}
		
		if (credential.getFlagRedSocial()!=null && !credential.getFlagRedSocial().equalsIgnoreCase("1") ) {
			
			if (Utility.isEmptyOrNull(credential.getPassword())) {
				notification.addError(messagePasswordSave);
			}
		}
		

		return notification;
	}
	
	
	
	public static Notification validation(RecoveryPasswordRequest recoveryPassword) {

		Notification notification = new Notification();

		if (recoveryPassword == null) {
			notification.addError(messageUserPassword);
			return notification;
		}

		if (Utility.isEmptyOrNull(recoveryPassword.getEmail())) {
			notification.addError(messageEmailSave);
		}		

		return notification;
	}
	
	
	public static Notification validation(UserDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageUserSave);
			return notification;
		}

		if (Utility.isEmptyOrNull(dto.getName())) {
			notification.addError(messageNameSave);
		}
		
		if (Utility.isEmptyOrNull(dto.getSurname())) {
			notification.addError(messageSurnameSave);
		}
		
		if (Utility.isEmptyOrNull(dto.getEmail())) {
			notification.addError(messageEmailSave);
		}
		
			

		return notification;
	}
	
	
	
	public static Notification validationUpdate(UserUpdateDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageUserSave);
			return notification;
		}

		if (Utility.isEmptyOrNull(dto.getName())) {
			notification.addError(messageNameSave);
		}
		
		if (Utility.isEmptyOrNull(dto.getSurname())) {
			notification.addError(messageSurnameSave);
		}
		
		if (Utility.isEmptyOrNull(dto.getEmail())) {
			notification.addError(messageEmailSave);
		}		

		return notification;
	}
	
	
	
	public static Notification validationTokenUpdate(UserxTokenDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageUserSave);
			return notification;
		}	
		
		if (Utility.isEmptyOrNull(dto.getPassword())) {
			notification.addError(messagePasswordSave);
		}
		
		return notification;
	}
	
	
	public static Notification validation(String token) {

		Notification notification = new Notification();

		if (Utility.isEmptyOrNull(token)) {
			notification.addError(messageToken);
		}

		return notification;
	}
	
	public static Notification validation(Long id) {
		Notification notification = new Notification();
		if (id <=0L) {
			notification.addError(messageUserGet);
			return notification;
		}		
		return notification;
	}	

}
