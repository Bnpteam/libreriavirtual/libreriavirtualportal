package pe.gob.bnp.libreriavirtual.portal.auth.dto;

import pe.gob.bnp.libreriavirtual.portal.auth.model.User;
import pe.gob.bnp.libreriavirtual.utilitary.common.Constants;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

public class UserDto {
	
	private String id;
	private String name;
	private String surname;
	private String email;
	private String password;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getTypeDocument() {
		return typeDocument;
	}

	public void setTypeDocument(String typeDocument) {
		this.typeDocument = typeDocument;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	private String birthDate;
	private String typeDocument;
	private String document;
	private String photo;
	private String provider;

	
	public UserDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.name = Constants.EMPTY_STRING;
		this.surname = Constants.EMPTY_STRING;
		this.email= Constants.EMPTY_STRING;
		this.password= Constants.EMPTY_STRING;
		this.birthDate=Constants.EMPTY_STRING;
		this.typeDocument=Constants.EMPTY_STRING;
		this.document=Constants.EMPTY_STRING;	
		this.photo=Constants.EMPTY_STRING;
		this.provider=Constants.EMPTY_STRING;
		
	}	

	public UserDto(User user) {
		super();
		this.id =Utility.parseLongToString(user.getId());
		this.name =Utility.getString(user.getUserName()); 
		this.surname = Utility.getString(user.getUserSurname());
		this.email= Utility.getString(user.getUserEmail());
		this.password= Utility.getString(user.getUserPassword());
		this.birthDate=Utility.parseDateToString(user.getBirthDate());		
		this.typeDocument =Utility.parseLongToString(user.getTypeDocument()); 
		this.document =Utility.getString(user.getDocument()); 
		this.photo=Utility.getString(user.getPhoto()); 
		this.provider=Utility.getString(user.getProvider()); 
		
	}


}
