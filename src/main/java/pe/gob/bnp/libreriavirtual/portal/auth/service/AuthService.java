package pe.gob.bnp.libreriavirtual.portal.auth.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.PortalCredential;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.RecoveryPasswordRequest;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserDto;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserUpdateDto;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserxTokenDto;
import pe.gob.bnp.libreriavirtual.portal.auth.exception.UserException;
import pe.gob.bnp.libreriavirtual.portal.auth.mapper.UserMapper;
import pe.gob.bnp.libreriavirtual.portal.auth.model.User;
import pe.gob.bnp.libreriavirtual.portal.auth.repository.UserRepository;
import pe.gob.bnp.libreriavirtual.portal.auth.validation.UserValidation;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;
import pe.gob.bnp.libreriavirtual.utilitary.service.UtilitaryService;

@Service
public class AuthService {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthService.class);

	@Autowired
	UserRepository userRepository;	

	@Autowired
	UtilitaryService utilityService;

	@Autowired
	UserMapper userMapper;	

	public ResponseTransaction loginPortal(PortalCredential credential) {

		ResponseTransaction response = new ResponseTransaction();

		Notification notification = UserValidation.validation(credential);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		response = userRepository.loginPortal(credential);

		return UserException.setMessageResponseLoginPortal(response);
	}
	
	public ResponseTransaction signupPortal(UserDto userDto) {

		Notification notification = UserValidation.validation(userDto);
		ResponseTransaction response = new ResponseTransaction();

		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		User user = userMapper.reverseMapperSave(userDto);
		
		response = userRepository.persistCuenta(user);
		
		if(response!=null && response.getCodeResponse().equalsIgnoreCase("0000")) {
			try {
				logger.info("Enviando email EMAIL_CREATE_USER... ini");						
				String templateName="EMAIL_CREATE_USER";						
				utilityService.sendEmailLV(templateName,response.getId());
				logger.info("Enviando email EMAIL_CREATE_USER... fin");
				List<Object> resourcesResponse = new ArrayList<>();
				response.setList(resourcesResponse);	
			}catch(Exception ex) {
				response.setCodeResponse("0666");			
				response.setResponse(ex.getMessage());
				logger.error("Erro send email:"+ex.getMessage());
				return response;
			}
		
		}
		
		return UserException.setMessageResponseSave(response);

	}
	
	public ResponseTransaction update(Long id, UserUpdateDto userDto) {

		Notification notification = UserValidation.validationUpdate(userDto);
		ResponseTransaction response = new ResponseTransaction();
		
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		User user = userMapper.reverseMapperUpdate(id, userDto);	
		
		response = userRepository.update(user);	
		
		return UserException.setMessageResponseUpdate(response);

	}
		
	public ResponseTransaction updatexToken(String token, UserxTokenDto userDto) {

		Notification notification = UserValidation.validationTokenUpdate(userDto);
		ResponseTransaction response = new ResponseTransaction();
		
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		User user = userMapper.reverseMapperTokenUpdate(token, userDto);	
		
		response = userRepository.updateToken(user);	
		
		return UserException.setMessageResponseTokenUpdate(response);

	}

	public ResponseTransaction forgotPassword(RecoveryPasswordRequest recoveryPassword) {

		Notification notification = UserValidation.validation(recoveryPassword);
		ResponseTransaction response = new ResponseTransaction();

		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}

		String tokenPassword = Utility.generateRandomString(15);

		response = userRepository.recoveryPassword(tokenPassword, recoveryPassword.getEmail());
		
		if(response!=null && response.getCodeResponse().equalsIgnoreCase("0000")) {
			try {
				logger.info("Enviando email EMAIL_RECOVERY_PASSWORD... ini");						
				String templateName="EMAIL_RECOVERY_PASSWORD";						
				utilityService.sendEmailLV(templateName,response.getId());
				logger.info("Enviando email EMAIL_RECOVERY_PASSWORD... fin");
				List<Object> resourcesResponse = new ArrayList<>();
				response.setList(resourcesResponse);	
			}catch(Exception ex) {
				response.setCodeResponse("0666");			
				response.setResponse(ex.getMessage());
				logger.error("Erro send email:"+ex.getMessage());
				return response;
			}
		
		}

		return UserException.setMessageResponseRecoveryPassword(response);

	}
	
	public ResponseTransaction get(Long id) {
		
		ResponseTransaction response = new ResponseTransaction();
		
		Notification notification = UserValidation.validation(id);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			return response;
		}
		return userRepository.read(id);
	}
		
}
