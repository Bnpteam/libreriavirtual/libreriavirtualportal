package pe.gob.bnp.libreriavirtual.portal.payment.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.libreriavirtual.config.DataSource;
import pe.gob.bnp.libreriavirtual.portal.payment.dto.RequestPaymentDto;
import pe.gob.bnp.libreriavirtual.portal.payment.dto.RequestPaymentResponse;
import pe.gob.bnp.libreriavirtual.portal.payment.dto.ResponsePaymentDto;
import pe.gob.bnp.libreriavirtual.portal.payment.dto.ResponsePaymentResponse;
import pe.gob.bnp.libreriavirtual.portal.payment.repository.PaymentRepository;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

@Repository
public class PaymentRepositoryJDBC  implements PaymentRepository {

	private static final Logger logger = LoggerFactory.getLogger(PaymentRepositoryJDBC.class);

	private String messageSuccess = "Transacción exitosa.";

	@Override
	public ResponseTransaction vposRequest(RequestPaymentDto requestPaymentDto) {
	
        String dBTransaction = "{ Call PKG_API_PAYMENT.SP_VPOS_REQUEST(?,?,?,?,?,?,?,?,?,?,?,?)}";/*12*/
             
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);) {
			con.setAutoCommit(false);
			cstm.setString("P_OPERATION_NUMBER",Utility.getString(requestPaymentDto.getOperationNro()));
			cstm.setString("P_PURCHASE_AMOUNT",Utility.getString(requestPaymentDto.getPurchaseAmount()));
			cstm.setString("P_SHIPPING_FIRSTNAME", Utility.getString(requestPaymentDto.getShippingFirstName()));
			cstm.setString("P_SHIPPING_LASTNAME", Utility.getString(requestPaymentDto.getShippingLastName()));
			cstm.setString("P_SHIPPING_EMAIL", Utility.getString(requestPaymentDto.getShippingEmail()));
			cstm.setString("P_SHIPPING_ADDRESS", Utility.getString(requestPaymentDto.getShippingAddress()));
			cstm.setString("P_SHIPPING_CITY", Utility.getString(requestPaymentDto.getShippingCity()));
			cstm.setString("P_SHIPPING_STATE",Utility.getString(requestPaymentDto.getShippingState()));
			cstm.setString("P_SHIPPING_COUNTRY",Utility.getString(requestPaymentDto.getShippingCountry()));
			cstm.setString("P_DESCRIPTION_PRODUCTS",Utility.getString(requestPaymentDto.getDescriptionProducts()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				con.commit();
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					RequestPaymentResponse userResponse = new RequestPaymentResponse(); 
					userResponse.setAcquirerId(Utility.getString(rs.getString("ACQUIRER_ID")));
					userResponse.setIdCommerce(Utility.getString(rs.getString("ID_COMMERCE")));
					userResponse.setPurchaseOperationNumber(Utility.getString(rs.getString("PURCHASE_OPERATION_NUMBER")));
					userResponse.setPurchaseAmount(Utility.getString(rs.getString("PURCHASE_AMOUNT")));
					userResponse.setPurchaseCurrencyCode(Utility.getString(rs.getString("PURCHASE_CURRENCY_CODE")));
					userResponse.setLanguage(Utility.getString(rs.getString("PASARELLA_LANGUAGE")));
					userResponse.setShippingFirstName(Utility.getString(rs.getString("SHIPPING_FIRSTNAME")));
					userResponse.setShippingLastName(Utility.getString(rs.getString("SHIPPING_LASTNAME")));
					userResponse.setShippingEmail(Utility.getString(rs.getString("SHIPPING_EMAIL")));
					userResponse.setShippingAddress(Utility.getString(rs.getString("SHIPPING_ADDRESS")));
					userResponse.setShippingZIP(Utility.getString(rs.getString("SHIPPING_ZIP")));
					userResponse.setShippingCity(Utility.getString(rs.getString("SHIPPING_CITY")));
					userResponse.setShippingState(Utility.getString(rs.getString("SHIPPING_STATE")));
					userResponse.setShippingCountry(Utility.getString(rs.getString("SHIPPING_COUNTRY")));
					userResponse.setUserCommerce(Utility.getString(rs.getString("USER_COMMERCE")));
					userResponse.setUserCodePayme(Utility.getString(rs.getString("USER_CODE_PAYME")));
					userResponse.setDescriptionProducts(Utility.getString(rs.getString("DESCRIPTION_PRODUCTS")));
					userResponse.setProgrammingLanguage(Utility.getString(rs.getString("PROGRAMMING_LANGUAGE")));
					
					String purchaseVerification=Utility.getString(rs.getString("PURCHASE_VERIFICATION"));
					String cryptPassarellaPOS=Utility.getStringSHA(purchaseVerification);
					
					userResponse.setPurchaseVerification(cryptPassarellaPOS);
									
					usersResponse.add(userResponse);
				}	
				response.setList(usersResponse);			
			}else {
				con.rollback();
			}
			
			if (rs != null) {
				rs.close();
			}
			
		}catch(SQLException e) {
			logger.error(" PKG_API_PAYMENT.SP_VPOS_REQUEST: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}
	
	
	@Override
	public ResponseTransaction vposResponse(ResponsePaymentDto responsePaymentDto) {
		
        String dBTransaction = "{ Call PKG_API_PAYMENT.SP_VPOS_RESPONSE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";/*22*/
             
        List<Object> usersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
      
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
				
			con.setAutoCommit(false);
			cstm.setString("P_AUTHORIZATION_RESULT",Utility.getString(responsePaymentDto.getAuthorizationResult()));
			cstm.setString("P_AUTHORIZATION_CODE", Utility.getString(responsePaymentDto.getAuthorizationCode()));
			cstm.setString("P_ERROR_CODE", Utility.getString(responsePaymentDto.getErrorCode()));
			cstm.setString("P_ERROR_MESSAGE", Utility.getString(responsePaymentDto.getErrorMessage()));
			cstm.setString("P_BIN", Utility.getString(responsePaymentDto.getBin()));
			cstm.setString("P_BRAND", Utility.getString(responsePaymentDto.getBrand()));
			cstm.setString("P_PAYMENT_REFERENCE_CODE", Utility.getString(responsePaymentDto.getPaymentReferenceCode()));
			cstm.setString("P_ACQUIRER_ID",Utility.getString(responsePaymentDto.getAcquirerId()));
			cstm.setString("P_ID_COMMERCE",Utility.getString(responsePaymentDto.getIdCommerce()));
			cstm.setString("P_PURCHASE_AMOUNT",Utility.getString(responsePaymentDto.getPurchaseAmount()));
			cstm.setString("P_PURCHASE_CURRENCY_CODE",Utility.getString(responsePaymentDto.getPurchaseCurrencyCode()));
			cstm.setString("P_PURCHASE_OPERATION_NUMBER",Utility.getString(responsePaymentDto.getPurchaseOperationNumber()));
			
			cstm.setString("P_SHIPPING_FIRSTNAME", Utility.getString(responsePaymentDto.getShippingFirstName()));
			cstm.setString("P_SHIPPING_LASTNAME", Utility.getString(responsePaymentDto.getShippingLastName()));
			cstm.setString("P_SHIPPING_EMAIL", Utility.getString(responsePaymentDto.getShippingEmail()));
			cstm.setString("P_SHIPPING_ADDRESS", Utility.getString(responsePaymentDto.getShippingAddress()));
			cstm.setString("P_SHIPPING_ZIP", Utility.getString(responsePaymentDto.getShippingAddress()));
			cstm.setString("P_SHIPPING_CITY", Utility.getString(responsePaymentDto.getShippingCity()));
			cstm.setString("P_SHIPPING_STATE",Utility.getString(responsePaymentDto.getShippingState()));
			cstm.setString("P_SHIPPING_COUNTRY",Utility.getString(responsePaymentDto.getShippingCountry()));
			cstm.setString("P_DESCRIPTION_PRODUCTS",Utility.getString(responsePaymentDto.getDescriptionProducts()));
			cstm.setString("P_PURCHASE_VERIFICATION",Utility.getString(responsePaymentDto.getPurchaseVerification()));
			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);			
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				con.commit();
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					ResponsePaymentResponse userResponse = new ResponsePaymentResponse(); 
					
					userResponse.setAuthorizationResult(Utility.getString(rs.getString("AUTHORIZATION_RESULT")));
					userResponse.setAuthorizationCode(Utility.getString(rs.getString("AUTHORIZATION_CODE")));
					userResponse.setErrorCode(Utility.getString(rs.getString("ERROR_CODE")));
					userResponse.setErrorMessage(Utility.getString(rs.getString("ERROR_MESSAGE")));
				
					String purchaseVerificationCommerce=Utility.getString(rs.getString("PURCHASE_VERIFICATION_COM"));
					String cryptPassarellaPOSCommerce=Utility.getStringSHA(purchaseVerificationCommerce);
					logger.info("passwordEncript:"+cryptPassarellaPOSCommerce);
					usersResponse.add(userResponse);
				}	
				response.setList(usersResponse);			
			}else {
				con.rollback();
			}
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error(" PKG_API_PAYMENT.SP_VPOS_RESPONSE: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}


	@Override
	public ResponseTransaction getPasarellaResponse(String operationNumber, String applicationId) {
	
		String dBTransaction = "{ Call PKG_API_PAYMENT.SP_GET_PASARELLA_RESPONSE(?,?,?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		ResultSet rs = null;
		
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setString("P_OPERATION_NUMBER", operationNumber);	
			cstm.setString("P_APPLICATION_ID", applicationId);	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					ResponsePaymentResponse userResponse = new ResponsePaymentResponse(); 
					
					userResponse.setAuthorizationResult(Utility.getString(rs.getString("AUTHORIZATION_RESULT")));
					userResponse.setAuthorizationCode(Utility.getString(rs.getString("AUTHORIZATION_CODE")));
					userResponse.setErrorCode(Utility.getString(rs.getString("ERROR_CODE")));
					userResponse.setErrorMessage(Utility.getString(rs.getString("ERROR_MESSAGE")));
					userResponse.setBin(Utility.getString(rs.getString("BIN")));
					userResponse.setBrand(Utility.getString(rs.getString("BRAND")));
					userResponse.setPaymentReferenceCode(Utility.getString(rs.getString("PAYMENT_REFERENCE_CODE")));
					
					
					userResponse.setAcquirerId(Utility.getString(rs.getString("ACQUIRER_ID")));
					userResponse.setIdCommerce(Utility.getString(rs.getString("ID_COMMERCE")));
					userResponse.setPurchaseOperationNumber(Utility.getString(rs.getString("PURCHASE_OPERATION_NUMBER")));
					userResponse.setPurchaseAmount(Utility.getString(rs.getString("PURCHASE_AMOUNT")));
					userResponse.setPurchaseCurrencyCode(Utility.getString(rs.getString("PURCHASE_CURRENCY_CODE")));
					
					userResponse.setShippingFirstName(Utility.getString(rs.getString("SHIPPING_FIRST_NAME")));
					userResponse.setShippingLastName(Utility.getString(rs.getString("SHIPPING_LAST_NAME")));
					userResponse.setShippingEmail(Utility.getString(rs.getString("SHIPPING_EMAIL")));
					userResponse.setShippingAddress(Utility.getString(rs.getString("SHIPPING_ADDRESS")));
					userResponse.setShippingZIP(Utility.getString(rs.getString("SHIPPING_ZIP")));
					userResponse.setShippingCity(Utility.getString(rs.getString("SHIPPING_CITY")));
					userResponse.setShippingState(Utility.getString(rs.getString("SHIPPING_STATE")));
					userResponse.setShippingCountry(Utility.getString(rs.getString("SHIPPING_COUNTRY")));
					
					userResponse.setDescriptionProducts(Utility.getString(rs.getString("DESCRIPTION_PRODUCTS")));
					userResponse.setDatetime(Utility.getString(rs.getString("PAGE_DATETIME")));
					
				
					
					slidersResponse.add(userResponse);
				}
				response.setList(slidersResponse);
			}
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error(" PKG_API_PAYMENT.SP_GET_PASARELLA_RESPONSE: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		} 
		return response;
	}

}
