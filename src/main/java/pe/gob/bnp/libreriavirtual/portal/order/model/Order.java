package pe.gob.bnp.libreriavirtual.portal.order.model;

import java.util.List;

public class Order {
	
	private String clientId;
	
	private Integer countryId;
	private String countryAddress;
	
	private Double subtotalCost;
	private Double shippingCost;
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getCountryAddress() {
		return countryAddress;
	}

	public void setCountryAddress(String countryAddress) {
		this.countryAddress = countryAddress;
	}

	public Double getSubtotalCost() {
		return subtotalCost;
	}

	public void setSubtotalCost(Double subtotalCost) {
		this.subtotalCost = subtotalCost;
	}

	public Double getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(Double shippingCost) {
		this.shippingCost = shippingCost;
	}

	public Double getIgvCost() {
		return igvCost;
	}

	public void setIgvCost(Double igvCost) {
		this.igvCost = igvCost;
	}

	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	public Integer getTypeRegionId() {
		return typeRegionId;
	}

	public void setTypeRegionId(Integer typeRegionId) {
		this.typeRegionId = typeRegionId;
	}

	public Integer getTypeDeliveryId() {
		return typeDeliveryId;
	}

	public void setTypeDeliveryId(Integer typeDeliveryId) {
		this.typeDeliveryId = typeDeliveryId;
	}

	public Integer getTypeShippingId() {
		return typeShippingId;
	}

	public void setTypeShippingId(Integer typeShippingId) {
		this.typeShippingId = typeShippingId;
	}

	public Integer getTypeVoucherId() {
		return typeVoucherId;
	}

	public void setTypeVoucherId(Integer typeVoucherId) {
		this.typeVoucherId = typeVoucherId;
	}

	public Integer getAddressId() {
		return addressId;
	}

	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getFiscalAddress() {
		return fiscalAddress;
	}

	public void setFiscalAddress(String fiscalAddress) {
		this.fiscalAddress = fiscalAddress;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public List<OrderDetail> getDetailBookAndProduct() {
		return detailBookAndProduct;
	}

	public void setDetailBookAndProduct(List<OrderDetail> detailBookAndProduct) {
		this.detailBookAndProduct = detailBookAndProduct;
	}

	private Double igvCost;
	private Double totalCost;
	
	private Integer typeRegionId;
	private Integer typeDeliveryId;
	private Integer typeShippingId;
	private Integer typeVoucherId;
	private Integer addressId;		
	
	private String ruc;
	private String razonSocial;
	private String fiscalAddress;
	private String nroDocumento;
	private String fullName; 
	
	private List<OrderDetail> detailBookAndProduct;

}
