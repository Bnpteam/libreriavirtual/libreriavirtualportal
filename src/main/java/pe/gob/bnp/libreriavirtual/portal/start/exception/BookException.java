package pe.gob.bnp.libreriavirtual.portal.start.exception;

import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

public class BookException {
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";
	
	
	public static final  String error0005CourseList="El id de la categoria no existe";
	public static final  String error0006CourseList="El id del autor no existe";
	public static final  String error0007CourseList="El id del año de publicacion no existe";
	public static final  String error0008CourseList="El id del tipo de publicacion no existe";

	public static ResponseTransaction setMessageResponseBookList(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		
		if (response.getCodeResponse().equals("0005")) {
			response.setResponse(error0005CourseList);
		}
		
		if (response.getCodeResponse().equals("0006")) {
			response.setResponse(error0006CourseList);
		}
		

		if (response.getCodeResponse().equals("0007")) {
			response.setResponse(error0007CourseList);
		}
		

		if (response.getCodeResponse().equals("0008")) {
			response.setResponse(error0008CourseList);
		}
				
		
		return response;
	}
}
