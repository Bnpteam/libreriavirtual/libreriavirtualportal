package pe.gob.bnp.libreriavirtual.portal.order.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderDetailDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderForeignDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.StockDto;
import pe.gob.bnp.libreriavirtual.portal.order.model.Order;
import pe.gob.bnp.libreriavirtual.portal.order.model.OrderDetail;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

@Component
public class OrderMapper {
	
	public Order reverseMapperSave(OrderDto orderDto) {
		Order order= new Order();
	
		order.setClientId(orderDto.getClientId());
		
		order.setSubtotalCost(Utility.parseStringToDouble(orderDto.getSubtotalCost()));
		order.setShippingCost(Utility.parseStringToDouble(orderDto.getShippingCost()));
		order.setIgvCost(Utility.parseStringToDouble(orderDto.getIgvCost()));
		order.setTotalCost(Utility.parseStringToDouble(orderDto.getTotalCost()));		
		
		order.setTypeRegionId(Utility.parseStringToInt(orderDto.getTypeRegionId()));
		order.setTypeDeliveryId(Utility.parseStringToInt(orderDto.getTypeDeliveryId()));
		order.setTypeShippingId(Utility.parseStringToInt(orderDto.getTypeShippingId()));
		order.setAddressId(Utility.parseStringToInt(orderDto.getAddressId()));
		order.setTypeVoucherId(Utility.parseStringToInt(orderDto.getTypeVoucherId()));

		order.setRuc(orderDto.getRuc());
		order.setRazonSocial(orderDto.getRazonSocial());
		order.setFiscalAddress(orderDto.getFiscalAddress());
		order.setNroDocumento(orderDto.getNroDocumento());
		
		order.setFullName(orderDto.getFullName());
		
		order.setDetailBookAndProduct(reverseMapperList(orderDto.getDetailBookAndProduct()));
		
		return order;
	}
	
	public Order reverseMapperStock(StockDto orderDto) {
		Order order= new Order();	
		order.setDetailBookAndProduct(reverseMapperList(orderDto.getDetailBookAndProduct()));
		
		return order;
	}
	
	public Order reverseMapperForeign(OrderForeignDto orderDto) {
		Order order= new Order();
	
		order.setClientId(orderDto.getClientId());
		
		order.setSubtotalCost(Utility.parseStringToDouble(orderDto.getSubtotalCost()));
		order.setShippingCost(Utility.parseStringToDouble(orderDto.getShippingCost()));
		order.setIgvCost(Utility.parseStringToDouble(orderDto.getIgvCost()));
		order.setTotalCost(Utility.parseStringToDouble(orderDto.getTotalCost()));
		order.setCountryAddress(Utility.getString(orderDto.getCountryAddress()));
		order.setCountryId(Utility.parseStringToInt(orderDto.getCountryId()));
		order.setDetailBookAndProduct(reverseMapperList(orderDto.getDetailBookAndProduct()));
		
		return order;
	}
	
	
	private List<OrderDetail> reverseMapperList(List<OrderDetailDto> detailsDto){
		List<OrderDetail> details=new ArrayList<>();	
		
		for(OrderDetailDto detail:detailsDto) {
			
			OrderDetail resourceDetail= new OrderDetail();
			resourceDetail.setBookId(Utility.parseStringToLong(detail.getBookId()));
			resourceDetail.setProductId(Utility.parseStringToLong(detail.getProductId()));
			resourceDetail.setSizeId(Utility.parseStringToLong(detail.getSizeId()));
			resourceDetail.setQuantity(Utility.parseStringToLong(detail.getQuantity()));
			details.add(resourceDetail);
		}		
		
		return details;
		
	}

}
