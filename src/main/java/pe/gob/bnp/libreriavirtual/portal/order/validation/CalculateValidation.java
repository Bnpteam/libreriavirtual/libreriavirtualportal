package pe.gob.bnp.libreriavirtual.portal.order.validation;

import pe.gob.bnp.libreriavirtual.portal.order.dto.CalculateDto;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

public class CalculateValidation {
	
	private static String messageCalculate = "No se encontraron datos ";
	
	private static String messageAmountEmpty = "Se debe ingresar un monto";	
	private static String messageWeightEmpty = "Se debe ingresar un peso";	
	private static String messageQuantityEmpty = "Se debe ingresar una cantidad";	
	
	private static String messageAmountNumber = "El monto no es válido";	
	private static String messageWeightNumber= "El peso no es válido";	
	private static String messageQuantityNumber= "La cantidad no es válida";	
	private static String  messageNegativeNumber="El monto, peso y cantidad deben ser positivos";
	
	
	public static Notification validation(CalculateDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageCalculate);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getAmount())) {
			notification.addError(messageAmountEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getWeight())) {
			notification.addError(messageWeightEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getQuantity())) {
			notification.addError(messageQuantityEmpty);
			return notification;
		}
		
		if (!Utility.isDecimal(dto.getAmount()) && !Utility.isInteger(dto.getAmount())) {
			notification.addError(messageAmountNumber);
			return notification;
		}
		
		if(dto.getAmount().contains("-")||dto.getWeight().contains("-") || dto.getQuantity().contains("-") ) {
			notification.addError(messageNegativeNumber);
			return notification;
		}
		
		if (!Utility.isDecimal(dto.getWeight()) && !Utility.isInteger(dto.getWeight())) {
			notification.addError(messageWeightNumber);
			return notification;
		}
		
		if (!Utility.isInteger(dto.getQuantity())) {
			notification.addError(messageQuantityNumber);
			return notification;
		}
		
		return notification;
	}

}
