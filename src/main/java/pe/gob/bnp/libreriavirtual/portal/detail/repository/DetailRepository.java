package pe.gob.bnp.libreriavirtual.portal.detail.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

@Repository
public interface DetailRepository {

	public ResponseTransaction listRecommended(Long bookId);
	
	public ResponseTransaction listRecommendedProduct(Long productId);
	
	public ResponseTransaction readBook(Long bookId);
	
	public ResponseTransaction readProduct(Long productId);
}
