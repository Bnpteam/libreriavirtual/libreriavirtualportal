package pe.gob.bnp.libreriavirtual.portal.start.validation;

import pe.gob.bnp.libreriavirtual.portal.start.dto.BookDto;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

public class BookValidation {
	
	private static String messageCourseList = "No se encontraron datos ";
	private static String messageCourseListAuthor= "Se debe ingresar autor";
	private static String messageCourseListCategoria= "Se debe ingresar categoria";
	private static String messageCourseListPublication= "Se debe ingresar publicacion";
	private static String messageCourseListYear= "Se debe ingresar año publicacion";
	
	private static String messageCourseListPage= "Se debe ingresar el nro. de página";
	private static String messageCourseListSize= "Se debe ingresar el nro. de registros";
	
	
	public static Notification validation(BookDto bookDto) {
		Notification notification = new Notification();

		if (bookDto == null) {
			notification.addError(messageCourseList);
			return notification;
		}

		if (Utility.isEmptyOrNull(bookDto.getAuthorId())) {
			notification.addError(messageCourseListAuthor);
		}
		
		if (Utility.isEmptyOrNull(bookDto.getCategoryId())) {
			notification.addError(messageCourseListCategoria);
		}
		
		if (Utility.isEmptyOrNull(bookDto.getPublicationId())) {
			notification.addError(messageCourseListPublication);
		}
		
		if (Utility.isEmptyOrNull(bookDto.getYearId())) {
			notification.addError(messageCourseListYear);
		}
		
		if (Utility.isEmptyOrNull(bookDto.getPage())) {
			notification.addError(messageCourseListPage);
		}
		
		if (Utility.isEmptyOrNull(bookDto.getSize())) {
			notification.addError(messageCourseListSize);
		}
		
	

		return notification;
	}

}
