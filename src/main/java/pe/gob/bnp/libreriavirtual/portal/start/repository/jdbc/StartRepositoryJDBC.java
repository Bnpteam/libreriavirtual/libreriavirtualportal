package pe.gob.bnp.libreriavirtual.portal.start.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.libreriavirtual.config.DataSource;
import pe.gob.bnp.libreriavirtual.portal.start.dto.BookDto;
import pe.gob.bnp.libreriavirtual.portal.start.dto.BooksResponse;
import pe.gob.bnp.libreriavirtual.portal.start.dto.MenusResponse;
import pe.gob.bnp.libreriavirtual.portal.start.dto.NoveltiesResponse;
import pe.gob.bnp.libreriavirtual.portal.start.dto.SubMenuResponse;
import pe.gob.bnp.libreriavirtual.portal.start.repository.StartRepository;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

@Repository
public class StartRepositoryJDBC  implements StartRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(StartRepositoryJDBC.class);	
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction listNovelties() {
		
        String dBTransaction = "{ Call PKG_API_START.SP_LIST_NOVELTY(?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
		ResultSet rs = null;
		try(Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);) {		
				
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					NoveltiesResponse sliderResponse = new NoveltiesResponse();
				    sliderResponse.setId(Utility.parseLongToString(rs.getLong("BOOK_ID")));
					sliderResponse.setTitle(Utility.getString(rs.getString("BOOK_TITLE")));
					sliderResponse.setSubtitle(Utility.getString(rs.getString("BOOK_SUBTITLE")));
					sliderResponse.setAuthor(Utility.getString(rs.getString("BOOK_AUTHOR")));
					sliderResponse.setMaterial(Utility.getString(rs.getString("BOOK_MATERIAL")));
					sliderResponse.setPrice(Utility.getString(rs.getString("BOOK_PRICE")));
					sliderResponse.setQuantity(Utility.parseLongToString(rs.getLong("BOOK_QUANTITY")));
					sliderResponse.setStock(Utility.getString(rs.getString("BOOK_STOCK")));
					sliderResponse.setImage(Utility.getString(rs.getString("BOOK_IMAGE")));
					slidersResponse.add(sliderResponse);
				}	
				response.setList(slidersResponse);
			}
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_START.SP_LIST_NOVELTY: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
	
		}
		return response;	
	}
	
	@Override
	public ResponseTransaction listBooks(BookDto bookRequest) {
		
        String dBTransaction = "{ Call PKG_API_START.SP_LIST_BOOK(?,?,?,?,?,?,?,?,?)}";/*9*/
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();       
		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
				
			cstm.setString("P_TITLE", Utility.getString(bookRequest.getTitle()));	
			cstm.setLong("P_AUTHOR_ID", Utility.parseStringToLong(bookRequest.getAuthorId()));		
			cstm.setLong("P_YEAR_ID", Utility.parseStringToLong(bookRequest.getYearId()));		
			cstm.setLong("P_CATEGORY_ID", Utility.parseStringToLong(bookRequest.getCategoryId()));	
			cstm.setLong("P_PUBLICATION_ID", Utility.parseStringToLong(bookRequest.getPublicationId()));	
			
			cstm.setInt("P_PAGE", Utility.parseStringToInt(bookRequest.getPage()));	
			cstm.setInt("P_SIZE", Utility.parseStringToInt(bookRequest.getSize()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {	
					BooksResponse sliderResponse = new BooksResponse();
				    sliderResponse.setId(Utility.parseLongToString(rs.getLong("BOOK_ID")));
					sliderResponse.setTitle(Utility.getString(rs.getString("BOOK_TITLE")));
					sliderResponse.setSubtitle(Utility.getString(rs.getString("BOOK_SUBTITLE")));
					sliderResponse.setAuthorId(Utility.getString(rs.getString("BOOK_AUTHOR_ID")));
					sliderResponse.setAuthorName(Utility.getString(rs.getString("BOOK_AUTHOR_NAME")));
					sliderResponse.setPublicationId(Utility.getString(rs.getString("BOOK_PUBLICATION_ID")));
					sliderResponse.setPublicationName(Utility.getString(rs.getString("BOOK_PUBLICATION_NAME")));
					sliderResponse.setMaterialId(Utility.getString(rs.getString("BOOK_MATERIAL_ID")));
					sliderResponse.setMaterialName(Utility.getString(rs.getString("BOOK_MATERIAL_NAME")));
					sliderResponse.setCategoryId(Utility.getString(rs.getString("BOOK_CATEGORY_ID")));
					sliderResponse.setCategoryName(Utility.getString(rs.getString("BOOK_CATEGORY_NAME")));
					sliderResponse.setYearId(Utility.getString(rs.getString("BOOK_YEAR_ID")));
					sliderResponse.setYearName(Utility.getString(rs.getString("BOOK_YEAR_NAME")));
					sliderResponse.setPrice(Utility.getString(rs.getString("BOOK_PRICE")));
					sliderResponse.setQuantity(Utility.parseLongToString(rs.getLong("BOOK_QUANTITY")));
					sliderResponse.setStock(Utility.getString(rs.getString("BOOK_STOCK")));
					sliderResponse.setImage(Utility.getString(rs.getString("BOOK_IMAGE")));
					sliderResponse.setTotal(Utility.getString(rs.getString("TOTAL_RECORDS")));
					slidersResponse.add(sliderResponse);
				}	
				response.setList(slidersResponse);
			}
			
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_START.SP_LIST_BOOK: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}

	@Override
	public ResponseTransaction listMenus() {
		
        String dBTransaction = "{ Call PKG_API_START.SP_LIST_MENU(?,?)}";
        List<Object> menusResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
        
		
		ResultSet rs = null;
		try(Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {					
				
					menusResponse.add(this.readMenu(rs.getLong("MENU_ID")));
				}	
				response.setList(menusResponse);
			}
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_START.SP_LIST_MENU: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
		
		}
		return response;	
	}
	
	@Override
	public MenusResponse readMenu(Long id) {
	
        String dBTransaction = "{ Call PKG_API_START.SP_GET_MENU(?,?,?,?)}";
        MenusResponse menuResponse = new MenusResponse();      
        ResponseTransaction response = new ResponseTransaction();
        
		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
		
			cstm.setLong("P_MENU_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);		
			cstm.registerOutParameter("S_C_CURSOR_SUBMENU",OracleTypes.CURSOR);		
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				if (rs.next()) {
					menuResponse.setId(Utility.parseLongToString(rs.getLong("MENU_ID")));
					menuResponse.setName(Utility.getString(rs.getString("MENU_NAME")));
					menuResponse.setDescription(Utility.getString(rs.getString("MENU_DESCRIPTION")));					
					menuResponse.setOrder(Utility.parseLongToString(rs.getLong("MENU_ORDER")));			
					
				}	
				
				if (rs != null) {
					rs.close();
				}
				
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_SUBMENU");
				List<SubMenuResponse> buttonDto = new ArrayList<>();
				while (rs.next()) {
					SubMenuResponse entidad = new SubMenuResponse();
					entidad.setId(Utility.parseLongToString(rs.getLong("SUBMENU_ID")));								
					entidad.setName(Utility.getString(rs.getString("SUBMENU_NAME")));
					entidad.setDescription(Utility.getString(rs.getString("SUBMENU_DESCRIPTION")));	
					entidad.setOrder(Utility.parseLongToString(rs.getLong("SUBMENU_ORDER")));					
					buttonDto.add(entidad);
				}	
				menuResponse.setSubmenu(buttonDto);				
				
			}
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("List PKG_API_START.SP_GET_MENU: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		
		return menuResponse;	
	}
}

