package pe.gob.bnp.libreriavirtual.portal.order.dto;

public class CalculateDto {
		
	private String amount;
	private String quantity;
	private String weight;
	
	private String typeDeliveryId;
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getTypeDeliveryId() {
		return typeDeliveryId;
	}

	public void setTypeDeliveryId(String typeDeliveryId) {
		this.typeDeliveryId = typeDeliveryId;
	}

	public String getTypeShippingId() {
		return typeShippingId;
	}

	public void setTypeShippingId(String typeShippingId) {
		this.typeShippingId = typeShippingId;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	private String typeShippingId;
	
	private String addressId;
}
