package pe.gob.bnp.libreriavirtual.portal.start.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.libreriavirtual.portal.start.dto.BookDto;
import pe.gob.bnp.libreriavirtual.portal.start.exception.BookException;
import pe.gob.bnp.libreriavirtual.portal.start.repository.StartRepository;
import pe.gob.bnp.libreriavirtual.portal.start.validation.BookValidation;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

@Service
public class StartService {
	
	@Autowired
	StartRepository startRepository;
	
	public ResponseTransaction searchNovelties(){	
		
		return startRepository.listNovelties();
	}
	
	public ResponseTransaction searchMenus(){	
		
		return startRepository.listMenus();
	}
	
	
	public ResponseTransaction searchBooks(BookDto bookDto) {

		ResponseTransaction response = new ResponseTransaction();
		Notification notification = BookValidation.validation(bookDto);
		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			
			return response;
		}
		
		String title=formatTitle(bookDto.getTitle());
		bookDto.setTitle(title);

		response = startRepository.listBooks(bookDto);

		return BookException.setMessageResponseBookList(response);
	}
	
	private String formatTitle(String title) {

		title=title.replace("CHR(38)", "'||'&'||'");
		
		return title;
		
	}


}
