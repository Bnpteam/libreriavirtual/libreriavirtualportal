package pe.gob.bnp.libreriavirtual.portal.payment.exception;

import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

public class PaymentException {
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";
	public static final  String error0002 ="El tipo entrega no existe";
	public static final  String error0003 ="El tipo de envio no existe";
	public static final  String error0004 ="El id del pais no existe";
	
	public static final  String error0005 ="El id del departamento no existe";
	public static final  String error0006 ="El id de la provincia no existe";
	public static final  String error0007 ="El id del distrito no existe";
	public static final  String error000222 ="El nro de operation no existe";
	public static final  String error0003Update="La direccion no existe o esta inactivo.";
	
	
	public static ResponseTransaction setMessageResponsePasarella(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
	
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error000222);
		}
		
		
		return response;
	}
	
	
	public static ResponseTransaction setMessageResponseRequest(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003);
		}
		
		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(error0004);
		}
		
		if (response.getCodeResponse().equals("0005")) {
			response.setResponse(error0005);
		}
		
		if (response.getCodeResponse().equals("0006")) {
			response.setResponse(error0006);
		}
		
		if (response.getCodeResponse().equals("0007")) {
			response.setResponse(error0007);
		}

		return response;
	}
	
	
	public static ResponseTransaction setMessageResponse(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003);
		}
		
		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(error0004);
		}
		
		if (response.getCodeResponse().equals("0005")) {
			response.setResponse(error0005);
		}
		
		if (response.getCodeResponse().equals("0006")) {
			response.setResponse(error0006);
		}
		
		if (response.getCodeResponse().equals("0007")) {
			response.setResponse(error0007);
		}

		return response;
	}

}
