package pe.gob.bnp.libreriavirtual.portal.payment.validation;


import pe.gob.bnp.libreriavirtual.portal.payment.dto.RequestPaymentDto;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

public class PasarellaValidation {
	
	private static String messagePayment = "No se encontraron datos ";
	
	private static String messageDescripcion= "Se debe ingresar una descripcion";	
	private static String messageFirstName= "Se debe ingresar un nombre";	
	private static String messageLastName= "Se debe ingresar un apellido";	
	private static String messageAddress= "Se debe ingresar una dirección de envio";	
	private static String messageCity= "Se debe ingresar una ciudad";	
	private static String messageCountry= "Se debe ingresar un pais";	
	private static String messageEmail= "Se debe ingresar un email de envio";	
	private static String messageState= "Se debe ingresar un departamento";	
	private static String messageAmountEmpty = "Se debe ingresar un monto";	
	private static String messageAmountNumber = "El monto no es válido";	
	
	
	public static Notification validation(RequestPaymentDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messagePayment);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getDescriptionProducts())) {
			notification.addError(messageDescripcion);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getShippingFirstName())) {
			notification.addError(messageFirstName);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getShippingLastName())) {
			notification.addError(messageLastName);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getShippingAddress())) {
			notification.addError(messageAddress);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getShippingCity())) {
			notification.addError(messageCity);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getShippingCountry())) {
			notification.addError(messageCountry);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getShippingEmail())) {
			notification.addError(messageEmail);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getShippingState())) {
			notification.addError(messageState);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getPurchaseAmount())) {
			notification.addError(messageAmountEmpty);
			return notification;
		}
		
		if (!Utility.isDecimal(dto.getPurchaseAmount()) && !Utility.isInteger(dto.getPurchaseAmount())) {
			notification.addError(messageAmountNumber);
			return notification;
		}
		
		return notification;
	}

}
