package pe.gob.bnp.libreriavirtual.portal.auth.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.PortalCredential;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.RecoveryPasswordRequest;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserDto;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserPublicResponse;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserUpdateDto;
import pe.gob.bnp.libreriavirtual.portal.auth.dto.UserxTokenDto;
import pe.gob.bnp.libreriavirtual.portal.auth.service.AuthService;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseHandler;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api/portal/auth")
@Api(value = "/api/portal/auth")
@CrossOrigin(origins = "*")
public class AuthPortalController {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthPortalController.class);	
	
	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	AuthService authService;

	/*
	 * login
	 */
	@RequestMapping(method = RequestMethod.POST, path = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Login de usuario", response = ResponseTransaction.class, responseContainer = "Set", httpMethod = "POST")
	@ApiResponses({ @ApiResponse(code = 200, message = "Operación exitosa"),
			@ApiResponse(code = 400, message = "Solicitud contiene errores"),
			@ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"), })
	public ResponseEntity<Object> login(
			@ApiParam(value = "objeto contiene el email y password ingresado por el usuario", required = true) @RequestBody PortalCredential credential) {
		ResponseTransaction response = new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = authService.loginPortal(credential);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos login client: " + tiempo);
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("login IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, "Error Autenticacion " + ex.getMessage());
		} catch (Throwable ex) {
			logger.error("login Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}
	
	@RequestMapping(method = RequestMethod.POST,path="/signup", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "registrar usuario", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> signup(
			@ApiParam(value = "Estructura JSON de usuarios", required = true)
			@RequestBody UserDto userDto) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = authService.signupPortal(userDto);
			 
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos signup client: " + tiempo);
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("signup IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("signup Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	
	@RequestMapping(method = RequestMethod.PUT,path="/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar usuario", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud")
	})		
	public ResponseEntity<Object> update(
			@ApiParam(value = "Estructura JSON de usuario", required = true)
			@PathVariable Long id, @RequestBody UserUpdateDto userDto) throws Exception {
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = authService.update(id,userDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos update user client: " + tiempo);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/user/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/user/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
	
	

	@RequestMapping(method = RequestMethod.PUT, path = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "actualizar clave usuario", response = ResponseTransaction.class, responseContainer = "Set", httpMethod = "PUT")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> updatePassword(	
			@RequestParam(value = "token", defaultValue = "", required=true) String  token, @RequestBody UserxTokenDto userxTokenDto){
		
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = authService.updatexToken(token,userxTokenDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos put clave client: " + tiempo);
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("/user/"+token+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/user/"+token+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}

	}	
	
	@RequestMapping(method = RequestMethod.POST, path = "/forgotpassword", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Envío de email para nueva clave", response= ResponseTransaction.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"),
	})	
	public ResponseEntity<Object> forgotPassword(@RequestBody RecoveryPasswordRequest recoveryPassword){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = authService.forgotPassword(recoveryPassword);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos forgotpassword client: " + tiempo);
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (IllegalArgumentException ex) {
			logger.error("forgotpassword IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("forgotpassword Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/user/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "leer usuario", response= UserPublicResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> get(
			@ApiParam(value = "id usuario", required = true)
			@PathVariable Long id){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = authService.get(id);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos get client: " + tiempo);
			if (response == null || response.getList().size()==0) {
				logger.info("/user: id:"+id+ "No se encontró datos.");
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("/user/"+id+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("/user/"+id+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("/user/"+id+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}	

}
