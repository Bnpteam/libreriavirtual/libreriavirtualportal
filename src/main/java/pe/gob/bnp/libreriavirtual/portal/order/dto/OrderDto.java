package pe.gob.bnp.libreriavirtual.portal.order.dto;

import java.util.List;

public class OrderDto {
	
	private String clientId;
	private String subtotalCost;
	private String shippingCost;
	private String igvCost;
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getSubtotalCost() {
		return subtotalCost;
	}

	public void setSubtotalCost(String subtotalCost) {
		this.subtotalCost = subtotalCost;
	}

	public String getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(String shippingCost) {
		this.shippingCost = shippingCost;
	}

	public String getIgvCost() {
		return igvCost;
	}

	public void setIgvCost(String igvCost) {
		this.igvCost = igvCost;
	}

	public String getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}

	public String getTypeRegionId() {
		return typeRegionId;
	}

	public void setTypeRegionId(String typeRegionId) {
		this.typeRegionId = typeRegionId;
	}

	public String getTypeDeliveryId() {
		return typeDeliveryId;
	}

	public void setTypeDeliveryId(String typeDeliveryId) {
		this.typeDeliveryId = typeDeliveryId;
	}

	public String getTypeShippingId() {
		return typeShippingId;
	}

	public void setTypeShippingId(String typeShippingId) {
		this.typeShippingId = typeShippingId;
	}

	public String getTypeVoucherId() {
		return typeVoucherId;
	}

	public void setTypeVoucherId(String typeVoucherId) {
		this.typeVoucherId = typeVoucherId;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getFiscalAddress() {
		return fiscalAddress;
	}

	public void setFiscalAddress(String fiscalAddress) {
		this.fiscalAddress = fiscalAddress;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public List<OrderDetailDto> getDetailBookAndProduct() {
		return detailBookAndProduct;
	}

	public void setDetailBookAndProduct(List<OrderDetailDto> detailBookAndProduct) {
		this.detailBookAndProduct = detailBookAndProduct;
	}

	private String totalCost;
	
	private String typeRegionId;
	private String typeDeliveryId;
	private String typeShippingId;
	private String typeVoucherId;
	
	private String addressId;
	private String ruc;
	private String razonSocial;
	private String fiscalAddress;
	private String nroDocumento;
	private String fullName; 
	
	private List<OrderDetailDto> detailBookAndProduct;

}
