package pe.gob.bnp.libreriavirtual.portal.start.dto;

import java.util.List;


public class MenusResponse {
	
	private String id;
	private String name;
	private String description;
	private String order;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public List<SubMenuResponse> getSubmenu() {
		return submenu;
	}
	public void setSubmenu(List<SubMenuResponse> submenu) {
		this.submenu = submenu;
	}
	private List<SubMenuResponse> submenu;

}
