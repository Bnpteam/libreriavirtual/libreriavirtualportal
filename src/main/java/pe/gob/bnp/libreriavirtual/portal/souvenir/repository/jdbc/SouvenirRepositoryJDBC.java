package pe.gob.bnp.libreriavirtual.portal.souvenir.repository.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.libreriavirtual.config.DataSource;
import pe.gob.bnp.libreriavirtual.portal.order.repository.jdbc.OrderRepositoryJDBC;
import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.GroupResponse;
import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.NoveltiesProductResponse;
import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.ProductDto;
import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.ProductsResponse;
import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.TopicResponse;
import pe.gob.bnp.libreriavirtual.portal.souvenir.repository.SouvenirRepository;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;


@Repository
public class SouvenirRepositoryJDBC   implements SouvenirRepository{
	
	private static final Logger logger = LoggerFactory.getLogger(OrderRepositoryJDBC.class);	
	
	private String messageSuccess="Transacción exitosa.";

	@Override
	public ResponseTransaction listGroup(Long submenuId) {
		
		String dBTransaction = "{ Call PKG_API_SOUVENIR.SP_LIST_GROUP(?,?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_SUBMENU_ID", submenuId);	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					GroupResponse groupResponse = new GroupResponse();
					groupResponse.setId(Utility.parseLongToString(rs.getLong("GROUP_ID")));
					groupResponse.setName(Utility.getString(rs.getString("GROUP_NAME")));
					groupResponse.setDescription(Utility.getString(rs.getString("GROUP_DESCRIPTION")));
					groupResponse.setOrder(Utility.parseLongToString(rs.getLong("GROUP_ORDER")));	
					
					
					slidersResponse.add(groupResponse);
				}
				response.setList(slidersResponse);
			}
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("PKG_API_SOUVENIR.SP_LIST_GROUP: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		} 
		return response;
	}

	@Override
	public ResponseTransaction listNovelties(Long submenuId) {
		
        String dBTransaction = "{ Call PKG_API_SOUVENIR.SP_LIST_NOVELTYP(?,?,?)}";
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
			cstm.setLong("P_SUBMENU_ID", submenuId);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {	
					NoveltiesProductResponse sliderResponse = new NoveltiesProductResponse();
				    sliderResponse.setId(Utility.parseLongToString(rs.getLong("PRODUCT_ID")));
					sliderResponse.setTitle(Utility.getString(rs.getString("PRODUCT_TITLE")));
					sliderResponse.setSubtitle(Utility.getString(rs.getString("PRODUCT_SUBTITLE")));					
					sliderResponse.setPrice(Utility.getString(rs.getString("PRODUCT_PRICE")));
					sliderResponse.setQuantity(Utility.parseLongToString(rs.getLong("PRODUCT_QUANTITY")));
					sliderResponse.setStock(Utility.getString(rs.getString("PRODUCT_STOCK")));
					sliderResponse.setImage(Utility.getString(rs.getString("PRODUCT_IMAGE")));
					slidersResponse.add(sliderResponse);
				}	
				response.setList(slidersResponse);
			}
			if (rs != null) {
				rs.close();
			}
		}catch(SQLException e) {
			logger.error("PKG_API_SOUVENIR.SP_LIST_NOVELTYP: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		return response;	
	}

	@Override
	public ResponseTransaction listProducts(ProductDto productDto) {
		
        String dBTransaction = "{ Call PKG_API_SOUVENIR.SP_LIST_PRODUCT(?,?,?,?,?,?,?)}";/*7*/
        
        List<Object> slidersResponse = new ArrayList<>();
        ResponseTransaction response = new ResponseTransaction();
       
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){			
				
			cstm.setLong("P_SUBMENU_ID", Utility.parseStringToLong(productDto.getSubmenuId()));
			cstm.setLong("P_GROUP_ID", Utility.parseStringToLong(productDto.getGroupId()));	
			cstm.setLong("P_TOPIC_ID", Utility.parseStringToLong(productDto.getTopicId()));	
			
			cstm.setInt("P_PAGE", Utility.parseStringToInt(productDto.getPage()));	
			cstm.setInt("P_SIZE", Utility.parseStringToInt(productDto.getSize()));	
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {	
					ProductsResponse sliderResponse = new ProductsResponse();
				    sliderResponse.setId(Utility.parseLongToString(rs.getLong("PRODUCT_ID")));
					sliderResponse.setTitle(Utility.getString(rs.getString("PRODUCT_TITLE")));
					sliderResponse.setSubtitle(Utility.getString(rs.getString("PRODUCT_SUBTITLE")));
					sliderResponse.setGroupId(Utility.getString(rs.getString("PRODUCT_GROUP_ID")));
					sliderResponse.setGroupName(Utility.getString(rs.getString("PRODUCT_GROUP_NAME")));
					sliderResponse.setTopicId(Utility.getString(rs.getString("PRODUCT_TOPIC_ID")));
					sliderResponse.setTopicName(Utility.getString(rs.getString("PRODUCT_TOPIC_NAME")));					
					sliderResponse.setPrice(Utility.getString(rs.getString("PRODUCT_PRICE")));
					sliderResponse.setQuantity(Utility.parseLongToString(rs.getLong("PRODUCT_QUANTITY")));
					sliderResponse.setStock(Utility.getString(rs.getString("PRODUCT_STOCK")));
					sliderResponse.setImage(Utility.getString(rs.getString("PRODUCT_IMAGE")));
					sliderResponse.setTotal(Utility.getString(rs.getString("TOTAL_RECORDS")));
					slidersResponse.add(sliderResponse);
				}	
				response.setList(slidersResponse);
			}
			if (rs != null) {
				rs.close();
			}
			
		}catch(SQLException e) {
			logger.error("PKG_API_SOUVENIR.SP_LIST_PRODUCT: "+e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));			
			response.setResponse(e.getMessage());
			
		}
		
		return response;	
	}

	@Override
	public ResponseTransaction listTopic() {
		
		String dBTransaction = "{ Call PKG_API_SOUVENIR.SP_LIST_TOPIC(?,?)}";

		List<Object> slidersResponse = new ArrayList<>();
		ResponseTransaction response = new ResponseTransaction();

		
		ResultSet rs = null;
		try (Connection con = DataSource.getConnection();
				CallableStatement cstm = con.prepareCall(dBTransaction);){
			
				
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();

			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodeResponse(codigoResultado);
			if (Utility.getString(codigoResultado).equals("0000")) {
				response.setResponse(messageSuccess);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					TopicResponse topicResponse = new TopicResponse();
					topicResponse.setId(Utility.parseLongToString(rs.getLong("TOPIC_ID")));
					topicResponse.setName(Utility.getString(rs.getString("TOPIC_NAME")));
					topicResponse.setDescription(Utility.getString(rs.getString("TOPIC_DESCRIPTION")));
					topicResponse.setOrder(Utility.parseLongToString(rs.getLong("TOPIC_ORDER")));	
					
					
					slidersResponse.add(topicResponse);
				}
				response.setList(slidersResponse);
			}
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("PKG_API_SOUVENIR.SP_LIST_TOPIC: " + e.getMessage());
			response.setCodeResponse(String.valueOf(e.getErrorCode()));
			response.setResponse(e.getMessage());
			
		} 
		return response;
	}
	
	

}
