package pe.gob.bnp.libreriavirtual.portal.souvenir.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.GroupResponse;
import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.NoveltiesProductResponse;
import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.ProductDto;
import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.ProductsResponse;
import pe.gob.bnp.libreriavirtual.portal.souvenir.dto.TopicResponse;
import pe.gob.bnp.libreriavirtual.portal.souvenir.service.SouvenirService;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseHandler;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api/portal/souvenir")
@Api(value = "/api/portal/souvenir")
@CrossOrigin(origins = "*")
public class SouvenirController {

	private static final Logger logger = LoggerFactory.getLogger(SouvenirController.class);

	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	SouvenirService souvenirService;

	@RequestMapping(method = RequestMethod.GET, path = "/group/{submenuId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar grupo productos x submenu", response = GroupResponse.class, responseContainer = "Set", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> listGroup(
			@ApiParam(value = "id submenu", required = true) @PathVariable Long submenuId) {
		ResponseTransaction response = new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = souvenirService.listGroup(submenuId);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos grupos productos x submenu: " + tiempo);
			if (response != null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("group EntityNotFoundResultException: " + ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("group IllegalArgumentException: " + ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("group Throwable: " + ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}

	@RequestMapping(method = RequestMethod.GET, path = "/topic", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar temas", response = TopicResponse.class, responseContainer = "Set", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> listTopic() {
		ResponseTransaction response = new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;			
			timeIni = System.currentTimeMillis(); 
			
			response = souvenirService.listTopic();
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos temas: " + tiempo);
			
			if (response != null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("topic EntityNotFoundResultException: " + ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("topic IllegalArgumentException: " + ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("topic Throwable: " + ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}

	@RequestMapping(method = RequestMethod.GET, path = "/novelties/{submenuId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar últimos 4 productos x submenu", response = NoveltiesProductResponse.class, responseContainer = "Set", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> listNovelties(
			@ApiParam(value = "id submenu", required = true) @PathVariable Long submenuId) {
		ResponseTransaction response = new ResponseTransaction();
		try {
			
			long timeIni, timeFin, tiempo;			
			timeIni = System.currentTimeMillis(); 
			
			response = souvenirService.listNovelties(submenuId);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos novedad producto: " + tiempo);
			if (response != null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("novelties/submenuId EntityNotFoundResultException: " + ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("novelties/submenuId IllegalArgumentException: " + ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("novelties/submenuId Throwable: " + ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}

	@RequestMapping(method = RequestMethod.POST, path = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar productos", response = ProductsResponse.class, responseContainer = "Set", httpMethod = "POST")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> listProducts(
			@ApiParam(value = "Estructura JSON de criterios para listar souvenirs", required = true) @RequestBody ProductDto productDto) {
		ResponseTransaction response = new ResponseTransaction();
		try {

			long timeIni, timeFin, tiempo;			
			timeIni = System.currentTimeMillis(); 
			
			response = souvenirService.listProducts(productDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos listarproductos: " + tiempo);
			if (response != null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("products EntityNotFoundResultException: " + ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("products IllegalArgumentException: " + ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("products Throwable: " + ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}

}
