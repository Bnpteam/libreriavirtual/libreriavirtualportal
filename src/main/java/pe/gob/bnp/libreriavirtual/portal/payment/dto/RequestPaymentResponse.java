package pe.gob.bnp.libreriavirtual.portal.payment.dto;


public class RequestPaymentResponse {
	
	private String acquirerId;
	private String idCommerce;
	private String purchaseOperationNumber;
	private String purchaseAmount;
	private String purchaseCurrencyCode;
	private String language;
	private String shippingFirstName;
	private String shippingLastName;
	private String shippingEmail;
	private String shippingAddress;
	private String shippingZIP;
	private String shippingCity;
	public String getAcquirerId() {
		return acquirerId;
	}
	public void setAcquirerId(String acquirerId) {
		this.acquirerId = acquirerId;
	}
	public String getIdCommerce() {
		return idCommerce;
	}
	public void setIdCommerce(String idCommerce) {
		this.idCommerce = idCommerce;
	}
	public String getPurchaseOperationNumber() {
		return purchaseOperationNumber;
	}
	public void setPurchaseOperationNumber(String purchaseOperationNumber) {
		this.purchaseOperationNumber = purchaseOperationNumber;
	}
	public String getPurchaseAmount() {
		return purchaseAmount;
	}
	public void setPurchaseAmount(String purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}
	public String getPurchaseCurrencyCode() {
		return purchaseCurrencyCode;
	}
	public void setPurchaseCurrencyCode(String purchaseCurrencyCode) {
		this.purchaseCurrencyCode = purchaseCurrencyCode;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getShippingFirstName() {
		return shippingFirstName;
	}
	public void setShippingFirstName(String shippingFirstName) {
		this.shippingFirstName = shippingFirstName;
	}
	public String getShippingLastName() {
		return shippingLastName;
	}
	public void setShippingLastName(String shippingLastName) {
		this.shippingLastName = shippingLastName;
	}
	public String getShippingEmail() {
		return shippingEmail;
	}
	public void setShippingEmail(String shippingEmail) {
		this.shippingEmail = shippingEmail;
	}
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public String getShippingZIP() {
		return shippingZIP;
	}
	public void setShippingZIP(String shippingZIP) {
		this.shippingZIP = shippingZIP;
	}
	public String getShippingCity() {
		return shippingCity;
	}
	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}
	public String getShippingState() {
		return shippingState;
	}
	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}
	public String getShippingCountry() {
		return shippingCountry;
	}
	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}
	public String getUserCommerce() {
		return userCommerce;
	}
	public void setUserCommerce(String userCommerce) {
		this.userCommerce = userCommerce;
	}
	public String getUserCodePayme() {
		return userCodePayme;
	}
	public void setUserCodePayme(String userCodePayme) {
		this.userCodePayme = userCodePayme;
	}
	public String getDescriptionProducts() {
		return descriptionProducts;
	}
	public void setDescriptionProducts(String descriptionProducts) {
		this.descriptionProducts = descriptionProducts;
	}
	public String getProgrammingLanguage() {
		return programmingLanguage;
	}
	public void setProgrammingLanguage(String programmingLanguage) {
		this.programmingLanguage = programmingLanguage;
	}
	public String getPurchaseVerification() {
		return purchaseVerification;
	}
	public void setPurchaseVerification(String purchaseVerification) {
		this.purchaseVerification = purchaseVerification;
	}
	private String shippingState;
	private String shippingCountry;
	private String userCommerce;
	private String userCodePayme;
	private String descriptionProducts;
	private String programmingLanguage;
	private String purchaseVerification;

}
