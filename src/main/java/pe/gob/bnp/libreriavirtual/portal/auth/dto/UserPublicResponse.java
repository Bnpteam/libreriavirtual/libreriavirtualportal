package pe.gob.bnp.libreriavirtual.portal.auth.dto;


public class UserPublicResponse {
	
	private String id;
	private String name;
	private String surname;	
	private String email;
	private String birthDate;
	private String typeDocumentId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getTypeDocumentId() {
		return typeDocumentId;
	}
	public void setTypeDocumentId(String typeDocumentId) {
		this.typeDocumentId = typeDocumentId;
	}
	public String getTypeDocumentName() {
		return typeDocumentName;
	}
	public void setTypeDocumentName(String typeDocumentName) {
		this.typeDocumentName = typeDocumentName;
	}
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getLastLoginDate() {
		return lastLoginDate;
	}
	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	private String typeDocumentName;
	private String document;
	private String photo;	
	private String provider;
	private String enabled;	
	private String createdDate;
	private String lastLoginDate;
	
	
}
