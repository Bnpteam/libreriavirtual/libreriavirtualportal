package pe.gob.bnp.libreriavirtual.portal.order.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.libreriavirtual.portal.order.dto.CalculateDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.CalculateResponse;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderCabResponse;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderDetailResponse;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderForeignDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderForeignResponse;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderResponse;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrdersResponse;
import pe.gob.bnp.libreriavirtual.portal.order.dto.StockDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.StockResponse;
import pe.gob.bnp.libreriavirtual.portal.order.service.OrderService;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseHandler;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.exception.EntityNotFoundResultException;

@RestController
@RequestMapping("/api/portal/order")
@Api(value = "/api/portal/order")
@CrossOrigin(origins = "*")
public class OrderController {
	
	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);	

	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	OrderService orderService;
	
	
	@RequestMapping(method = RequestMethod.POST, path="/calculate", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "calcular costo total ", response= CalculateResponse.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> calculate(
			@ApiParam(value = "Estructura JSON de criterios para calcular", required = true)
			@RequestBody CalculateDto calculateDto){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = orderService.calculate(calculateDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos calculate: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("calculate EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("calculate IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("calculate Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.POST, path="/stock", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "validar stock disponible ", response= StockResponse.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> validateStock(
			@ApiParam(value = "Estructura JSON de criterios para calcular", required = true)
			@RequestBody StockDto stockDto){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = orderService.validateStock(stockDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos stock: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("stock EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("stock IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("stock Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	

	@RequestMapping(method = RequestMethod.POST, path="/order", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "guardar pedido", response= OrderResponse.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> save(
			@ApiParam(value = "Estructura JSON de criterios para pedido", required = true)
			@RequestBody OrderDto orderDto){
		ResponseTransaction response=new ResponseTransaction();
		try {
			
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = orderService.save(orderDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos post order: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("order EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("order IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("order Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.POST, path="/orderforeign", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "guardar pedido extranjero", response= OrderForeignResponse.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> saveForeign(
			@ApiParam(value = "Estructura JSON de criterios para pedido extranjero", required = true)
			@RequestBody OrderForeignDto orderDto){
		ResponseTransaction response=new ResponseTransaction();
		try {
			
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = orderService.saveForeign(orderDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos orderforeign: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("order EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("order IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("order Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}


	@RequestMapping(method = RequestMethod.GET, path="/orders/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "pedidos x usuario ", response= OrdersResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> listOrder(
			@ApiParam(value = "id usuario", required = true)
			@PathVariable Long userId){
		ResponseTransaction response=new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = orderService.listOrders(userId);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos getorder: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("order/"+userId+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("order/"+userId+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("order/"+userId+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	
	@RequestMapping(method = RequestMethod.GET, path="/orderCab/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "cabecera pedido ", response= OrderCabResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> getOrderCab(
			@ApiParam(value = "id order", required = true)
			@PathVariable Long orderId){
		ResponseTransaction response=new ResponseTransaction();
		try {
			
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = orderService.getOrderCab(orderId);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos getorder: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("orderCab/"+orderId+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("orderCab/"+orderId+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("orderCab/"+orderId+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/orderDet/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "detalle pedido", response= OrderDetailResponse.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> getOrder(
			@ApiParam(value = "id pedido", required = true)
			@PathVariable Long orderId){
		ResponseTransaction response=new ResponseTransaction();
		try {
			
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = orderService.getOrderDetail(orderId);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos getorderdetail: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size()==0) {
				return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("orderDet/"+orderId+" EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response,"No se encontró datos."+ex);
		} catch (IllegalArgumentException ex) {
			logger.error("orderDet/"+orderId+" IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response,ex.getMessage());
		} catch (Throwable ex) {
			logger.error("orderDet/"+orderId+" Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response,ex);
		}		
		
	}

}
