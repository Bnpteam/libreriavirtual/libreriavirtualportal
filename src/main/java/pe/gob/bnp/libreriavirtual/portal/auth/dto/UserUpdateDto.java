package pe.gob.bnp.libreriavirtual.portal.auth.dto;


import pe.gob.bnp.libreriavirtual.portal.auth.model.User;
import pe.gob.bnp.libreriavirtual.utilitary.common.Constants;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

public class UserUpdateDto {
	
	private String id;
	private String name;
	private String surname;
	private String email;
	private String passwordOld;
	private String passwordNew;
	private String birthDate;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswordOld() {
		return passwordOld;
	}

	public void setPasswordOld(String passwordOld) {
		this.passwordOld = passwordOld;
	}

	public String getPasswordNew() {
		return passwordNew;
	}

	public void setPasswordNew(String passwordNew) {
		this.passwordNew = passwordNew;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getTypeDocument() {
		return typeDocument;
	}

	public void setTypeDocument(String typeDocument) {
		this.typeDocument = typeDocument;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	private String typeDocument;
	private String document;
	private String photo;
	private String provider;

	
	public UserUpdateDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.name = Constants.EMPTY_STRING;
		this.surname = Constants.EMPTY_STRING;
		this.email= Constants.EMPTY_STRING;
		this.passwordOld= Constants.EMPTY_STRING;
		this.passwordNew= Constants.EMPTY_STRING;
		this.birthDate=Constants.EMPTY_STRING;
		this.typeDocument=Constants.EMPTY_STRING;
		this.document=Constants.EMPTY_STRING;	
		this.photo=Constants.EMPTY_STRING;
		this.provider=Constants.EMPTY_STRING;
		
	}	

	public UserUpdateDto(User user) {
		super();
		this.id =Utility.parseLongToString(user.getId());
		this.name =Utility.getString(user.getUserName()); 
		this.surname = Utility.getString(user.getUserSurname());
		this.email= Utility.getString(user.getUserEmail());
		this.passwordOld= Utility.getString(user.getUserPassword());
		this.passwordNew= Utility.getString(user.getUserPasswordNew());
		this.birthDate=Utility.parseDateToString(user.getBirthDate());		
		this.typeDocument =Utility.parseLongToString(user.getTypeDocument()); 
		this.document =Utility.getString(user.getDocument()); 
		this.photo=Utility.getString(user.getPhoto()); 
		this.provider=Utility.getString(user.getProvider()); 
		
	}


}
