package pe.gob.bnp.libreriavirtual.portal.auth.exception;

import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

public class UserException {
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";	
	public static final  String error0002Login ="El email ingresado no existe.";
	public static final  String error0003Login ="El usuario esta inactivo.";
	public static final  String error0004Login ="El password ingresado es incorrecto.";
	
	public static final  String error0003Update ="El usuario no existe o esta inactivo.";	
	
	public static final String  error0005Update ="La contraseña actual no es la correcta";

	public static final  String error0001 ="El email del Usuario ya se encuentra registrado.";
	public static final  String error0002Token ="El token ingresado no existe.";
	public static final  String error0003Token ="El token ingresado ha expirado.";
	public static final  String errorUserUpdate0003="El usuario no se encontró o esta inactivo.";

	public static ResponseTransaction setMessageResponseLoginPortal(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002Login);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003Login);
		}
		
		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(error0004Login);
		}

		return response;
	} 	
	
	public static ResponseTransaction setMessageResponseSave(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001);
		}

		return response;
	}	
	
	public static ResponseTransaction setMessageResponseUpdate(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003Update);
		}
		
		if (response.getCodeResponse().equals("0005")) {
			response.setResponse(error0005Update);
		}

		return response;
	}	
	
	public static ResponseTransaction setMessageResponseTokenUpdate(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002Token);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003Token);
		}	

		return response;
	}
	
	public static ResponseTransaction setMessageResponseRecoveryPassword(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002Login);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003Login);
		}

		return response;
	}
	
	public static ResponseTransaction setMessageResponseUser(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}		

		return response;
	}	

}
