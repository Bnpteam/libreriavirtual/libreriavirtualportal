package pe.gob.bnp.libreriavirtual.portal.payment.dto;


public class ResponsePaymentResponse {	
	
	private String authorizationResult;
	private String authorizationCode;
	private String errorCode;
	private String errorMessage;
	private String bin;
	private String brand;
	private String paymentReferenceCode;
	private String acquirerId;
	private String idCommerce;
	
	private String purchaseOperationNumber;
	private String purchaseAmount;
	private String purchaseCurrencyCode;
	private String shippingFirstName;
	private String shippingLastName;
	private String shippingEmail;
	private String shippingAddress;
	private String shippingZIP;
	public String getAuthorizationResult() {
		return authorizationResult;
	}
	public void setAuthorizationResult(String authorizationResult) {
		this.authorizationResult = authorizationResult;
	}
	public String getAuthorizationCode() {
		return authorizationCode;
	}
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getBin() {
		return bin;
	}
	public void setBin(String bin) {
		this.bin = bin;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getPaymentReferenceCode() {
		return paymentReferenceCode;
	}
	public void setPaymentReferenceCode(String paymentReferenceCode) {
		this.paymentReferenceCode = paymentReferenceCode;
	}
	public String getAcquirerId() {
		return acquirerId;
	}
	public void setAcquirerId(String acquirerId) {
		this.acquirerId = acquirerId;
	}
	public String getIdCommerce() {
		return idCommerce;
	}
	public void setIdCommerce(String idCommerce) {
		this.idCommerce = idCommerce;
	}
	public String getPurchaseOperationNumber() {
		return purchaseOperationNumber;
	}
	public void setPurchaseOperationNumber(String purchaseOperationNumber) {
		this.purchaseOperationNumber = purchaseOperationNumber;
	}
	public String getPurchaseAmount() {
		return purchaseAmount;
	}
	public void setPurchaseAmount(String purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}
	public String getPurchaseCurrencyCode() {
		return purchaseCurrencyCode;
	}
	public void setPurchaseCurrencyCode(String purchaseCurrencyCode) {
		this.purchaseCurrencyCode = purchaseCurrencyCode;
	}
	public String getShippingFirstName() {
		return shippingFirstName;
	}
	public void setShippingFirstName(String shippingFirstName) {
		this.shippingFirstName = shippingFirstName;
	}
	public String getShippingLastName() {
		return shippingLastName;
	}
	public void setShippingLastName(String shippingLastName) {
		this.shippingLastName = shippingLastName;
	}
	public String getShippingEmail() {
		return shippingEmail;
	}
	public void setShippingEmail(String shippingEmail) {
		this.shippingEmail = shippingEmail;
	}
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public String getShippingZIP() {
		return shippingZIP;
	}
	public void setShippingZIP(String shippingZIP) {
		this.shippingZIP = shippingZIP;
	}
	public String getShippingCity() {
		return shippingCity;
	}
	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}
	public String getShippingState() {
		return shippingState;
	}
	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}
	public String getShippingCountry() {
		return shippingCountry;
	}
	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}
	public String getDescriptionProducts() {
		return descriptionProducts;
	}
	public void setDescriptionProducts(String descriptionProducts) {
		this.descriptionProducts = descriptionProducts;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	private String shippingCity;
	private String shippingState;
	private String shippingCountry;
	private String descriptionProducts;
	private String datetime;
	//private String purchaseVerification;
	//private String purchaseVerificationCommerce;

}
