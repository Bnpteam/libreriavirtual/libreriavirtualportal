package pe.gob.bnp.libreriavirtual.portal.order.validation;

import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderDetailDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderForeignDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.StockDto;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;

public class OrderValidation {
	
	private static String messageOrder = "No se encontraron datos ";
	
	private static String messageClientEmpty = "Se debe ingresar cliente";	
	private static String messageSubtotalCostEmpty = "Se debe ingresar subtotal";	
	private static String messageShippingCostEmpty = "Se debe ingresar costo envio";	
	private static String messageIgvCostEmpty = "Se debe ingresar costo igv";		
	private static String messageTotalCostEmpty = "Se debe ingresar costo total";	
	
	private static String messageTypeRegionEmpty = "Se debe ingresar tipo region";	
	private static String messageTypeDeliveryEmpty = "Se debe ingresar tipo entrega";	
	private static String messageTypeVoucherEmpty = "Se debe ingresar tipo comprobante";	
	
	
	private static String messageSubtotalCostNumber = "El subtotal no es válido";	
	private static String messageShippingCostNumber= "El costo envio no es válido";	
	private static String messageIgvCostNumber= "La costo igv no es válido";	
	private static String messageTotalCostNumber= "La costo total no es válido";	
	private static String messageBooks="No hay libros seleccionados";
	
	
	private static String messageQuantityInteger="La cantidad ingresada no es un numero válido";
	private static String messageQuantityNull="La cantidad ingresada esta vacia";
	private static String messageQuantityNegative="La cantidad ingresada no puede ser negativa";
	
	
	
	public static Notification validation(OrderDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageOrder);
			return notification;
		}
		

		if (Utility.isEmptyOrNull(dto.getClientId())) {
			notification.addError(messageClientEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getSubtotalCost())) {
			notification.addError(messageSubtotalCostEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getShippingCost())) {
			notification.addError(messageShippingCostEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getIgvCost())) {
			notification.addError(messageIgvCostEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getTotalCost())) {
			notification.addError(messageTotalCostEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getTypeRegionId())) {
			notification.addError(messageTypeRegionEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getTypeDeliveryId())) {
			notification.addError(messageTypeDeliveryEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getTypeVoucherId())) {
			notification.addError(messageTypeVoucherEmpty);
			return notification;
		}
		
		/**
		 * validacion tipo money
		 */
		
		if (!Utility.isDecimal(dto.getSubtotalCost()) && !Utility.isInteger(dto.getSubtotalCost())) {
			notification.addError(messageSubtotalCostNumber);
			return notification;
		}
		
		if (!Utility.isDecimal(dto.getShippingCost()) && !Utility.isInteger(dto.getShippingCost())) {
			notification.addError(messageShippingCostNumber);
			return notification;
		}
		
		if (!Utility.isDecimal(dto.getIgvCost()) && !Utility.isInteger(dto.getIgvCost())) {
			notification.addError(messageIgvCostNumber);
			return notification;
		}
		
		if (!Utility.isDecimal(dto.getTotalCost()) && !Utility.isInteger(dto.getTotalCost())) {
			notification.addError(messageTotalCostNumber);
			return notification;
		}
		
		return notification;
	}
	
	public static Notification validation(StockDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageOrder);
			return notification;
		}
		
		
		if(dto.getDetailBookAndProduct().size()<=0) {
		
			notification.addError(messageBooks);
			return notification;
		}
		
	
		return notification;
	}
	
	
	public static Notification validation(OrderDetailDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageOrder);
			return notification;
		}
		
		
		if (Utility.isEmptyOrNull(dto.getQuantity().toString())) {
			notification.addError(messageQuantityNull);
			return notification;
		}
		
		if ( dto.getQuantity().toString().contains("-")) {
			notification.addError(messageQuantityNegative);
			return notification;
		}
		
		
		if ( !Utility.isInteger(dto.getQuantity().toString())) {
			notification.addError(messageQuantityInteger);
			return notification;
		}
		
	
		return notification;
	}
	
	
	public static Notification validation(OrderForeignDto dto) {

		Notification notification = new Notification();

		if (dto == null) {
			notification.addError(messageOrder);
			return notification;
		}
		

		if (Utility.isEmptyOrNull(dto.getClientId())) {
			notification.addError(messageClientEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getCountryId())) {
			notification.addError(messageClientEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getSubtotalCost())) {
			notification.addError(messageSubtotalCostEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getShippingCost())) {
			notification.addError(messageShippingCostEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getIgvCost())) {
			notification.addError(messageIgvCostEmpty);
			return notification;
		}
		
		if (Utility.isEmptyOrNull(dto.getTotalCost())) {
			notification.addError(messageTotalCostEmpty);
			return notification;
		}		
		
		
		/**
		 * validacion tipo money
		 */
		
		if (!Utility.isDecimal(dto.getSubtotalCost()) && !Utility.isInteger(dto.getSubtotalCost())) {
			notification.addError(messageSubtotalCostNumber);
			return notification;
		}
		
		if (!Utility.isDecimal(dto.getShippingCost()) && !Utility.isInteger(dto.getShippingCost())) {
			notification.addError(messageShippingCostNumber);
			return notification;
		}
		
		if (!Utility.isDecimal(dto.getIgvCost()) && !Utility.isInteger(dto.getIgvCost())) {
			notification.addError(messageIgvCostNumber);
			return notification;
		}
		
		if (!Utility.isDecimal(dto.getTotalCost()) && !Utility.isInteger(dto.getTotalCost())) {
			notification.addError(messageTotalCostNumber);
			return notification;
		}
		
		return notification;
	}

}
