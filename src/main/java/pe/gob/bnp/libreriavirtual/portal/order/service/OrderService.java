package pe.gob.bnp.libreriavirtual.portal.order.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.bnp.libreriavirtual.portal.order.dto.CalculateDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderDetailDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.OrderForeignDto;
import pe.gob.bnp.libreriavirtual.portal.order.dto.StockDto;
import pe.gob.bnp.libreriavirtual.portal.order.exception.CalculateException;
import pe.gob.bnp.libreriavirtual.portal.order.exception.OrderException;
import pe.gob.bnp.libreriavirtual.portal.order.mapper.OrderMapper;
import pe.gob.bnp.libreriavirtual.portal.order.model.Order;
import pe.gob.bnp.libreriavirtual.portal.order.model.OrderDetail;
import pe.gob.bnp.libreriavirtual.portal.order.repository.OrderRepository;
import pe.gob.bnp.libreriavirtual.portal.order.validation.CalculateValidation;
import pe.gob.bnp.libreriavirtual.portal.order.validation.OrderValidation;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.common.Utility;
import pe.gob.bnp.libreriavirtual.utilitary.service.UtilitaryService;

@Service
public class OrderService {
	
	private static final Logger logger = LoggerFactory.getLogger(OrderService.class);	
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	UtilitaryService utilityService;
	
	@Autowired
	OrderMapper orderMapper;
	
	
	public ResponseTransaction listOrders(Long userId) throws IOException {

		ResponseTransaction response = orderRepository.listOrder(userId);
		
		return OrderException.setMessageResponseListOrder(response);
	}
	
	public ResponseTransaction getOrderCab(Long orderId) throws IOException {

		ResponseTransaction response = orderRepository.getOrderCab(orderId);
		
		return OrderException.setMessageResponseGetOrder(response);
	}
	
	
	public ResponseTransaction getOrderDetail(Long orderId) throws IOException {

		ResponseTransaction response = orderRepository.getOrderDetail(orderId);
		
		return OrderException.setMessageResponseGetOrder(response);
	}
	
	
	
	public ResponseTransaction calculate(CalculateDto calculateDto) {

		Notification notification =CalculateValidation.validation(calculateDto);
		ResponseTransaction response = new ResponseTransaction();

		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			logger.error("Error 0077:"+notification.errorMessage());
			return response;
		}
		
		 BigDecimal bigAmount = new BigDecimal(calculateDto.getAmount());
		 bigAmount = bigAmount.setScale(2, RoundingMode.HALF_UP);
		 
		 BigDecimal bigWeight = new BigDecimal(calculateDto.getWeight());
		 bigWeight = bigWeight.setScale(2, RoundingMode.HALF_UP);
		 
		 calculateDto.setAmount(bigAmount+"");
		 calculateDto.setWeight(bigWeight+"");
		 //System.out.println("peso:"+bigWeight);
		 
		response = orderRepository.calculate(calculateDto);    
		
		return CalculateException.setMessageResponseCalculate(response);

	}
	
	
	public ResponseTransaction validateStock(StockDto stockDto) {

		Notification notification =OrderValidation.validation(stockDto);
		ResponseTransaction response = new ResponseTransaction();

		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			logger.error("Error 0077:"+notification.errorMessage());
			return response;
		}	
		
		
		for (OrderDetailDto  detail: stockDto.getDetailBookAndProduct()) {
			
			Notification notification2 =OrderValidation.validation(detail);
			ResponseTransaction response2 = new ResponseTransaction();

			if (notification2.hasErrors()) {
				response2.setCodeResponse("0077");
				response2.setResponse(notification2.errorMessage());
				logger.error("Error 0077:"+notification2.errorMessage());
				return response2;
			}
			
		}	
		
		 Order order= orderMapper.reverseMapperStock(stockDto);
		 for (OrderDetail  detail: order.getDetailBookAndProduct()) {			
			
				response=orderRepository.validateStock(detail);	
				
				if(response!= null && response.getCodeResponse()!=null && !response.getCodeResponse().equals("0000")) {
					return OrderException.setMessageResponseValidateStock(response);
				}
			}	
		
		
		return OrderException.setMessageResponseValidateStock(response);

	}
	
	
	public ResponseTransaction save(OrderDto orderDto) {

		Notification notification =OrderValidation.validation(orderDto);
		ResponseTransaction response = new ResponseTransaction();

		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			logger.error("Error 0077:"+notification.errorMessage());
			return response;
		}
		
		 BigDecimal bigSubtotal = new BigDecimal(orderDto.getSubtotalCost());
		 bigSubtotal = bigSubtotal.setScale(2, RoundingMode.HALF_UP);
		 
		 BigDecimal bigShippingCost = new BigDecimal(orderDto.getShippingCost());
		 bigShippingCost = bigShippingCost.setScale(2, RoundingMode.HALF_UP);
		 
		 BigDecimal bigIgvCost = new BigDecimal(orderDto.getIgvCost());
		 bigIgvCost = bigIgvCost.setScale(2, RoundingMode.HALF_UP);
		 
		 BigDecimal bigTotalCost = new BigDecimal(orderDto.getTotalCost());
		 bigTotalCost = bigTotalCost.setScale(2, RoundingMode.HALF_UP);
		 
		 orderDto.setSubtotalCost(bigSubtotal+"");
		 orderDto.setShippingCost(bigShippingCost+"");
		 orderDto.setIgvCost(bigIgvCost+"");
		 orderDto.setTotalCost(bigTotalCost+"");
		 
		 Order order= orderMapper.reverseMapperSave(orderDto);
		response = orderRepository.persist(order);    
		
		
		
		
		
		if (response!= null && response.getCodeResponse()!=null && response.getCodeResponse().equals("0000")){
			String idOrder=response.getId();
			for (OrderDetail  detail: order.getDetailBookAndProduct()) {
				orderRepository.persistDetail(Utility.parseStringToLong(idOrder),detail);			  
			}			
		}	
		
		return OrderException.setMessageResponseSaveOrder(response);

	}
	
	public ResponseTransaction saveForeign(OrderForeignDto orderDto) {

		Notification notification =OrderValidation.validation(orderDto);
		ResponseTransaction response = new ResponseTransaction();

		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			logger.error("Error 0077:"+notification.errorMessage());
			return response;
		}
		
		 BigDecimal bigSubtotal = new BigDecimal(orderDto.getSubtotalCost());
		 bigSubtotal = bigSubtotal.setScale(2, RoundingMode.HALF_UP);
		 
		 BigDecimal bigShippingCost = new BigDecimal(orderDto.getShippingCost());
		 bigShippingCost = bigShippingCost.setScale(2, RoundingMode.HALF_UP);
		 
		 BigDecimal bigIgvCost = new BigDecimal(orderDto.getIgvCost());
		 bigIgvCost = bigIgvCost.setScale(2, RoundingMode.HALF_UP);
		 
		 BigDecimal bigTotalCost = new BigDecimal(orderDto.getTotalCost());
		 bigTotalCost = bigTotalCost.setScale(2, RoundingMode.HALF_UP);
		 
		 orderDto.setSubtotalCost(bigSubtotal+"");
		 orderDto.setShippingCost(bigShippingCost+"");
		 orderDto.setIgvCost(bigIgvCost+"");
		 orderDto.setTotalCost(bigTotalCost+"");
		 
		 Order order= orderMapper.reverseMapperForeign(orderDto);
		response = orderRepository.persistForeign(order);    
		
				
		if (response!= null && response.getCodeResponse()!=null && response.getCodeResponse().equals("0000")){
			String idOrder=response.getId();
			
			for (OrderDetail  detail: order.getDetailBookAndProduct()) {
				orderRepository.persistDetail(Utility.parseStringToLong(idOrder),detail);			  
			}		
		
			logger.info("Enviando email EMAIL_PAYMENT_FOREIGN... ini");						
			String templateName="EMAIL_PAYMENT_FOREIGN";						
			utilityService.sendEmailLV(templateName,idOrder);
			logger.info("Enviando email EMAIL_PAYMENT_FOREIGN... fin");
		}
		
	
		
		return OrderException.setMessageResponseSaveOrder(response);

	}
}
