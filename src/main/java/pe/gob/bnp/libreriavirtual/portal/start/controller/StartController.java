package pe.gob.bnp.libreriavirtual.portal.start.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.libreriavirtual.portal.start.dto.BookDto;
import pe.gob.bnp.libreriavirtual.portal.start.dto.BooksResponse;
import pe.gob.bnp.libreriavirtual.portal.start.dto.MenusResponse;
import pe.gob.bnp.libreriavirtual.portal.start.dto.NoveltiesResponse;
import pe.gob.bnp.libreriavirtual.portal.start.service.StartService;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseHandler;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.exception.EntityNotFoundResultException;


@RestController
@RequestMapping("/api/portal/start")
@Api(value = "/api/portal/start")
@CrossOrigin(origins = "*")
public class StartController {
	
	private static final Logger logger = LoggerFactory.getLogger(StartController.class);	

	@Autowired
	ResponseHandler responseHandler;

	@Autowired
	StartService startService;
	
	
	@RequestMapping(method = RequestMethod.GET, path = "/menus", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar menu", response =MenusResponse.class, responseContainer = "Set", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> searchMenuAll() {
		ResponseTransaction response = new ResponseTransaction();
		try {
			long timeIni, timeFin, tiempo;			
			timeIni = System.currentTimeMillis(); 
			
			response = startService.searchMenus();
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos menus: " + tiempo);
			  
			  
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("menus EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("menus IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("menus Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}

	@RequestMapping(method = RequestMethod.GET, path = "/novelties", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar últimos 4 ejemplares", response =NoveltiesResponse.class, responseContainer = "Set", httpMethod = "GET")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> searchAll() {
		ResponseTransaction response = new ResponseTransaction();
		try {
			
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis(); 
			
			response = startService.searchNovelties();
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos novedades: " + tiempo);
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("novelties EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("novelties IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("novelties Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}
	
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/books", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "listar ejemplares", response = BooksResponse.class, responseContainer = "Set", httpMethod = "POST")
	@ApiResponses({ @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
			@ApiResponse(code = 404, message = "Solicitud contiene errores."),
			@ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"), })
	public ResponseEntity<Object> searchBookAll(	
			@ApiParam(value = "Estructura JSON de criterios para listar libros", required = true)
			@RequestBody BookDto bookDto){
		ResponseTransaction response = new ResponseTransaction();
		try {
			
			long timeIni, timeFin, tiempo;
			timeIni = System.currentTimeMillis();
			
			response = startService.searchBooks(bookDto);
			
			timeFin = System.currentTimeMillis(); 
			tiempo = timeFin - timeIni; 
			logger.info("milisegundos books: " + tiempo);
			
			if(response!=null && !response.getCodeResponse().equalsIgnoreCase(("0000"))) {
				return this.responseHandler.getOkResponseTransaction(response);
			}
			if (response == null || response.getList().size() == 0) {
				return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros.");
			}
			return this.responseHandler.getOkResponseTransaction(response);
		} catch (EntityNotFoundResultException ex) {
			logger.error("books EntityNotFoundResultException: "+ex.getMessage());
			return this.responseHandler.getNotFoundObjectResponse(response, "No se encontraron registros." + ex);
		} catch (IllegalArgumentException ex) {
			logger.error("books IllegalArgumentException: "+ex.getMessage());
			return this.responseHandler.getAppCustomErrorResponse(response, ex.getMessage());
		} catch (Throwable ex) {
			logger.error("books Throwable: "+ex.getMessage());
			return this.responseHandler.getAppExceptionResponse(response, ex);
		}

	}
	
	

}
