package pe.gob.bnp.libreriavirtual.portal.payment.service;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.bnp.libreriavirtual.portal.payment.dto.RequestPaymentDto;
import pe.gob.bnp.libreriavirtual.portal.payment.dto.ResponsePaymentResponse;
import pe.gob.bnp.libreriavirtual.portal.payment.exception.PaymentException;
import pe.gob.bnp.libreriavirtual.portal.payment.repository.PaymentRepository;
import pe.gob.bnp.libreriavirtual.portal.payment.validation.PasarellaValidation;
import pe.gob.bnp.libreriavirtual.utilitary.common.Notification;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.service.UtilitaryService;

@Service
public class PaymentService {

	private static final Logger logger = LoggerFactory.getLogger(PaymentService.class);

	@Autowired
	PaymentRepository paymentRepository;
	
	@Autowired
	UtilitaryService utilityService;

	public ResponseTransaction pasarellaRequest(RequestPaymentDto requestPaymentDto) {

		Notification notification = PasarellaValidation.validation(requestPaymentDto);
		ResponseTransaction response = new ResponseTransaction();

		if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			logger.error("Error 0077:" + notification.errorMessage());
			return response;
		}

		response =null;// paymentRepository.vposRequest(validatePasarella(requestPaymentDto));

		return response;

	}
	
	public ResponseTransaction getPasarella(String operationNumber,String applicationId) throws IOException {

		ResponseTransaction response = paymentRepository.getPasarellaResponse(operationNumber,applicationId);
		try {
		
			if(response!=null && response.getCodeResponse().equalsIgnoreCase("0000")) {
				List<Object> emailList = response.getList();
				if (!emailList.isEmpty()) {
					Object sharedObject = emailList.get(0);
					if (sharedObject instanceof ResponsePaymentResponse) {
						ResponsePaymentResponse emailData = (ResponsePaymentResponse) sharedObject;
						
						if(emailData!=null && emailData.getAuthorizationResult().equalsIgnoreCase("00")) {
							//validar codigo respuesta solo 00
							logger.info("Enviando email EMAIL_PAYMENT... ini");						
							String templateName="EMAIL_PAYMENT";						
							utilityService.sendEmailLV(templateName,operationNumber);
							logger.info("Enviando email EMAIL_PAYMENT... fin");
							
						}else {
							logger.error("Error payme :"+((emailData!=null)?emailData.getAuthorizationResult():""));
						}
					}
				}			
			
			}
		
		}catch(Exception ex) {
			response.setCodeResponse("0666");			
			response.setResponse(ex.getMessage());
			logger.error("Erro send email:"+ex.getMessage());
			return response;
		}	

		
		return PaymentException.setMessageResponsePasarella(response);
	}
	
	
	/*
	public ResponseTransaction pasarellaResponse(ResponsePaymentDto responsePaymentDto) {

		//Notification notification = PasarellaValidation.validation(responsePaymentDto);
		ResponseTransaction response = new ResponseTransaction();

		/*if (notification.hasErrors()) {
			response.setCodeResponse("0077");
			response.setResponse(notification.errorMessage());
			logger.error("Error 0077:" + notification.errorMessage());
			return response;
		}*/

		//response = paymentRepository.vposResponse(responsePaymentDto);

		//return PaymentException.setMessageResponse(response);

	//}
	
	/*private RequestPaymentDto validatePasarella(RequestPaymentDto requestPaymentDto) {
		
		String descriptionProducts=requestPaymentDto.getDescriptionProducts();
		String firstName=requestPaymentDto.getShippingFirstName();
		String lastName=requestPaymentDto.getShippingLastName();
		String email=requestPaymentDto.getShippingEmail();
		String address=requestPaymentDto.getShippingAddress();
		String city=requestPaymentDto.getShippingCity();
		String state=requestPaymentDto.getShippingState();
		String country=requestPaymentDto.getShippingCountry();			
		
		
		//validar caracteres especiales
		descriptionProducts=Utility.replaceSpecial(descriptionProducts);
		firstName=Utility.replaceSpecial(firstName);
		lastName=Utility.replaceSpecial(lastName);
		email=Utility.replaceSpecial(email);
		address=Utility.replaceSpecial(address);
		city=Utility.replaceSpecial(city);
		state=Utility.replaceSpecial(state);
		country=Utility.replaceSpecial(country);
		
		//validar tamaño
		descriptionProducts=Utility.substringSpecial(descriptionProducts,30);
		firstName=Utility.substringSpecial(firstName,30);
		lastName=Utility.substringSpecial(lastName,50);
		email=Utility.substringSpecial(email,50);
		address=Utility.substringSpecial(address,50);
		city=Utility.substringSpecial(city,50);
		state=Utility.substringSpecial(state,15);
		country=Utility.substringSpecial(country,2);	
		
		requestPaymentDto.setDescriptionProducts(descriptionProducts);
		requestPaymentDto.setShippingFirstName(firstName);
		requestPaymentDto.setShippingLastName(lastName);
		requestPaymentDto.setShippingEmail(email);
		requestPaymentDto.setShippingAddress(address);
		requestPaymentDto.setShippingCity(city);
		requestPaymentDto.setShippingState(state);
		requestPaymentDto.setShippingCountry(country);
		
		
		return requestPaymentDto;
	}*/

}
