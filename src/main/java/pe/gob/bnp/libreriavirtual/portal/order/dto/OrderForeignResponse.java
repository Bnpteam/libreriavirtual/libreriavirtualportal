package pe.gob.bnp.libreriavirtual.portal.order.dto;


public class OrderForeignResponse {
	
	private String orderId;
	private String orderNro;
	private String clientName;
	private String emailName;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrderNro() {
		return orderNro;
	}
	public void setOrderNro(String orderNro) {
		this.orderNro = orderNro;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getEmailName() {
		return emailName;
	}
	public void setEmailName(String emailName) {
		this.emailName = emailName;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryAddress() {
		return countryAddress;
	}
	public void setCountryAddress(String countryAddress) {
		this.countryAddress = countryAddress;
	}
	public String getEmailFlag() {
		return emailFlag;
	}
	public void setEmailFlag(String emailFlag) {
		this.emailFlag = emailFlag;
	}
	private String countryName;
	private String countryAddress;
	private String emailFlag;
	

}
