package pe.gob.bnp.libreriavirtual.portal.auth.dto;

public class RecoveryPasswordRequest {

	private String email;

	public RecoveryPasswordRequest() {
		super();
		this.email= "";				
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
