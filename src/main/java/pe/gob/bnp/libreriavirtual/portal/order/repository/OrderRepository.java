package pe.gob.bnp.libreriavirtual.portal.order.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.libreriavirtual.portal.order.dto.CalculateDto;
import pe.gob.bnp.libreriavirtual.portal.order.model.Order;
import pe.gob.bnp.libreriavirtual.portal.order.model.OrderDetail;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;
import pe.gob.bnp.libreriavirtual.utilitary.repository.BaseRepository;

@Repository
public interface OrderRepository extends BaseRepository<Order> {
	
	public ResponseTransaction calculate(CalculateDto calculateDto);
	
	public ResponseTransaction validateStock(OrderDetail  detail);
	
	public ResponseTransaction persistDetail(Long id,OrderDetail detail);
	
	public ResponseTransaction persistForeign(Order order);
	
	public ResponseTransaction listOrder(Long userId);
	
	public ResponseTransaction getOrderDetail(Long orderId);
	
	public ResponseTransaction getOrderCab(Long orderId);

}
