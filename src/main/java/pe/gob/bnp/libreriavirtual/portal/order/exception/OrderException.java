package pe.gob.bnp.libreriavirtual.portal.order.exception;

import java.util.List;

import pe.gob.bnp.libreriavirtual.portal.order.dto.StockResponse;
import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

public class OrderException {
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";
	public static final  String error0002 ="El cliente no existe";
	public static final  String error0002Book ="El Libro no existe";
	public static final  String error0003Book ="El Producto no existe";
	public static final  String error0003 ="El tipo de region no existe";
	public static final  String error0004 ="El tipo de entrega no existe";
	
	public static final  String error0005 ="El tipo de envio no existe";
	public static final  String error0006 ="El id de la direccion no existe";
	public static final  String error0007 ="El id del comprobante no existe";
	public static final  String error0008 ="El id del pedido no existe";
	
	public static final  String error0019 ="No hay suficiente stock para el libro ";
	
	public static final  String error0003Update="La direccion no existe o esta inactivo.";
	
	
	public static ResponseTransaction setMessageResponseListOrder(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}		
		
		return response;
	}
	
	public static ResponseTransaction setMessageResponseGetOrder(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0008")) {
			response.setResponse(error0008);
		}		
		
		return response;
	}
	
	public static ResponseTransaction setMessageResponseSaveOrder(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003);
		}
		
		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(error0004);
		}
		
	/*	if (response.getCodeResponse().equals("0005")) {
			response.setResponse(error0005);
		}
	*/	
		if (response.getCodeResponse().equals("0006")) {
			response.setResponse(error0006);
		}
		
		if (response.getCodeResponse().equals("0007")) {
			response.setResponse(error0007);
		}

		return response;
	}
	
	public static ResponseTransaction setMessageResponseValidateStock(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002Book);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003Book);
		}
		
		if (response.getCodeResponse().equals("0019")) {
			
			String title="";
			
			List<Object> emailList = response.getList();
			if (!emailList.isEmpty()) {
				Object sharedObject = emailList.get(0);
				if (sharedObject instanceof StockResponse) {
					StockResponse	emailData = (StockResponse) sharedObject;
					title=emailData.getBookTitle();
				//	stock=emailData.getBookStock();
				}
			}
			response.setResponse(error0019+title);
		}
	

		return response;
	}

}
