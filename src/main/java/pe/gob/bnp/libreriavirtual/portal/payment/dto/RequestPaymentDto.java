package pe.gob.bnp.libreriavirtual.portal.payment.dto;

public class RequestPaymentDto {
	
	private String operationNro;
	private String purchaseAmount;
	private String shippingFirstName;
	public String getOperationNro() {
		return operationNro;
	}
	public void setOperationNro(String operationNro) {
		this.operationNro = operationNro;
	}
	public String getPurchaseAmount() {
		return purchaseAmount;
	}
	public void setPurchaseAmount(String purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}
	public String getShippingFirstName() {
		return shippingFirstName;
	}
	public void setShippingFirstName(String shippingFirstName) {
		this.shippingFirstName = shippingFirstName;
	}
	public String getShippingLastName() {
		return shippingLastName;
	}
	public void setShippingLastName(String shippingLastName) {
		this.shippingLastName = shippingLastName;
	}
	public String getShippingEmail() {
		return shippingEmail;
	}
	public void setShippingEmail(String shippingEmail) {
		this.shippingEmail = shippingEmail;
	}
	public String getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public String getShippingCity() {
		return shippingCity;
	}
	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}
	public String getShippingState() {
		return shippingState;
	}
	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}
	public String getShippingCountry() {
		return shippingCountry;
	}
	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}
	public String getDescriptionProducts() {
		return descriptionProducts;
	}
	public void setDescriptionProducts(String descriptionProducts) {
		this.descriptionProducts = descriptionProducts;
	}
	private String shippingLastName;
	private String shippingEmail;
	private String shippingAddress;
	private String shippingCity;
	private String shippingState;
	private String shippingCountry;
	private String descriptionProducts;

}
