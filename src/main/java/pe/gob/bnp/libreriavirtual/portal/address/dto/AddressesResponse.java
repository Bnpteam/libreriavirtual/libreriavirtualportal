package pe.gob.bnp.libreriavirtual.portal.address.dto;


public class AddressesResponse {
	
	private String id;
	private String userId;
	private String userName;
	private String countryId;
	private String countryName;
	private String codeDepartment;
	private String department;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCodeDepartment() {
		return codeDepartment;
	}
	public void setCodeDepartment(String codeDepartment) {
		this.codeDepartment = codeDepartment;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCodeProvince() {
		return codeProvince;
	}
	public void setCodeProvince(String codeProvince) {
		this.codeProvince = codeProvince;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCodeDistrict() {
		return codeDistrict;
	}
	public void setCodeDistrict(String codeDistrict) {
		this.codeDistrict = codeDistrict;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPersonContact() {
		return personContact;
	}
	public void setPersonContact(String personContact) {
		this.personContact = personContact;
	}
	public String getCellphoneContact() {
		return cellphoneContact;
	}
	public void setCellphoneContact(String cellphoneContact) {
		this.cellphoneContact = cellphoneContact;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	private String codeProvince;
	private String province;
	private String codeDistrict;
	private String district;
	private String identifier;
	private String description;
	private String personContact;
	private String cellphoneContact;
	private String reference;


}
