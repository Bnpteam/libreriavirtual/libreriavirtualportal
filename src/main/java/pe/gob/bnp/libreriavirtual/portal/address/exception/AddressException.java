package pe.gob.bnp.libreriavirtual.portal.address.exception;

import pe.gob.bnp.libreriavirtual.utilitary.common.ResponseTransaction;

public class AddressException {
	
	public static final  String error9999 ="No se ejecutó ninguna transacción.";
	public static final  String error0001 ="La direccion ya se encuentra registrada.";
	public static final  String error0002 ="El id del usuario no existe o esta inactivo";
	public static final  String error0003 ="La categoria asociada no existe";
	public static final  String error0004 ="El id de la direccion no existe";
	
	public static final  String error0003Update="La direccion no existe o esta inactivo.";
	
	
	public static ResponseTransaction setMessageResponseListAddress(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0002")) {
			response.setResponse(error0002);
		}
		
		
		return response;
	}
	
	public static ResponseTransaction setMessageResponseAddress(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}
		
		if (response.getCodeResponse().equals("0004")) {
			response.setResponse(error0004);
		}
		
		
		return response;
	}
	
	public static ResponseTransaction setMessageResponseSave(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001);
		}

		return response;
	}
	
	public static ResponseTransaction setMessageResponseUpdate(ResponseTransaction response) {

		if (response.getCodeResponse().equals("9999")) {
			response.setResponse(error9999);
		}

		if (response.getCodeResponse().equals("0001")) {
			response.setResponse(error0001);
		}
		
		if (response.getCodeResponse().equals("0003")) {
			response.setResponse(error0003Update);
		}

		return response;
	}

}
