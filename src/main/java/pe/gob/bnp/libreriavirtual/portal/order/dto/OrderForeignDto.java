package pe.gob.bnp.libreriavirtual.portal.order.dto;

import java.util.List;


public class OrderForeignDto {
	
	private String clientId;
	private String subtotalCost;
	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getSubtotalCost() {
		return subtotalCost;
	}

	public void setSubtotalCost(String subtotalCost) {
		this.subtotalCost = subtotalCost;
	}

	public String getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(String shippingCost) {
		this.shippingCost = shippingCost;
	}

	public String getIgvCost() {
		return igvCost;
	}

	public void setIgvCost(String igvCost) {
		this.igvCost = igvCost;
	}

	public String getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCountryAddress() {
		return countryAddress;
	}

	public void setCountryAddress(String countryAddress) {
		this.countryAddress = countryAddress;
	}

	public List<OrderDetailDto> getDetailBookAndProduct() {
		return detailBookAndProduct;
	}

	public void setDetailBookAndProduct(List<OrderDetailDto> detailBookAndProduct) {
		this.detailBookAndProduct = detailBookAndProduct;
	}

	private String shippingCost;
	private String igvCost;
	private String totalCost;
	
	private String countryId;
	private String countryAddress;	
	
	private List<OrderDetailDto> detailBookAndProduct;
		

}
