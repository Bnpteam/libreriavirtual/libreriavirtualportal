## SERVICIOS LIBRERIA VIRTUAL
### Descripción
- Backend de servicios Libreria Virtual.
- Desde la página principal de Libreria https://libreriavirtual.bnp.gob.pe/#/  se puede visualizar los libros o productos a vender de la BNP. Usuarios:
    Usuario Cliente o Público: Usuario que realiza compras.
    Usuario Administrador: Personal de la BNP encargado de cambiar los estados en los que se encuentre la BNP. https://libreriavirtual.bnp.gob.pe/#/loginadmin


### Configuración

- Para configurar en el ambientes de TEST:

 * Dentro del Proyecto libreriavirtualportal cambiar parametro spring.profiles.active a "development" en el archivo application.properties.
 * Dentro del Proyecto libreriavirtualportal cambiar Datasource  dentro de /config , apuntando a la conexion de TEST.
 * Desactivar ssl archivo principal LibreriavirtualApplication y parametros server.ssl.
 * IDE - STS 4.0.
 * TOMCAT 9.0 EMBEBIDO.
 * JDK 1.8.0.261
 * RUTA: http://servicioslibreriavirtual.bnp.gob.pe:8086/libreriavirtual/swagger-ui.html#
   
- Para configurar en el ambiente de PROD:

 * Dentro del Proyecto libreriavirtualportal cambiar parametro spring.profiles.active a "production" en el archivo application.properties.
 * Activar ssl archivo principal LibreriavirtualApplication y parametros server.ssl. (cambio de puerto)
 * IDE - STS 4.0.
 * TOMCAT 9.0 EMBEBIDO.
 * JDK 1.8.0.261
 * RUTA: https://apilibreriavirtual.bnp.gob.pe:9880/libreriavirtual/swagger-ui.html#/   ( actualmente libreria virtual apunta a un war en jboss 7)
 

### Despliegue Microservicio

- Utilizar el archivo libreriavirtual-be.service como base para desplegar el microservicio
- Pasos de despliegue:
    1. Generar jar del aplicativo libreriavirtualportal (mave clean build install).
    2. Copiar el jar dentro del servidor test o prod que desee en la ruta indicada por el archivo libreriavirtual-be.service. (dar permisos escritura y lectura al jar)
    3. Generar en caso no exista un servicio en linux con el nombre del archivo libreriavirtual-be.service.
        * Para crear servicio en linux, acceder a la ruta /etc/systemd validar los servicios existentes y copiar con el comando "cp" servicio existente actualizar       las rutas para que apunten al jar creado. 
    4. Una vez creado o actualizado el archivo .service reiniciarlo de preferencia utilizar los comando:
        systemctl status libreriavirtual-be.service (ver el estado actual)
        systemctl stop libreriavirtual-be.service (detener servicio)
        systemctl start libreriavirtual-be.service (inciar servicio)
    
    5. Finalmente validar los servicios desde la ruta generado por el swagger.

 
#### version: v1.0.2